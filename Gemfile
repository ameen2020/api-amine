source 'https://rubygems.org'

gem 'rails',    '~> 5.1.2'   # Yes, its Rails :p
gem 'pg',       '~> 0.18'    # for postgresql
gem 'puma',     '~> 3.7'     # as application server
gem 'bcrypt',   '~> 3.1.7'   # to encrept data and passwords
gem 'jwt'                    # generate and handle api authorization tokens
gem 'consul'                 # for scope authorization
gem 'ransack'                # for db custom search
gem 'kaminari'               # handle ActiveRecord Pagination
gem 'rack-attack'            # to throttle all apis
gem 'acts_as_api'            # serialize models for the json apis
gem 'rack-cors', require: 'rack/cors' # protect from cors requests
gem 'json_translate'         # Allow you to translate your model attribute
gem 'email_validator'        # Help validate the email format
gem 'validates_timeliness', '~> 4.0' # Validate Date, Time or DateTime
gem 'geocoder'
gem 'wicked_pdf'            # Use wicked-pdf and Wkhtmltopdf-binary to hundle with pdf
gem 'wkhtmltopdf-binary'
gem 'paper_trail'            # Track changes to your models
gem 'ruby-graphviz'

gem 'faker'                  # to generate a random data
gem 'factory_girl_rails'     # factory to gererate random records
gem "paperclip", "~> 5.0.0"
gem 'pubnub', '~> 4.0.23'
gem 'resque' # Background processing for Ruby
gem 'ruby-ntlm'
gem "resque-history" #resque-history adds functionality record recently history of job executions
gem 'resque-scheduler'
gem 'spreadsheet', '~> 1.1'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'pry-rails'                 # for better debuggin
  gem 'rspec-rails', '~> 3.6'     # Rspec for unit testing
  gem 'rails-erd'                 # Generate pfd for your schema database RUN => rake erd
  gem 'shoulda',     '~> 3.5'     # add matchers for easies tests
  gem 'webmock'                   # stub and setting expectations on HTTP requests
  gem 'database_cleaner'          # clean test db for each/all request
  gem 'annotate'                  # annotate model fields
  gem 'simplecov', require: false # for test coverage

  # Capistrano family
  gem "capistrano", "~> 3.9"
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-rvm'
  gem 'capistrano3-puma'
  gem "letter_opener"
  gem "capistrano-resque", "~> 0.2.2", require: false
  gem "capistrano-db-tasks", require: false
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end
