# == Schema Information
#
# Table name: assignments
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  email                    :string
#  date                     :date
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  views_counter            :integer          default(0), not null
#

class Assignment < ApplicationRecord

  ## -------------------- Requirements -------------------- ##
  translates :title, :description
  include AssignmentPresenter
  include Imageable
  include IncreaseViews
  include SearchableTranslation
  include Notifiable
  include SendNotifiactionAdmin
  include CreatedBy


  ## -------------------- Associations -------------------- ##
  has_many :notifications_histories, as: :notifiable, dependent: :destroy
  has_paper_trail

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation:{ required_keys: [:ar, :en] },
             unique_translation: true
  validates :description_translations, presence: true, translation: { required_keys: [:ar, :en] }
  validates_datetime :date,  on_or_after: (-> { Date.today  }), if: :date_changed?
  validates :email, allow_nil: true, email: true

  ## ---------------------- Scopes ------------------------ ##
  scope :active_jobs, -> { where('date >= ? or date is null', Date.today)  }
end
