# == Schema Information
#
# Table name: pictures
#
#  id                      :integer          not null, primary key
#  imageable_id            :integer          not null
#  imageable_type          :string           not null
#  cover                   :boolean          default(FALSE), not null
#  attachment_file_name    :string
#  attachment_content_type :string
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class Picture < ApplicationRecord

  ## -------------------- Requirements -------------------- ##
  include PicturePresenter

  Paperclip.interpolates :imageable_type do |attachment, style|
      attachment.instance.imageable_type.pluralize.downcase
  end

  Paperclip.interpolates :imageable_id do |attachment, style|
      attachment.instance.imageable_id
  end

  has_attached_file :attachment, styles: { thumb: '300x300>' },
                                 url:   "/upload/:imageable_type/:imageable_id/picture_:id/:style/:filename",
                                 path:  ':rails_root/public:url'

  validates_attachment :attachment, content_type: { content_type: /\Aimage\/.*\z/ }

  # ---------------------- Association ------------------- #
  belongs_to :imageable, polymorphic: true, optional: true

  ## -------------------- Validations --------------------- ##
  # validates :imageable_id,   presence: true
  # validates :imageable_type, presence: true
  validates :imageable_type, uniqueness: { scope: [:cover, :imageable_id], message: :one_cover_only }, if: :cover

  ## ---------------------- Scopes ------------------------ ##
  scope :cover,     -> { where(cover: true)  }
  scope :non_cover, -> { where(cover: false) }

  default_scope -> { order(cover: :desc) } # Show cover always first

  def image_url(size = nil)
     URI.join(ActionController::Base.asset_host, attachment&.url(size)).to_s
  end
end
