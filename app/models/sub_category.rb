# == Schema Information
#
# Table name: sub_categories
#
#  id                 :integer          not null, primary key
#  title_translations :jsonb            not null
#  status             :integer          default(1), not null
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  logo_file_name     :string
#  logo_content_type  :string
#  logo_file_size     :integer
#  logo_updated_at    :datetime
#

class SubCategory < ApplicationRecord
  
  ## -------------------- Requirements -------------------- ##
  translates :title
  include SubCategoryPresenter
  include SearchableTranslation

  has_attached_file :logo, styles: { thumb: '200x200>' },
                                 url: '/upload/:class/logo/:id/:style/:filename',
                                 path: ':rails_root/public:url',
                                 default_url: 'default_logo.jpg'
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\z/

  ## ---------------------- Association ----------------------- ##
  has_many :offers

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] },
            unique_translation: true

## ---------------------- Methods ----------------------- ##
  def logo_url
    URI.join(ActionController::Base.asset_host, logo.url).to_s
  end
end
