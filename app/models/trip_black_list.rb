# == Schema Information
#
# Table name: trip_black_lists
#
#  id          :integer          not null, primary key
#  employee_id :integer          not null
#  blocker_id  :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class TripBlackList < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include TripBlackListPresenter

  ## ----------------------- Scopes ----------------------- ##
  ## --------------------- Constants ---------------------- ##
  ## ----------------------- Enums ------------------------ ##
  ## -------------------- Associations -------------------- ##

  belongs_to :employee
  belongs_to :blocker, class_name: 'Employee'

  ## -------------------- Validations --------------------- ##

  validates :employee_id, presence: true, uniqueness: true

  ## --------------------- Callbacks ---------------------- ##
  ## ------------------- Class Methods -------------------- ##
  ## ---------------------- Methods ----------------------- ##
end
