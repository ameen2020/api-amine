class ApplicationRecord < ActiveRecord::Base
  include Exportable

  self.abstract_class = true

  # Order json fields by language content
  # NOTE : Need to be improved, it might case a SQL Injection
  scope :order_by_json, lambda { |column = 'name_translations', dir = :asc|
    order("#{column}->>'#{I18n.locale}' #{dir.to_s == 'asc' ? 'ASC' : 'DESC'}")
  }

  # support add scopes to search
  def self.ransackable_scopes(_auth_object = nil)
    %w[title_translation_cont translations_cont]
  end
end
