# == Schema Information
#
# Table name: event_registrations
#
#  id            :integer          not null, primary key
#  employee_id   :integer          not null
#  event_id      :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  register_code :string           not null
#  locale        :string           default("en"), not null
#

class EventRegistration < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include EventRegistrationPresenter
  include Localeable

  ## -------------------- Associations -------------------- ##
  belongs_to :employee
  belongs_to :event

  ## --------------------- Callbacks ---------------------- ##

  before_destroy :valid_for_cancel?
  after_create_commit :send_registration_code_email
  after_create_commit :inform_admins
  before_validation :generate_random_code
  after_destroy_commit :inform_admins_of_cancelation

  ## -------------------- Validations --------------------- ##
  validates :employee_id, presence: true
  validates :register_code, presence: true, uniqueness: true
  validates :event_id,    presence: true
  validates :employee_id, uniqueness: { scope: :event_id, message: :already_registered}

  validate :is_event_registrable?, on: :create

  ## ---------------------- Methods ----------------------- ##

  def is_event_registrable?
    errors.add(:base, :cant_register) unless event&.can_register?
  end

  def valid_for_cancel?
    unless event.can_cancel?
      errors.add(:base, :cant_cancel)
      throw :abort
    end
  end

  def generate_random_code
    self.register_code = loop do
      random_code = SecureRandom.hex(3).upcase
      break random_code unless self.class.exists?(register_code: random_code)
    end if register_code.nil?
  end

  def send_registration_code_email
    EventRegistrationMailer.send_registration_code(self.id).deliver_later
  end

  def send_reminder_email(days)
    EventRegistrationMailer.reminder(self.id, days).deliver_later
  end

  # Send emails to Sub Admins for every registration
  def inform_admins
    Employee.sub_admin.each do |emp|
      EventRegistrationMailer.new_registration(self.id, emp.id).deliver_later
    end
  end

  def push_reminder_notification(days)
    %w(ar en).each do |locale|
      message = [I18n.t('notifications.event.reminder', title: event.send("title_#{locale}"), count: days, locale: locale.to_sym)]
      Resque.enqueue(Publisher, self.employee_id, message, 'EVENTS', self.event_id, nil, locale)
    end
  end

  # Infrom all admins when user cancel the registration
  def inform_admins_of_cancelation
    Employee.where.not(role: 1).each do |emp|
      EventRegistrationMailer.cancel_by_user(emp.id, self.employee_id, self.event_id, JSON.parse(self.to_json)['event_registration']).deliver_later
    end
  end

  ## --------------------- Exporters ---------------------- ##

  def self.exportable_headers
    [
      'Employee Id',
      'Employee Full Name',
      'Employee Phone Number',
      'Employee Email',
      'Register Code',
      'Registration Time',
    ]
  end

  def exportable_data
    [
      employee.id,
      employee.full_name,
      employee.phone_number,
      employee.email,
      register_code,
      created_at.in_time_zone('Riyadh').strftime('%F %r')
    ]
  end
end
