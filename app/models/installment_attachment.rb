# == Schema Information
#
# Table name: installment_attachments
#
#  id                      :integer          not null, primary key
#  installment_id          :integer          not null
#  attachment_file_name    :string
#  attachment_content_type :string
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class InstallmentAttachment < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include InstallmentAttachementPresenter
  include AttachmentType

  ## -------------------- Associations -------------------- ##

  belongs_to :installment

  ## -------------------- Validations --------------------- ##

  validates_attachment_presence :attachment

end
