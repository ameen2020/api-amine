# == Schema Information
#
# Table name: chalets
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  address_translations     :jsonb            not null
#  city_translations        :jsonb            not null
#  location_translations    :jsonb            not null
#  start_date               :datetime         not null
#  end_date                 :datetime
#  attachment_file_name     :string
#  attachment_content_type  :string
#  attachment_file_size     :integer
#  attachment_updated_at    :datetime
#  latitude                 :decimal(10, 8)   default(0.0), not null
#  longitude                :decimal(11, 8)   default(0.0), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  last_cancel              :datetime
#  views_counter            :integer          default(0), not null
#

class Chalet < ApplicationRecord
  translates :title, :description,:city, :location, :address

  ## -------------------- Requirements -------------------- ##
  include ChaletPresenter
  include AttachmentType
  include Imageable
  include IncreaseViews
  include SearchableTranslation
  include Notifiable
  include SendNotifiactionAdmin
  include CreatedBy

  ## -------------------- Associations -------------------- ##
  has_many :chalet_registrations, dependent: :delete_all
  has_many :employees,            through:   :chalet_registrations
  has_many :notifications_histories, as: :notifiable, dependent: :destroy
  has_and_belongs_to_many :regions, dependent: :destroy
  has_paper_trail

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] }, unique_translation: true
  validates :description_translations, presence: true, translation: { required_keys: [:ar, :en] }
  validates :region_ids, presence: true

  validates_datetime :start_date, on_or_after: (-> { Date.today  }),
                     if: :start_date_changed?

  validates_datetime :end_date,  on_or_after: (-> (e){ e.start_date }),
                      allow_nil: true, if: :end_date_changed?

  validates_datetime :last_cancel, before: (-> (e){ e.start_date }),
                     allow_nil: true, if: :last_cancel_changed?


  ## --------------------- Call Backs --------------------- ##

  before_destroy :infrom_registered_users_of_destroy, prepend: true

  ## ---------------------- Scopes ------------------------ ##

    # get the count of pending chalet registerations based on the role
    scope :with_pendings, -> (role = 'sub_admin'){
      sql = ChaletRegistration.select('COUNT(*)')
      sql = sql.sub_admin_pending if role == 'sub_admin'
      sql = sql.super_admin_pending.where('chalet_registrations.chalet_id = chalets.id').to_sql

      select('"chalets".*, (' + sql + ') as "pending_registrations_count"')
    }


  ## ------------------- Class Methods -------------------- ##
  # Get list of month dates that have chalet
  # @param [Integer] year
  # @param [Integer] month
  # @return [Array]  Array of dates
  def self.get_calender(year, month)
    from = Date.parse("#{year}-#{month}-01")
    to   = from.end_of_month
    Chalet.where('start_date between ? and ?', from, to).pluck(:start_date).uniq
  end

  ## ---------------------- Methods ----------------------- ##

  # Check if chalet registration still available
  # @return [Boolean]
  def can_register?
    start_date > Date.today
  end

  # Check if chalet registr cancelation still available
  # @return [Boolean]
  def can_cancel?
    last_cancel ? last_cancel >= Date.today : start_date > Date.today
  end

  def pending_registrations_count
    self['pending_registrations_count']
  end

  # Inform all registered users about chalet cancelation
  def infrom_registered_users_of_destroy
    chalet_registrations.each do |register|
      ChaletRegistrationMailer.inform_user_of_delete(register.employee_id, title_en, register.locale).deliver_later
      %w(ar en).each do |locale|
        message = [I18n.t('notifications.chalet.cancel', title: send("title_#{locale}"), locale: locale.to_sym)]
        # used CHALET push type to ignore any action by mobile
        Resque.enqueue(Publisher, register.employee_id, message, 'CHALET', self.id, self.id, locale)
      end
    end
  end

  def export_registered_users
    CSV.generate(encoding: 'UTF-8') do |csv|
      csv << [
        'Employee Id',
        'Employee Full Name',
        'Employee Phone Number',
        'Employee Email',
        'Register Code',
        'Registration Time',
      ]
      chalet_registrations.includes(:employee)
                        .order(created_at: :asc)
                        .each do |register|
        csv << [
          register.employee.id,
          register.employee.full_name,
          register.employee.phone_number,
          register.employee.email,
          register.register_code,
          register.created_at.in_time_zone('Riyadh').strftime('%F %r'),
        ]
      end
    end
  end
end
