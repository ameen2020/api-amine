# == Schema Information
#
# Table name: notifications_settings
#
#  id             :integer          not null, primary key
#  employee_id    :integer          not null
#  event          :boolean          default(FALSE), not null
#  offer          :boolean          default(FALSE), not null
#  trip           :boolean          default(FALSE), not null
#  news           :boolean          default(FALSE), not null
#  memo           :boolean          default(FALSE), not null
#  job            :boolean          default(FALSE), not null
#  magazine       :boolean          default(FALSE), not null
#  region         :integer          default([]), is an Array
#  sub_category   :integer          default([]), is an Array
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  chalet         :boolean          default(FALSE), not null
#  mbadrat        :boolean          default(FALSE), not null
#  employee_voice :boolean          default(FALSE), not null
#  installment    :boolean          default(FALSE), not null
#

class NotificationsSetting < ApplicationRecord

  ## -------------------- Requirements -------------------- ##
  include NotificationsSettingPresenter

  # ---------------------- Association ------------------- #
  belongs_to :employee

  # ---------------------- validations ------------------- #

  def categories
    Category.category_setting.as_api_response(:notification_settings, {active_settings: active_settings})
  end

  def active_settings
    attribute_names.select{ |x|self[x] == true }
  end

  def regions(current_user)
    region_ids = current_user.notifications_setting&.region
    Region.all.as_api_response(:notification_settings, {region_ids: region_ids})
  end

  def sub_categories(current_user)
      sub_category_ids = current_user.notifications_setting&.sub_category
    SubCategory.all.as_api_response(:notification_settings, {sub_category_ids: sub_category_ids})
  end

  def self.disable_all
    update_all(
      event: false,
      offer: false,
      trip: false,
      news: false,
      memo: false,
      job: false,
      magazine: false,
      region: [],
      sub_category: [],
      chalet: false,
      installment: false,
      mbadrat: false,
      employee_voice: false
    )
  end
end
