# == Schema Information
#
# Table name: mbadrat_faqs
#
#  id                       :integer          not null, primary key
#  mbadrat_id               :integer          not null
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class MbadratFaq < ApplicationRecord
  translates :title, :description
  include MbadratFaqPresenter
  include SearchableTranslation
  ## -------------------- Associations -------------------- ##
  belongs_to :mbadrat
    ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] }, unique_translation: true
  validates :description_translations, presence: true, translation: { required_keys: [:ar, :en] }
end
