# == Schema Information
#
# Table name: employee_voices
#
#  id                            :integer          not null, primary key
#  employee_id                   :integer          not null
#  voice_category                :integer          default("suggestion"), not null
#  phone_number                  :string           not null
#  message                       :string           not null
#  attachment_file_name          :string
#  attachment_content_type       :string
#  attachment_file_size          :integer
#  attachment_updated_at         :datetime
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  admin_id                      :integer
#  reply_message                 :text
#  replied_at                    :datetime
#  locale                        :string           default("en"), not null
#  attachment_admin_file_name    :string
#  attachment_admin_content_type :string
#  attachment_admin_file_size    :integer
#  attachment_admin_updated_at   :datetime
#

class EmployeeVoice < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  include EmployeeVoicePresenter
  include AttachmentType
  # include Notifiable
  has_attached_file :attachment_admin, styles: { thumb: '200x200>' },
                                        url:   "/upload/:class/:id/:style/:filename",
                                        path:  ':rails_root/public:url'

  validates_attachment_content_type :attachment_admin, content_type: [
    'application/pdf',
    'image/png',
    'image/jpg',
    'image/jpeg',
    'image/gif'
  ]


  ## ---------------------- Associations ----------------------- ##
  belongs_to :employee
  belongs_to :admin, class_name: 'Employee', optional: true

  ## --------------------- Callbacks ---------------------- ##
  after_update_commit :send_reply_email
  before_validation do
    self.replied_at = Time.zone.now if replied? && replied_at.nil?
  end

  ## -------------------- Validations --------------------- ##
  validates :employee_id, presence: true
  validates :phone_number,    presence: true
  validates :message, presence: true
  validates :admin,         presence: true, if: -> { reply_message.present? }
  validates :reply_message, presence: true, if: -> { admin_id.present? }
  ## ----------------------- Enums ------------------------ ##
  enum voice_category: {
    suggestion: 0,
    complain:  1
  }
  ## ---------------------- Methods ----------------------- ##
  def send_reply_email
    EmployeeVoiceMailer.reply(id,attachment_admin.path).deliver_later if replied?
  end

  def status
    replied? ? :replied : :pending
  end

  def replied?
    reply_message.present? && admin_id.present?
  end

  def attachment_admin_url
    URI.join(ActionController::Base.asset_host, attachment_admin.url).to_s if attachment_admin.present?
  end
end
