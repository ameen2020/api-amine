# == Schema Information
#
# Table name: employees
#
#  id                       :integer          not null, primary key
#  employee_id              :string
#  first_name               :string           not null
#  last_name                :string           default("-"), not null
#  email                    :string
#  password_digest          :string
#  role                     :integer          default("employee"), not null
#  status                   :integer          default("active"), not null
#  otp                      :integer
#  otp_expire_on            :datetime
#  register_token           :string
#  register_token_expire_on :datetime
#  new_phone_number         :string
#  phone_number             :string
#  otp_verified             :boolean          default(FALSE), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  reset_password_token     :string
#  reset_password_sent_at   :datetime
#  username                 :string           not null
#

class Employee < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  REGISTER_TKN_EXP = 5.minutes

  include EmployeePresenter
  include Forgettable
  include OtpAuth

  has_secure_password(validations: false)

  ## ---------------------- Associations ----------------------- ##
  has_many :event_registrations, dependent: :destroy
  has_many :employee_voices, dependent: :destroy
  has_many :events, -> {order("event_registrations.created_at DESC")}, through: :event_registrations

  has_many :trip_registrations, dependent: :destroy
  has_many :trips, -> {order("trip_registrations.created_at DESC")}, through: :trip_registrations

  has_many :chalet_registrations, dependent: :destroy
  has_many :chalets, -> {order("chalet_registrations.created_at DESC")}, through: :chalet_registrations

  has_many :offer_feedback
  has_many :ashanaks
  has_many :installments
  has_one  :notifications_setting
  has_one  :trip_black_list
  has_many :notifications_histories
  has_many :notification_admins
  has_many :sent_messages, class_name: "Message", foreign_key: :sender_id
  has_many :versions,   foreign_key: 'whodunnit'

  ## --------------------- Callbacks ---------------------- ##
  before_create :generate_register_token
  after_create_commit :create_notifications_setting

  ## -------------------- Validations --------------------- ##
  validates :employee_id,  allow_nil:  true, uniqueness: true, if: :employee_id
  validates :first_name,   presence:  true
  validates :last_name,    presence:  true
  validates :email, presence: true, uniqueness: { case_sensitive: false }, email: true
  validates :phone_number, allow_nil: true, uniqueness: true#, mobile_number: true
  validates :new_phone_number,  allow_nil: true, uniqueness: true#, mobile_number: true

  validate :check_new_number, on: :update


  ## ----------------------- Enums ------------------------ ##

  # User Statuses
  enum status: {
    # pending:  0, # When user created will be pending
    active:   1, # When user login first time by otp will be activated
    inactive: 2  # Admin can dissable users and those uses will not be able to login or use the app
  }

  # User Roles
  enum role: {
    super_admin:    0,
    employee:       1,
    sub_admin:      2,
    admin:          3
  }

  ## ------------------- Class Methods -------------------- ##

  # ِAdmin login
  def self.login(email, password)
    active.where(role: [0,2,3]).find_by(email: email)&.authenticate(password) || false
  end

  # Find pending employees with employee id a valid register token
  # Used to validate user request to set phone_number in registration step 2
  def self.find_registered_user(employee_id, token)
    emp = pending.find_by(employee_id: employee_id, register_token: token)
    return false unless emp&.valid_register_token?
    emp
  end

  ## --------------------- Exporters ---------------------- ##

  def self.exportable_headers
    [
      'Id',
      'MOH ID',
      'Full Name',
      'Phone Number',
      'Email',
      'Role',
      'Join Time'
    ]
  end

  def exportable_data
    [
      id,
      employee_id,
      full_name,
      phone_number,
      email,
      role.titleize,
      created_at.in_time_zone('Riyadh').strftime('%F %r')
    ]
  end

  ## ---------------------- Methods ----------------------- ##

  # JWT payload
  def login_payload
    {
      id:         self.id,
      first_name: self.first_name,
      last_name:  self.last_name,
      full_name:  self.full_name,
      admin:      self.admin?,
      role:       self.role,
      avatar:     self.avatar
    }
  end

  # Generate a tempraty token that will be used by the new employee
  # while in registration process
  def generate_register_token
    self.register_token = SecureRandom.hex(6)
    self.register_token_expire_on = Time.zone.now + REGISTER_TKN_EXP
  end

  # Clear the exist register token
  def clear_register_token
    self.register_token = nil
    self.register_token_expire_on = nil
  end

  # Clear the exist register token and Save!
  def clear_register_token!
    clear_register_token
    save!(validate: false)
  end

  # Check if register token is still valid
  def valid_register_token?
    register_token_expire_on && (register_token_expire_on >= Time.zone.now) || false
  end

  # Used after registration to:
  # - set user phone number
  # - send OTP to the client
  def set_register_phone_number(phone_number)
    # update the phone_number
    self.phone_number = phone_number

    # generate OTP
    generate_otp

    # if save success, sent OTP Message
    send_otp! if self.save

    # return self
    self
  end

  # Check the new number phone if already taken by any user
  def check_new_number
    return unless new_phone_number?
    errors.add(:base, :check_new_number) if Employee.exists?(phone_number: new_phone_number)
  end

  def avatar
    URI.join(ActionController::Base.asset_host, 'default_profile.jpg').to_s
  end
end
