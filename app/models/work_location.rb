# == Schema Information
#
# Table name: work_locations
#
#  id                 :integer          not null, primary key
#  region_id          :integer          not null
#  title_translations :jsonb            not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class WorkLocation < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  translates :title
  include WorkLocationPresenter
  include SearchableTranslation

  ## ---------------------- Association -------------------- ##
  belongs_to :region
  has_many :ashanaks, dependent: :restrict_with_error

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] },
            unique_translation: true
end
