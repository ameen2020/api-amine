# == Schema Information
#
# Table name: trips
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  location_translations    :jsonb            not null
#  start_date               :datetime         not null
#  end_date                 :datetime
#  last_register_date       :datetime         not null
#  last_cancel_date         :datetime
#  capacity                 :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  views_counter            :integer          default(0), not null
#

class Trip < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  translates :title, :description, :location
  include TripPresenter
  include Imageable
  include IncreaseViews
  include SearchableTranslation
  include Notifiable
  include SendNotifiactionAdmin
  include CreatedBy

  ## -------------------- Associations -------------------- ##

  has_many :notifications_histories, as: :notifiable, dependent: :destroy
  has_many :trip_registrations, dependent: :delete_all
  has_many :employees, through: :trip_registrations
  has_paper_trail

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] },
            unique_translation: true
  validates :description_translations, presence: true, translation: { required_keys: [:ar, :en] }
  validates :location_translations,    presence: true, translation: { required_keys: [:ar, :en] }

  validates_datetime :start_date, on_or_after: (-> { Date.today }),
                     if: :start_date_changed?

  validates_datetime :end_date,on_or_after: (-> (e){ e.start_date }),
                     allow_nil: true, if: :end_date_changed?

  validates_datetime :last_register_date, before: (-> (e){ e.start_date }), allow_nil: true,
                     if: :last_register_date_changed?

  validates_datetime :last_cancel_date, before: (-> (e){ e.start_date }),
                     allow_nil: true, if: :last_cancel_date_changed?

  ## --------------------- Call Backs --------------------- ##

  before_destroy :infrom_registered_users_of_destroy, prepend: true

  ## ---------------------- Scopes ------------------------ ##

  # get the count of pending trip registerations based on the role
  scope :with_pendings, -> (role = 'sub_admin'){
    sql = TripRegistration.select('COUNT(*)')
    sql = sql.sub_admin_pending if role == 'sub_admin'
    sql = sql.super_admin_pending.where('trip_registrations.trip_id = trips.id').to_sql

    select('"trips".*, (' + sql + ') as "pending_registrations_count"')
  }

  ## ------------------- Class Methods -------------------- ##

  # Get list of month dates that have trips
  # @param [Integer] year
  # @param [Integer] month
  # @return [Array]  Array of dates
  def self.get_calender(year, month)
    from = Date.parse("#{year}-#{month}-01")
    to   = from.end_of_month
    Trip.where('start_date between ? and ?', from, to).pluck(:start_date).uniq
  end

  ## ---------------------- Methods ----------------------- ##

  # Check the number of available tickets
  # @return [NilClass] if no capacity
  # @return [Integer]  if capacity is set
  def remaining
    capacity? ? (capacity - trip_registrations.count) : nil
  end

  # Check if trip registration still available
  # @return [Boolean]
  def can_register?
    start_date > Date.today && (last_register_date > Date.today || last_register_date.today?) && ( capacity? ? ( remaining > 0 && remaining <= capacity ) : true )
  end

  # Check if trip registr cancelation still available
  # @return [Boolean]
  def can_cancel?
    last_cancel_date ? last_cancel_date >= Date.today : start_date > Date.today
  end

  def pending_registrations_count
    self['pending_registrations_count']
  end

  # Inform all registered users about trip cancelation
  def infrom_registered_users_of_destroy
    trip_registrations.each do |register|
      TripRegistrationMailer.inform_user_of_delete(register.employee_id, title, register.locale).deliver_later
      %w(ar en).each do |locale|
        message = [I18n.t('notifications.trip.cancel', title: send("title_#{locale}"), locale: locale.to_sym)]
        Resque.enqueue(Publisher, register.employee_id, message, 'ASHANAK', self.id, self.id, locale)
      end
    end
  end
end
