# == Schema Information
#
# Table name: magazins
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  attachment_file_name     :string
#  attachment_content_type  :string
#  attachment_file_size     :integer
#  attachment_updated_at    :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  views_counter            :integer          default(0), not null
#

class Magazin < ApplicationRecord

  ## -------------------- Requirements -------------------- ##
  translates :title, :description
  include MagazinPresenter
  include Imageable
  include IncreaseViews
  include AttachmentType
  include Notifiable
  include SendNotifiactionAdmin
  include CreatedBy


  ## -------------------- Associations -------------------- ##
  has_many :notifications_histories, as: :notifiable, dependent: :destroy
  has_paper_trail

  ## -------------------- Validations --------------------- ##
  validates_attachment_presence :attachment
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] },
             unique_translation: true
  validates :description_translations, presence: true, translation: { required_keys: [:ar, :en] }
end
