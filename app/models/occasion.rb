# == Schema Information
#
# Table name: occasions
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class Occasion < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  include OccasionPresenter
  include SearchableTranslation
  translates :title, :description

  # ---------------------- Association ------------------- #
  has_many :ashanaks, dependent: :restrict_with_error

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] },
            unique_translation: true
  validates :description_translations, presence: true, translation: { required_keys: [:ar, :en] }
end
