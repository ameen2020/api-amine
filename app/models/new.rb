# == Schema Information
#
# Table name: news
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  date                     :date
#  important                :boolean          default(FALSE), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  views_counter            :integer          default(0), not null
#

class New < ApplicationRecord

  ## -------------------- Requirements -------------------- ##
  translates :title, :description
  include NewPresenter
  include Imageable
  include IncreaseViews
  include Notifiable
  include SendNotifiactionAdmin
  include CreatedBy

  ## -------------------- Associations -------------------- ##
  has_many :notifications_histories, as: :notifiable, dependent: :destroy
  has_paper_trail

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] },
            unique_translation: true
  validates :description_translations, presence: true, translation: { required_keys: [:ar, :en] }
  validates :important, presence: true, if: proc {|t|t.important.nil?}
  validates_datetime :date,  on_or_after: (-> { Date.today  }), if: :date_changed?

  ## ---------------------- Scopes ------------------------ ##
  scope :active_news, -> { where('date <= ? or date is null', Date.today)  }

end
