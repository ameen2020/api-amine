# == Schema Information
#
# Table name: events
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  address_translations     :jsonb            not null
#  city_translations        :jsonb            not null
#  location_translations    :jsonb            not null
#  start_date               :datetime         not null
#  end_date                 :datetime
#  capacity                 :integer
#  latitude                 :decimal(10, 8)   default(0.0), not null
#  longitude                :decimal(11, 8)   default(0.0), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  last_cancel              :datetime
#  locations                :json             not null
#  views_counter            :integer          default(0), not null
#

class Event < ApplicationRecord
  translates :title, :description, :address, :city, :location

  ## -------------------- Requirements -------------------- ##
  include EventPresenter
  include Imageable
  include IncreaseViews
  include SearchableTranslation
  include Notifiable
  include MultiLocations
  include SendNotifiactionAdmin
  include CreatedBy

  ## -------------------- Associations -------------------- ##
  has_many :event_registrations, dependent: :delete_all
  has_many :employees,           through:   :event_registrations
  has_many :notifications_histories, as: :notifiable, dependent: :destroy
  has_and_belongs_to_many :regions, dependent: :destroy
  has_paper_trail

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] }, unique_translation: true
  validates :description_translations, presence: true, translation: { required_keys: [:ar, :en] }
  validates_datetime :start_date, on_or_after: (-> { Date.today  }),
                     if: :start_date_changed?

  validates_datetime :end_date,  on_or_after: (-> (e){ e.start_date }),
                      allow_nil: true, if: :end_date_changed?

  validates_datetime :last_cancel, before: (-> (e){ e.start_date }),
                     allow_nil: true, if: :last_cancel_changed?

  validates :capacity,  numericality: {
                        only_integer: true,
                        greater_than: 0
                      }, allow_nil: true

  ## --------------------- Call Backs --------------------- ##

  before_destroy :infrom_registered_users_of_destroy, prepend: true

  ## ---------------------- Scopes ------------------------ ##

  ## ------------------- Class Methods -------------------- ##
  # Get list of month dates that have events
  # @param [Integer] year
  # @param [Integer] month
  # @return [Array]  Array of dates
  def self.get_calender(year, month)
    from = Date.parse("#{year}-#{month}-01")
    to   = from.end_of_month
    Event.where('start_date between ? and ?', from, to).pluck(:start_date).uniq
  end

  ## ---------------------- Methods ----------------------- ##

  # Check the number of available tickets
  # @return [NilClass] if no capacity
  # @return [Integer]  if capacity is set
  def remaining
    capacity? ? (capacity - event_registrations.count) : nil
  end

  # Check if event registration still available
  # @return [Boolean]
  def can_register?
    start_date > Date.today && ( capacity? ? ( remaining > 0 && remaining <= capacity ) : true )
  end

  # Check if event registr cancelation still available
  # @return [Boolean]
  def can_cancel?
    last_cancel ? last_cancel >= Date.today : start_date > Date.today
  end

  # Inform all registered users about trip cancelation
  def infrom_registered_users_of_destroy
    event_registrations.each do |register|
      EventRegistrationMailer.inform_user_of_delete(register.employee_id, title_en, register.locale).deliver_later
      %w(ar en).each do |locale|
        message = [I18n.t('notifications.event.cancel', title: send("title_#{locale}"), locale: locale.to_sym)]
        # used ASHANAK push type to ignore any action by mobile
        Resque.enqueue(Publisher, register.employee_id, message, 'ASHANAK', self.id, self.id, locale)
      end
    end
  end

  def export_registered_users
    CSV.generate(encoding: 'UTF-8') do |csv|
      csv << [
        'Employee Id',
        'Employee Full Name',
        'Employee Phone Number',
        'Employee Email',
        'Register Code',
        'Registration Time',
      ]
      event_registrations.includes(:employee)
                        .order(created_at: :asc)
                        .each do |register|
        csv << [
          register.employee.id,
          register.employee.full_name,
          register.employee.phone_number,
          register.employee.email,
          register.register_code,
          register.created_at.in_time_zone('Riyadh').strftime('%F %r'),
        ]
      end
    end
  end
end
