# frozen_string_literal: true

# == Schema Information
#
# Table name: ashanaks
#
#  id                  :integer          not null, primary key
#  region_id           :integer          not null
#  occasion_id         :integer          not null
#  employee_id         :integer          not null
#  work_location_id    :integer          not null
#  date                :date             not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  phone_number        :string           default(""), not null
#  address             :text             default(""), not null
#  super_admin_approve :integer          default("pending"), not null
#  sub_admin_approve   :integer          default("pending"), not null
#  super_admin_comment :text
#  sub_admin_comment   :text
#  locale              :string           default("en"), not null
#

class Ashanak < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include AshanakPresenter
  include Localeable

  attr_accessor :admin_id

  ## -------------------- Associations -------------------- ##
  belongs_to :region
  belongs_to :work_location
  belongs_to :occasion
  belongs_to :employee
  has_many :attachments, class_name: 'AshanakAttachement', dependent: :destroy
  accepts_nested_attributes_for :attachments

  ## --------------------- Callbacks ---------------------- ##

  after_create_commit :send_email_after_ashanak_request
  after_update_commit :inform_user
  after_update_commit :infrom_super_admins

  ## -------------------- Validations --------------------- ##

  validates :phone_number, presence: true, length: { minimum: 8, maximum: 14 }, numericality: { only_integer: true }, on: :create
  validates :address,      presence: true
  validates :date,         presence: true
  # validates_datetime :date,  on_or_after: (-> { (Date.today  - 30.days) }), if: proc {|d| d.present?}
  validates :super_admin_comment, presence: true, if: -> { super_admin_approve_changed? && super_admin_rejected? }
  validates :sub_admin_comment,   presence: true, if: -> { sub_admin_approve_changed?   && sub_admin_rejected?   }

  validate :ashanak_conditions, on: :create

  ## ----------------------- Enums ------------------------ ##

  enum super_admin_approve: {
    pending:  0, # When User create will be pending
    accepted: 1, # After super_admin Approved the request will chenge to accepted
    rejected: 2  # If super_admin Rejected the request will be rejected
  }, _prefix: :super_admin

  enum sub_admin_approve: {
    pending:  0, # When User create will be pending
    accepted: 1, # After sub_admin Approved the request will chenge to accepted
    rejected: 2  # If sub_admin Rejected the request will be rejected
  }, _prefix: :sub_admin

  ## ------------------- Class Methods -------------------- ##

  def self.pending_counter(role)
    case role.to_s
    when 'admin', 'admin2'
      super_admin_pending.count
    when 'sub_admin'
      sub_admin_pending.super_admin_pending.count
    else
      0
    end
  end

  ## --------------------- Exporters ---------------------- ##

  def self.exportable_headers
    [
      'Testahil ID',
      'Employee ID',
      'MOH ID',
      'Full Name',
      'Phone Number',
      'Email',
      'Region',
      'City',
      'Occasion',
      'Contact Phnoe',
      'Address',
      'Date',
      'Super Admin Status',
      'Sub Admin Status',
      'Super Admin Comment',
      'Sub Admin Comment',
      'Create Time'
    ]
  end

  def exportable_data
    [
      id,
      employee.id,
      employee.employee_id,
      employee.full_name,
      employee.phone_number,
      employee.email,
      region.title_en,
      work_location.title_en,
      occasion.title_en,
      phone_number,
      address,
      date.strftime("%Y-%m-%d"),
      super_admin_approve&.titleize,
      sub_admin_approve&.titleize,
      super_admin_comment,
      sub_admin_comment,
      created_at.in_time_zone('Riyadh').strftime('%F %r')
    ]
  end

  ## ---------------------- Methods ----------------------- ##

  # inform the user only if the status changed and it became approved or rejected
  def inform_user
    return unless (saved_change_to_super_admin_approve? || saved_change_to_sub_admin_approve?) && (approved? || rejected?)
    push_notification
    send_ashanak_email_to_user
  end

  # Push the notification to client device
  def push_notification
    %w[ar en].each do |locale|
      message = [I18n.t('notifications.ashanak.status', title: occasion.send("title_#{locale}"), status: I18n.t(status, locale: locale.to_sym), locale: locale.to_sym)]
      Resque.enqueue(Publisher, employee_id, message, 'ASHANAK', id, id, locale)
    end
  end

  # Send emails to Sub Admins for every request
  def send_email_after_ashanak_request
    Employee.sub_admin.each do |emp|
      AshanakMailer.ashanak_email_to_admin(id, emp.id).deliver_later
    end
  end

  def send_ashanak_email_to_user
    AshanakMailer.send_approval_email(id).deliver_later
  end

  # Send email to Super Admin when sub admin accept registration
  def infrom_super_admins
    return unless saved_change_to_sub_admin_approve? && super_admin_pending? && sub_admin_accepted?
    Employee.admin.or(Employee.admin2).each do |emp|
      AshanakMailer.inform_super_admin(id, emp.id, admin_id).deliver_later
    end
  end

  # Is the ashanak request approved by super admin ? ( and not rejected by sub admin )
  def approved?
    super_admin_accepted? && !sub_admin_rejected?
  end

  # Is the ashanak request rejected by super or sub admins ?
  def rejected?
    sub_admin_rejected? || super_admin_rejected?
  end

  # Check the status of the trip registration
  def status
    if approved?
      :approved
    elsif rejected?
      :rejected
    else
      :pending
    end
  end

  # Merge sub admin and super admin comments into one string
  def comments
    [super_admin_comment, sub_admin_comment].compact.join(', ')
  end

  # Check Ashanak roles and conditions
  def ashanak_conditions
    errors.add(:base, :accepted_with_same_occasion) if same_occasion_accepted_within(2).exists?
    errors.add(:base, :pending_with_same_occasion)  if same_occasion_pending.exists?
    errors.add(:base, :rejected_with_same_occasion) if same_occasion_rejected.count >= 3
  end

  # Find same employee with same occasion
  def same_employee_and_occasion
    self.class
    .where(occasion_id: occasion_id)
    .where(employee_id: employee_id)
  end

  # Check if smae employee have same request accepted for same occasian within last X years
  # Task : Prevent user to apply for 2 years in case he has approved request in the same occasion
  def same_occasion_accepted_within(years_count = 2)
    same_employee_and_occasion.where(created_at: years_count.years.ago..Time.now)
                              .super_admin_accepted
  end

  # Check if employee have smae occasion pending
  # Task: Prevent user to apply in the same occasion in case he has pending request in the same occasion.
  def same_occasion_pending
    same_employee_and_occasion.super_admin_pending
  end

  # Check rejected requests of same occasion
  # Task: prevent the user to apply in the same occasion in the same month if he has 3 rejected request in the same month and same occasion.
  def same_occasion_rejected
    same_employee_and_occasion.super_admin_rejected
                              .or(same_employee_and_occasion.sub_admin_rejected)
                              .where(created_at: 1.month.ago..Time.now)
  end
end
