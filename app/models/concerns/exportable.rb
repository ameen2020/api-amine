# frozen_string_literal: true

module Exportable
  extend ActiveSupport::Concern

  class_methods do
    def exportable_headers
      column_names.map(&:titleize)
    end
  end

  def exportable_data
    as_json[model_name.singular].values
  end
end
