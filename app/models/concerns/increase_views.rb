module IncreaseViews

  def increase_views!
    self.update_column(:views_counter, views_counter + 1)
  end

end
