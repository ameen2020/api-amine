module OtpAuth
  extend ActiveSupport::Concern

  # OTP Configs
  OTP_EXPIRE_TIME  = 5.minutes        # The expiration time of token
  OTP_MOBILE_FIELD = :phone_number    # the filed of the mobile number
  OTP_CODE_FIELD   = :otp             # the filed of the otp code
  OTP_EXP_FIELD    = :otp_expire_on   # the field of the time expiration of otp

  included do
    # before_create        :generate_otp
    # after_create_commit  :send_otp!
  end

  module ClassMethods
    # Get OTP
    # find the users by mobile number, generate and send the otp if user found
    # @return [object]
    def get_otp(mobile_number)
      find_by(OTP_MOBILE_FIELD => mobile_number)&.generate_and_send_otp! || false
    end

    # Confirm User OTP by finding the user with mobile number and OTP
    # @return [object]
    def confirm_otp(mobile_number, otp_code)
      user = find_by(OTP_MOBILE_FIELD => mobile_number, OTP_CODE_FIELD => otp_code)
      return false unless user&.valid_otp?

      # Clear the otp from db
      user.clear_otp!

      # activate the user if his status is pending
      user.active! if user.pending?

      user
    end
  end

  # Generate The OTP Token
  def generate_otp
    self[OTP_CODE_FIELD] = rand(1000...9999)
    self[OTP_EXP_FIELD]  = Time.zone.now + OTP_EXPIRE_TIME
    self
  end

  # Send the otp to the client by SMS
  def send_otp!
    # The SMS logic will be implemented later
    # Nofifier::SMS.send(:otp_sms, self.mobile_number, self.otp)
    Rails.logger.debug("\e[46;1mSEND OTP #{self[OTP_CODE_FIELD]} TO THE CLIEND #{id} : #{self[OTP_MOBILE_FIELD]}\e[0m")
    true
  end

  # generate the otp and send it to the client
  def generate_and_send_otp!
    generate_otp && save! && send_otp! && self
  end

  # Check if the OTP is still valid?
  def valid_otp?
    self[OTP_EXP_FIELD] >= Time.zone.now
  end

  # Clear the OTP from db and save the record
  def clear_otp!
    self[OTP_CODE_FIELD] = nil
    self[OTP_EXP_FIELD]  = nil
    self.save!(validate: false)
    self
  end
end
