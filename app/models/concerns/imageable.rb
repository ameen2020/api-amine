module Imageable
  extend ActiveSupport::Concern

  included do

    has_many :pictures, as: :imageable, dependent: :destroy
    has_one  :cover_picture, -> { cover }, class_name: 'Picture', as: :imageable, dependent: :destroy
    accepts_nested_attributes_for :pictures
    accepts_nested_attributes_for :cover_picture
  end

  def images(size = nil)
    pictures.map{|i| i.image_url(size)  }
  end

  def cover(size = :thumb)
    cover_picture&.image_url(size) || default_cover(size)
  end

  def default_cover(size = nil)
    images(size).first || URI.join(ActionController::Base.asset_host, 'default_logo.jpg').to_s
  end

  def set_cover(id)
    pictures.transaction do
      pictures.cover.update(cover: false)
      return true if pictures.find(id).update(cover: true)
      false
    end
  end

  def remove_picture(id)
    return true if pictures.find(id).destroy
    false
  end
end
