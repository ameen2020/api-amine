module Localeable
  extend ActiveSupport::Concern

  included do
    before_validation :set_request_locale, on: :create
  end

  def set_request_locale
    self.locale = I18n.locale.to_s
  end
end