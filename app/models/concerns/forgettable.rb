module Forgettable
  PASS_TOKEN_EXP = 1.hour

  extend ActiveSupport::Concern
  included do
    validates :reset_password_token, uniqueness: true, if: :reset_password_token
  end

  # Process forgot password
  def forgot_password(send_mail = true)
    self.reset_password_token = loop do
      password_token = SecureRandom.hex(10)
      break password_token unless self.class.exists?(reset_password_token: password_token)
    end

    send_forgot_password_email(self.reset_password_token) if send_mail
    self.reset_password_sent_at = Time.zone.now

    save
  end

  def password_token_valid?
    self.reset_password_token.present? && self.reset_password_sent_at + PASS_TOKEN_EXP > Time.zone.now
  end

  def clean_reset_password!
    self.reset_password_token   = nil
    self.reset_password_sent_at = nil
    self.save
  end

  # Send forgot password email to user
  def send_forgot_password_email(token)
    EmployeeMailer.forgot_password(self.id, token).deliver_later
  end
end
