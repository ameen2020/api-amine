module SendNotifiactionAdmin
  extend ActiveSupport::Concern

  included do
    ## ---------------- CallBack --------------- ##

    attr_accessor :current_user

    ## ---------------- CallBack --------------- ##

    after_create_commit :create_notifiaction_admin

    ## ---------------- Associations ----------- ##

    has_one :admin_notification, as: :noticeable, dependent: :destroy
  end

  def create_notifiaction_admin
    create_admin_notification(employee_id: current_user&.id) if current_user.present?
  end
end
