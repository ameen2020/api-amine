module AttachmentType
  extend ActiveSupport::Concern

  ## ---------------------- AttachmentType ----------------------- ##
  included do
    has_attached_file :attachment, styles: { thumb: '200x200>' },
                                   url:   "/upload/:class/:id/:style/:filename",
                                   path:  ':rails_root/public:url'

    validates_attachment_content_type :attachment, content_type: [
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      'application/vnd.ms-powerpointtd',
      'application/vnd.ms-excel',
      'application/vnd.ms-access',
      'Application/vnd.ms-word',
      'application/msword',
      'application/pdf',
      'image/png',
      'image/jpg',
      'image/jpeg',
      'text/plain',
      'image/gif'
    ]
  end

  def attachment_url
    URI.join(ActionController::Base.asset_host, attachment.url).to_s
  end
end
