# frozen_string_literal: true

# This module extends the functinality of JsonTranslate Gem And
# Create Search Scopes for Ransack Gem to search in all Translatable Attributes
module SearchableTranslation
  extend ActiveSupport::Concern

  included do
    scope :translations_cont, lambda { |value|
      where(
        translated_attribute_names.map do |f|
          "lower(#{f}_translations::jsonb::text) ILIKE :value"
        end.compact.join(' OR '),
        value: "%#{value.downcase}%"
      )
    }

    # find with case-insensitive title_transulation
    scope :title_translation_cont, lambda { |value, locale = I18n.locale|
      translation_hash = { locale => value.downcase }
      where('lower(title_translations::text)::jsonb @> :translation::jsonb', translation: translation_hash.to_json)
    }
  end
end
