module Notifiable
  extend ActiveSupport::Concern

  included do
    after_create_commit :push_notification
  end

  def push_notification
    Resque.enqueue(PushNotificationWorker, self.model_name.singular, id)
  end
end
