module CreatedBy

  def created_by
    Employee.find(record_created.whodunnit) if record_created
  end

  def record_created
    versions.find_by(event: 'create')
  end

end
