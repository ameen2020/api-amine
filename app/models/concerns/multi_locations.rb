module MultiLocations
  extend ActiveSupport::Concern

  included do
    validate  :locations_format
  end

  def locations_format
    errors.add(:locations, :locations_should_be_array) unless locations.is_a?(Array)
    errors.add(:locations, :one_location_at_least)     if locations&.empty?
    # errors.add(:locations, :locations_should_contain_lat_long) if
  end
end