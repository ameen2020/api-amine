# == Schema Information
#
# Table name: feedback_categories
#
#  id                 :integer          not null, primary key
#  title_translations :jsonb            not null
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class FeedbackCategory < ApplicationRecord
  ## -------------------- Requirements --------------------- ##

  translates :title
  include FeedbackCategoryPresenter
  include SearchableTranslation

  ## --------------------- Association --------------------- ##

  has_many :feedbacks, class_name: 'OfferFeedback', foreign_key: :category_id ,dependent: :restrict_with_error

  ## --------------------- Validations --------------------- ##

  validates :title_translations, presence: true, translation: { required_keys: %i[ar en] }, unique_translation: true
end
