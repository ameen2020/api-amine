# == Schema Information
#
# Table name: trip_registrations
#
#  id                  :integer          not null, primary key
#  employee_id         :integer          not null
#  trip_id             :integer          not null
#  relationship_id     :integer
#  full_name           :string
#  id_number           :integer
#  companion           :boolean          default(FALSE), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  register_code       :string           not null
#  super_admin_approve :integer          default("pending"), not null
#  sub_admin_approve   :integer          default("pending"), not null
#  super_admin_comment :text
#  sub_admin_comment   :text
#  locale              :string           default("en"), not null
#

class TripRegistration < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include TripRegistrationPresenter
  include Localeable

  ## -------------------- Association --------------------- ##

  belongs_to :trip
  belongs_to :employee
  belongs_to :relationship, optional: true

  has_many :attachments, class_name: 'TripRegistrationAttachment', dependent: :destroy
  accepts_nested_attributes_for :attachments

  ## -------------------- Validations --------------------- ##

  validates :relationship_id, presence: true, if: :companion
  validates :full_name, presence: true, if: :companion
  validates :id_number, presence: true, if: :companion

  validates :trip_id, presence: true
  validates :employee_id, presence: true
  validates :employee_id, uniqueness: { scope: :trip_id, message: :already_registered}
  validates :super_admin_comment, presence: true, if: -> { super_admin_approve_changed? && super_admin_rejected? }
  validates :sub_admin_comment,   presence: true, if: -> { sub_admin_approve_changed?   && sub_admin_rejected?   }

  validate :is_trip_registrable?, on: :create

  ## ---------------------- Call Backs ----------------------- ##

  before_validation   :generate_random_code
  after_update_commit :inform_user
  after_update_commit :infrom_super_admins
  after_create_commit :inform_admins
  after_destroy_commit :inform_admins_of_cancelation

  ## ------------------------ Enums -------------------------- ##

  enum super_admin_approve: {
    pending:  0, # When User register will be pending
    accepted: 1, # After super_admin Approved the request will chenge to accepted
    rejected: 2  # If super_admin Rejected the request will be rejected
  }, _prefix: :super_admin

  enum sub_admin_approve: {
    pending:  0, # When User register will be pending
    accepted: 1, # After sub_admin Approved the request will chenge to accepted
    rejected: 2  # If sub_admin Rejected the request will be rejected
  }, _prefix: :sub_admin

  ## ------------------- Class Methods -------------------- ##

  def self.pending_counter(role)
    case role.to_s
    when "admin", "admin2"
      super_admin_pending.count
    when "sub_admin"
      sub_admin_pending.super_admin_pending.count
    else
      0
    end
  end

  ## ---------------------- Methods ----------------------- ##

  # Validate if employee can register to the trip
  def is_trip_registrable?
    return errors.add(:base, :cant_register) if is_employee_in_black_list?
    errors.add(:base, :cant_register) unless trip&.can_register?
  end

  # Is the registration approved by super admin ? ( and not rejected by sub admin )
  def approved?
    self.super_admin_accepted? && !self.sub_admin_rejected?
  end

  # Is the trip rejected by super or sub admins ?
  def rejected?
    self.sub_admin_rejected? || self.super_admin_rejected?
  end

  # Check the status of the trip registration
  def status
    if approved?
      :approved
    elsif rejected?
      :rejected
    else
      :pending
    end
  end

  # Generate rondom code for employee who registered on particuler trip
  def generate_random_code
    self.register_code = loop do
      random_code = SecureRandom.hex(3).upcase
      break random_code unless self.class.exists?(register_code: random_code)
    end if register_code.nil?
  end

  # inform the user only if the status changed and it became approved or rejected
  def inform_user
    if (saved_change_to_super_admin_approve? || saved_change_to_sub_admin_approve?) && (approved? || rejected?)
      push_notification
      send_email_after_approval
    end
  end

  # Send Push Notification to the user
  def push_notification
    %w(ar en).each do |locale|
      message = [I18n.t('notifications.trip.status', title: self.trip.send("title_#{locale}"), status: I18n.t(status, locale: locale.to_sym), locale: locale.to_sym)]
      Resque.enqueue(Publisher, self.employee_id, message, 'TRIPS', self.trip_id, self.id, locale)
    end
  end

  # Send Email After admin Approval or rejected
  def send_email_after_approval
    TripRegistrationMailer.send_approval_email(self.id).deliver_later
  end

  # Send reminder email
  def send_reminder_email(days)
    TripRegistrationMailer.reminder(self.id, days).deliver_later
  end

  # push reminder notification
  def push_reminder_notification(days)
    %w(ar en).each do |locale|
      message = [I18n.t('notifications.trip.reminder', title: self.trip.send("title_#{locale}"), count: days, locale: locale.to_sym)]
      Resque.enqueue(Publisher, self.employee_id, message, 'TRIPS', self.trip_id, nil, locale)
    end
  end

  # Send emails to Sub Admins for every registration
  def inform_admins
    Employee.sub_admin.each do |emp|
      TripRegistrationMailer.new_registration(self.id, emp.id).deliver_later
    end
  end

  # Send email to Super Admin when sub admin accept registration
  def infrom_super_admins
    Employee.admin.or(Employee.admin2).each do |emp|
      TripRegistrationMailer.inform_super_admin(self.id, emp.id).deliver_later
    end if saved_change_to_sub_admin_approve? && super_admin_pending? && sub_admin_accepted?
  end

  # Infrom all admins when user cancel the registration
  def inform_admins_of_cancelation
    Employee.where.not(role: 1).each do |emp|
      TripRegistrationMailer.cancel_by_user(emp.id, self.employee_id, self.trip_id, JSON.parse(self.to_json)['trip_registration']).deliver_later
    end
  end

  # Convert the comments into one string
  def comments
    [super_admin_comment, sub_admin_comment].compact.join(', ')
  end

  def is_employee_in_black_list?
    TripBlackList.exists?(employee_id: employee_id)
  end

  ## --------------------- Exporters ---------------------- ##

  def self.exportable_headers
    [
      'Employee Id',
      'Employee Full Name',
      'Employee Phone Number',
      'Employee Email',
      'Register Code',
      'Companion',
      'Companion Relationship',
      'Companion Full Name',
      'Companion Id Number',
      'Super Admin Approve',
      'Sub Admin Approve',
      'Super Admin Comment',
      'Sub Admin Comment',
      'Registration Time'
    ]
  end

  def exportable_data
    [
      employee.id,
      employee.full_name,
      employee.phone_number,
      employee.email,
      register_code,
      companion,
      relationship&.title_en,
      full_name,
      id_number,
      super_admin_approve&.titleize,
      sub_admin_approve&.titleize,
      super_admin_comment,
      sub_admin_comment,
      created_at.in_time_zone('Riyadh').strftime('%F %r')
    ]
  end
end
