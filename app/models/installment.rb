# == Schema Information
#
# Table name: installments
#
#  id                  :integer          not null, primary key
#  nationality_id      :integer          not null
#  employee_id         :integer          not null
#  company_name        :string           not null
#  locale              :string           default("en"), not null
#  super_admin_approve :integer          default("pending"), not null
#  sub_admin_approve   :integer          default("pending"), not null
#  super_admin_comment :text
#  sub_admin_comment   :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Installment < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  include InstallmentPresenter
  include Localeable

  attr_accessor :admin_id
  ## -------------------- Associations -------------------- ##
  belongs_to :nationality
  belongs_to :employee
  has_many :attachments, class_name: 'InstallmentAttachment', dependent: :destroy
  accepts_nested_attributes_for :attachments

  ## --------------------- Callbacks ---------------------- ##
  after_create_commit :send_email_after_installment_request
  after_update_commit :inform_user
  after_update_commit :infrom_super_admins

  ## -------------------- Validations --------------------- ##
  validates :super_admin_comment, presence: true, if: -> { super_admin_approve_changed? && super_admin_rejected? }
  validates :sub_admin_comment,   presence: true, if: -> { sub_admin_approve_changed?   && sub_admin_rejected?   }

  ## ----------------------- Enums ------------------------ ##
  enum super_admin_approve: {
    pending:  0, # When User create will be pending
    accepted: 1, # After super_admin Approved the request will chenge to accepted
    rejected: 2  # If super_admin Rejected the request will be rejected
  }, _prefix: :super_admin

  enum sub_admin_approve: {
    pending:  0, # When User create will be pending
    accepted: 1, # After sub_admin Approved the request will chenge to accepted
    rejected: 2  # If sub_admin Rejected the request will be rejected
  }, _prefix: :sub_admin

  ## ------------------- Class Methods -------------------- ##

  def self.pending_counter(role)
    case role.to_s
    when 'admin', 'admin2'
      super_admin_pending.count
    when 'sub_admin'
      sub_admin_pending.super_admin_pending.count
    else
      0
    end
  end

  def inform_user
    return unless (saved_change_to_super_admin_approve? || saved_change_to_sub_admin_approve?) && (approved? || rejected?)
      push_notification
    send_installment_email_to_user
  end

  # Push the notification to client device
  def push_notification
    %w[ar en].each do |locale|
      message = [I18n.t('notifications.installment.status', title: send("company_name"), status: I18n.t(status, locale: locale.to_sym), locale: locale.to_sym)]
      Resque.enqueue(Publisher, employee_id, message, 'INSTALLMENT', id, id, locale)
    end
  end

  # Send emails to Sub Admins for every request
  def send_email_after_installment_request
    Employee.sub_admin.each do |emp|
      InstallmentMailer.installment_email_to_admin(id, emp.id).deliver_later
    end
  end

  def send_installment_email_to_user
    InstallmentMailer.send_approval_email(id).deliver_later
  end

  # Send email to Super Admin when sub admin accept registration
  def infrom_super_admins
    return unless saved_change_to_sub_admin_approve? && super_admin_pending? && sub_admin_accepted?
    Employee.admin.or(Employee.admin2).each do |emp|
      InstallmentMailer.inform_super_admin(id, emp.id, admin_id).deliver_later
    end
  end

  # Is the installment request approved by super admin ? ( and not rejected by sub admin )
  def approved?
    super_admin_accepted? && !sub_admin_rejected?
  end

  # Is the installment request rejected by super or sub admins ?
  def rejected?
    sub_admin_rejected? || super_admin_rejected?
  end

  def status
    if approved?
      :approved
    elsif rejected?
      :rejected
    else
      :pending
    end
  end



  # Merge sub admin and super admin comments into one string
  def comments
    [super_admin_comment, sub_admin_comment].compact.join(', ')
  end

end
