# == Schema Information
#
# Table name: ashanak_attachements
#
#  id                      :integer          not null, primary key
#  ashanak_id              :integer          not null
#  attachment_file_name    :string
#  attachment_content_type :string
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  by_admin                :boolean          default(FALSE), not null
#

class AshanakAttachement < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include AshanakAttachementPresenter
  include AttachmentType

  ## -------------------- Associations -------------------- ##

  belongs_to :ashanak

  ## -------------------- Validations --------------------- ##

  validates_attachment_presence :attachment

  ## ---------------------- Scopes ------------------------ ##

  scope :by_admins, -> { where(by_admin: true) }

  ## ---------------------- Methods ----------------------- ##

end
