# == Schema Information
#
# Table name: notifications_histories
#
#  id              :integer          not null, primary key
#  employee_id     :integer          not null
#  notifiable_id   :integer          not null
#  notifiable_type :string           not null
#  read            :boolean          default("unseen")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class NotificationsHistory < ApplicationRecord

  ## -------------------- Requirements -------------------- ##

  include NotificationsHistoryPresenter
  attr_accessor :category

  ## --------------------- Callbacks ---------------------- ##

  after_create_commit :push_notification

  # ---------------------- Association ------------------- #

  belongs_to :notifiable, polymorphic: true, optional: true
  belongs_to :employee

  ## ----------------------- Enums ------------------------ ##

  enum read: {
    seen:   true,
    unseen: false
  }

  ## ---------------------- Methods ----------------------- ##

  # Push the notification to client device
  def push_notification
    %w(ar en).each do |locale|
      message = check_notifiable_type(locale)
      Resque.enqueue(Publisher, self.employee_id, message, push_type, self.notifiable_id, self.id, locale)
    end
  end

  # Get the push_type from notifiable_type
  def push_type
    case notifiable_type
    when 'Event'            then 'EVENTS'
    when 'Offer'            then 'OFFERS'
    when 'Trip'             then 'TRIPS'
    when 'New'              then 'NEWS'
    when 'Memo'             then 'MEMOS'
    when 'Assignment'       then 'JOBS'
    when 'Magazin'          then 'MAGAZINE'
    when 'Chalet'           then 'CHALET'
    when 'Mbadrat'          then 'MBADRAT'
    when 'EmployeeVoice'    then 'EMPLOYEEVOICE'
    when 'Installment'      then 'INSTALLMENT'
    when 'Message'          then 'MESSAGE'
    end
  end

  # Category name
  def category_name
    @category ||= Category.find_by(category_constant: push_type)
    {
      en: (category&.title_en || '-'),
      ar: (category&.title_ar || '-')
    }
  end

  def check_notifiable_type(locale)
    push_type == 'MESSAGE' ? [notifiable.message] : [I18n.t('notifications.new', title: notifiable.send("title_#{locale}"), category: category_name[locale.to_sym], locale: locale.to_sym)]
  end
end
