# == Schema Information
#
# Table name: offer_attachments
#
#  id                      :integer          not null, primary key
#  offer_id                :integer          not null
#  attachment_file_name    :string
#  attachment_content_type :string
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class OfferAttachment < ApplicationRecord

  ## -------------------- Requirements -------------------- ##
  include OfferAttachmentPresenter
  include AttachmentType

  ## ---------------------- Associations ----------------------- ##
  belongs_to :offer

  ## -------------------- Validations --------------------- ##
  validates_attachment_presence :attachment
end
