# == Schema Information
#
# Table name: relationships
#
#  id                 :integer          not null, primary key
#  title_translations :jsonb            not null
#  status             :boolean          default(TRUE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Relationship < ApplicationRecord
  ## -------------------- Requirements --------------------- ##
  include RelationshipPresenter
  translates :title

  ## -------------------- Association --------------------- ##
  has_many :trip_registrations, dependent: :restrict_with_error

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] },
            unique_translation: true
end
