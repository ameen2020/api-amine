# == Schema Information
#
# Table name: chalet_registrations
#
#  id                      :integer          not null, primary key
#  employee_id             :integer          not null
#  chalet_id               :integer          not null
#  register_code           :string
#  attachment_file_name    :string
#  attachment_content_type :string
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#  locale                  :string           default("en"), not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  super_admin_approve     :integer          default("pending"), not null
#  sub_admin_approve       :integer          default("pending"), not null
#  super_admin_comment     :text
#  sub_admin_comment       :text
#

class ChaletRegistration < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include ChaletRegistrationPresenter
  include AttachmentType
  include Localeable

  ## -------------------- Associations -------------------- ##
  belongs_to :employee
  belongs_to :chalet

  ## --------------------- Callbacks ---------------------- ##

  before_destroy :valid_for_cancel?
  after_create_commit :send_registration_code_email
  after_create_commit :inform_admins
  after_update_commit :inform_user
  after_update_commit :infrom_super_admins
  before_validation :generate_random_code
  after_destroy_commit :inform_admins_of_cancelation

  ## -------------------- Validations --------------------- ##
  validates :employee_id, presence: true
  validates :register_code, presence: true, uniqueness: true
  validates :chalet_id,    presence: true
  validates :employee_id, uniqueness: { scope: :chalet_id, message: :already_registered}
  validates :super_admin_comment, presence: true, if: -> { super_admin_approve_changed? && super_admin_rejected? }
  validates :sub_admin_comment,   presence: true, if: -> { sub_admin_approve_changed?   && sub_admin_rejected?   }
  validate :is_chalet_registrable?, on: :create


  ## ------------------------ Enums -------------------------- ##

  enum super_admin_approve: {
    pending:  0, # When User register will be pending
    accepted: 1, # After super_admin Approved the request will chenge to accepted
    rejected: 2  # If super_admin Rejected the request will be rejected
  }, _prefix: :super_admin

  enum sub_admin_approve: {
    pending:  0, # When User register will be pending
    accepted: 1, # After sub_admin Approved the request will chenge to accepted
    rejected: 2  # If sub_admin Rejected the request will be rejected
  }, _prefix: :sub_admin

  ## ------------------- Class Methods -------------------- ##\

  def self.pending_counter(role)
    case role.to_s
    when "admin", "admin2"
      super_admin_pending.count
    when "sub_admin"
      sub_admin_pending.super_admin_pending.count
    else
      0
    end
  end

  ## ---------------------- Methods ----------------------- ##

  def is_chalet_registrable?
    errors.add(:base, :cant_register) unless chalet&.can_register?
  end

  # Is the registration approved by super admin ? ( and not rejected by sub admin )
  def approved?
    self.super_admin_accepted? && !self.sub_admin_rejected?
  end

  # Is the trip rejected by super or sub admins ?
  def rejected?
    self.sub_admin_rejected? || self.super_admin_rejected?
  end

  # Check the status of the trip registration
  def status
    if approved?
      :approved
    elsif rejected?
      :rejected
    else
      :pending
    end
  end

  def valid_for_cancel?
    unless chalet.can_cancel?
      errors.add(:base, :cant_cancel)
      throw :abort
    end
  end

  def generate_random_code
    self.register_code = loop do
      random_code = SecureRandom.hex(3).upcase
      break random_code unless self.class.exists?(register_code: random_code)
    end if register_code.nil?
  end

  def send_registration_code_email
    ChaletRegistrationMailer.send_registration_code(self.id).deliver_later
  end

  def send_reminder_email(days)
    ChaletRegistrationMailer.reminder(self.id, days).deliver_later
  end

  # Send emails to Sub Admins for every registration
  def inform_admins
    Employee.sub_admin.each do |emp|
      ChaletRegistrationMailer.new_registration(self.id, emp.id).deliver_later
    end
  end



  # inform the user only if the status changed and it became approved or rejected
  def inform_user
    if (saved_change_to_super_admin_approve? || saved_change_to_sub_admin_approve?) && (approved? || rejected?)
      push_notification
      send_email_after_approval
    end
  end

  # Send Email After admin Approval or rejected
  def send_email_after_approval
    ChaletRegistrationMailer.send_approval_email(self.id).deliver_later
  end

  # Send reminder email
  def send_reminder_email(days)
    ChaletRegistrationMailer.reminder(self.id, days).deliver_later
  end

  # Send Push Notification to the user
  def push_notification
    %w(ar en).each do |locale|
      message = [I18n.t('notifications.chalet.status', title: self.chalet.send("title_#{locale}"), status: I18n.t(status, locale: locale.to_sym), locale: locale.to_sym)]
      Resque.enqueue(Publisher, self.employee_id, message, 'CHALET', self.chalet_id, self.id, locale)
    end
  end





  def push_reminder_notification(days)
    %w(ar en).each do |locale|
      message = [I18n.t('notifications.chalet.reminder', title: chalet.send("title_#{locale}"), count: days, locale: locale.to_sym)]
      Resque.enqueue(Publisher, self.employee_id, message, 'CHALET', self.chalet_id, nil, locale)
    end
  end




  # Send emails to Sub Admins for every registration
  def inform_admins
    Employee.sub_admin.each do |emp|
      ChaletRegistrationMailer.new_registration(self.id, emp.id).deliver_later
    end
  end

  # Send email to Super Admin when sub admin accept registration
  def infrom_super_admins
    Employee.admin.or(Employee.admin2).each do |emp|
      ChaletRegistrationMailer.inform_super_admin(self.id, emp.id).deliver_later
    end if saved_change_to_sub_admin_approve? && super_admin_pending? && sub_admin_accepted?
  end




  # Infrom all admins when user cancel the registration
  def inform_admins_of_cancelation
    Employee.where.not(role: 1).each do |emp|
      ChaletRegistrationMailer.cancel_by_user(emp.id, self.employee_id, self.chalet_id, JSON.parse(self.to_json)['chalet_registration']).deliver_later
    end
  end

  # Convert the comments into one string
  def comments
    [super_admin_comment, sub_admin_comment].compact.join(', ')
  end

  ## --------------------- Exporters ---------------------- ##

  def self.exportable_headers
    [
      'Employee Id',
      'Employee Full Name',
      'Employee Phone Number',
      'Employee Email',
      'Register Code',
      'Super Admin Approve',
      'Sub Admin Approve',
      'Super Admin Comment',
      'Sub Admin Comment',
      'Registration Time',
    ]
  end

  def exportable_data
    [
      employee.id,
      employee.full_name,
      employee.phone_number,
      employee.email,
      register_code,
      super_admin_approve&.titleize,
      sub_admin_approve&.titleize,
      super_admin_comment,
      sub_admin_comment,
      created_at.in_time_zone('Riyadh').strftime('%F %r')
    ]
  end
end
