# == Schema Information
#
# Table name: messages
#
#  id         :integer          not null, primary key
#  sender_id  :integer          not null
#  title      :string           not null
#  message    :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Message < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
    include MessagePresenter
    # include Notifiable
    attr_accessor :all_employees, :employee_ids
  ## ----------------------- Scopes ----------------------- ##
  ## --------------------- Constants ---------------------- ##
  ## ----------------------- Enums ------------------------ ##
  ## -------------------- Associations -------------------- ##
  has_many    :notifications_histories, as: :notifiable, dependent: :destroy
  belongs_to  :sender,    class_name: 'Employee'
  ## -------------------- Validations --------------------- ##
  validates :title,   presence: true
  validates :message, presence: true
  ## --------------------- Callbacks ---------------------- ##
  ## ------------------- Class Methods -------------------- ##
  after_create :check_all_employees_or_employee_ids
  ## ---------------------- Methods ----------------------- ##

  def check_all_employees_or_employee_ids
    all_employees ? create_notifications_histories(employees) : create_notifications_histories(employee_ids)
  end

  def employees
    Employee.all.pluck(:id)
  end

  def create_notifications_histories(ids)
    Resque.enqueue(PushNotificationWorker, self.model_name.singular, id , ids)
  end
end
