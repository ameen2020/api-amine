# == Schema Information
#
# Table name: mbadrats
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  video                    :string
#  attachment_file_name     :string
#  attachment_content_type  :string
#  attachment_file_size     :integer
#  attachment_updated_at    :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  views_counter            :integer          default(0), not null
#

class Mbadrat < ApplicationRecord
  translates :title, :description
  include MbadratPresenter
  include Imageable
  include IncreaseViews
  include SearchableTranslation
  include AttachmentType
  include Notifiable
  include SendNotifiactionAdmin
  include CreatedBy

  ## -------------------- Associations -------------------- ##
  has_many :notifications_histories, as: :notifiable, dependent: :destroy
  has_many :mbadrat_faqs, dependent: :destroy
  accepts_nested_attributes_for :mbadrat_faqs, allow_destroy: true
  has_paper_trail

  ## -------------------- Validations --------------------- ##
  # validates_attachment_presence :attachment
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] }, unique_translation: true
  validates :description_translations, presence: true, translation: { required_keys: [:ar, :en] }
  validates :video, format: { with: /\Ahttps:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9_-]*)\z/i , message: I18n.t('activerecord.errors.messages.invalid_video')}, if: -> { video.present? }

  ## ------------------------ Methods ------------------------ ##
  def video_thumbnail
    return unless video.present?
    vurl = video&.gsub(/[^[:ascii:]]/,"").strip
    if vurl
      uri = URI.parse(vurl)
      if uri.host&.include?('youtube') && uri.query.present?
        params = CGI::parse(uri.query)
        return "https://i1.ytimg.com/vi/#{params['v'][0]}/0.jpg"
      end
    end
  end

end
