# == Schema Information
#
# Table name: offer_locations
#
#  id         :integer          not null, primary key
#  offer_id   :integer          not null
#  latitude   :decimal(10, 8)   default(0.0), not null
#  longitude  :decimal(11, 8)   default(0.0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class OfferLocation < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  include OfferLocationPresenter
  ## ---------------------- Association ------------------- ##
  belongs_to :offer

  ## -------------------- Validations --------------------- ##
  geocoded_by :full_street_address   # can also be an IP address
end
