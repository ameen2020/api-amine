# == Schema Information
#
# Table name: admin_notifications
#
#  id              :integer          not null, primary key
#  employee_id     :integer          not null
#  noticeable_id   :integer
#  noticeable_type :string
#  status          :integer          default("unseen"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class AdminNotification < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include AdminNotificationPresenter

  ## ----------------------- Scopes ----------------------- ##
  ## --------------------- Constants ---------------------- ##
  ## ----------------------- Enums ------------------------ ##

  enum status: {
    unseen: 0,
    seen:   1
  }

  ## -------------------- Associations -------------------- ##

  belongs_to :noticeable, polymorphic: true
  belongs_to :employee

  ## -------------------- Validations --------------------- ##
  ## --------------------- Callbacks ---------------------- ##

  after_create_commit :notify_admins

  ## ------------------- Class Methods -------------------- ##
  ## ---------------------- Methods ----------------------- ##

  def notify_admins
    Employee.where(role: [0, 3]).each do |admin|
      AdminNotifiactionMailer.new_post(admin, self).deliver_later
    end
  end
end
