# == Schema Information
#
# Table name: versions
#
#  id             :integer          not null, primary key
#  item_type      :string           not null
#  item_id        :integer          not null
#  event          :string           not null
#  whodunnit      :integer
#  object         :json
#  created_at     :datetime
#  object_changes :json
#

class Version < ApplicationRecord
  include PaperTrail::VersionConcern
  include VersionPresenter
  ## -------------------- Associations -------------------- ##
  belongs_to :employee, foreign_key: 'whodunnit',   optional: true
  ## -------------------- Validations  -------------------- ##
  ## -------------------- Methods      -------------------- ##

  def self.options(_user)
    {
      category: Version.pluck(:item_type).uniq,
      event:    ['create','update','destroy']
    }
  end

end
