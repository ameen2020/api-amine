# == Schema Information
#
# Table name: pages
#
#  id                   :integer          not null, primary key
#  title_translations   :jsonb            not null
#  content_translations :jsonb            not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Page < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  translates :title, :content
  include PagePresenter
  ## -------------------- Associations -------------------- ##
  ## -------------------- Validations --------------------- ##
  validates :title_translations,    presence: true, translation: { required_keys: [:ar, :en] }, unique_translation: true
  validates :content_translations,  presence: true, translation: { required_keys: [:ar, :en] }
  ## --------------------- Call Backs --------------------- ##
  ## ---------------------- Scopes ------------------------ ##
  ## ------------------- Class Methods -------------------- ##
  ## ---------------------- Methods ----------------------- ##

end
