# == Schema Information
#
# Table name: categories
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb
#  status                   :integer          default("active"), not null
#  category_constant        :string           not null
#  position                 :integer
#  category_color           :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  logo_file_name           :string
#  logo_content_type        :string
#  logo_file_size           :integer
#  logo_updated_at          :datetime
#

class Category < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  translates :title, :description
  include SearchableTranslation
  include CategoryPresenter

  has_attached_file :logo, styles: { thumb: '200x200>' },
                                 url: '/upload/:class/logo/:id/:style/:filename',
                                 path: ':rails_root/public:url',
                                 default_url: 'default_logo.jpg'
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\z/

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] },
            unique_translation: true
  validates :description_translations, translation: { allow_nil: true }
  validates :category_constant, uniqueness: true, presence: true

  ## ---------------------- Before action ------------------------ ##
  before_validation :uppercase_category_constant

  ## ---------------------- Scopes ------------------------ ##
  enum status: {
    unactive: 0,
    active: 1
  }

  scope :category_setting, -> { where.not(category_constant: %w[EMPLOYEEVOICE ASHANAK])  }

  ## ---------------------- Methods ----------------------- ##
  def uppercase_category_constant
    category_constant.upcase!
  end

  def logo_url
    URI.join(ActionController::Base.asset_host, logo.url).to_s
  end

  def category_counter
    category_model&.where('created_at >= ?', 15.hours.ago.utc)&.count || 0
  end

  def category_model
    case category_constant
    when 'EVENT' then Event
    when 'OFFER' then Offer
    when 'TRIPS' then Trip
    when 'NEWS'  then New
    when 'MEMOS' then Memo
    when 'JOBS'  then Assignment
    when 'MAGAZINE' then Magazin
    when 'CHALET' then Chalet
    when 'MBADRAT' then Mbadrat
    when 'INSTALLMENT' then Installment
    when 'EMPLOYEEVOICE' then EmployeeVoice
    end
  end
end
