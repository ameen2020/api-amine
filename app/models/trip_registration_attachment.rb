# == Schema Information
#
# Table name: trip_registration_attachments
#
#  id                      :integer          not null, primary key
#  trip_registration_id    :integer          not null
#  attachment_file_name    :string
#  attachment_content_type :string
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class TripRegistrationAttachment < ApplicationRecord

  ## -------------------- Requirements -------------------- ##
  include TripRegistrationAttachmentPresenter
  include AttachmentType

  ## ---------------------- Associations ----------------------- ##
  belongs_to :trip_registration

  ## -------------------- Validations --------------------- ##
  validates_attachment_presence :attachment
end
