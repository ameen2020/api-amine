# == Schema Information
#
# Table name: nationalities
#
#  id                :integer          not null, primary key
#  name_translations :jsonb            not null
#  status            :integer          default("active"), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Nationality < ApplicationRecord
  translates :name

  ## -------------------- Requirements -------------------- ##
  include NationalityPresenter
  include SearchableTranslation
  ## -------------------- Associations -------------------- ##
  has_many :installments, dependent: :delete_all

  ## -------------------- Validations --------------------- ##
  validates :name_translations, presence: true, translation: { required_keys: [:ar, :en] }, unique_translation: true

    ## ----------------------- Enums ------------------------ ##
  enum status: {
    inactive: 0,
    active:  1
  }

end
