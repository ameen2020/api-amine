# == Schema Information
#
# Table name: regions
#
#  id                 :integer          not null, primary key
#  title_translations :jsonb            not null
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  ashanak            :boolean          default(TRUE), not null
#

class Region < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  include SearchableTranslation
  include RegionPresenter
  translates :title

  ## ---------------------- Association -------------------- ##
  has_and_belongs_to_many :offers, dependent: :destroy
  has_and_belongs_to_many :events, dependent: :destroy
  has_and_belongs_to_many :chalets, dependent: :destroy
  has_many :work_locations
  has_many :ashanaks, dependent: :restrict_with_error

  ## -------------------- Validations --------------------- ##
  validates :title_translations, presence: true, translation: { required_keys: [:ar, :en] },
            unique_translation: true
end
