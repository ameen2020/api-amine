# == Schema Information
#
# Table name: offer_feedbacks
#
#  id          :integer          not null, primary key
#  employee_id :integer          not null
#  offer_id    :integer          not null
#  rating      :integer          not null
#  comment     :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer
#

class OfferFeedback < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include OfferFeedbackPresenter

  ## ---------------------- Associations ------------------ ##
  belongs_to :employee
  belongs_to :offer
  belongs_to :category, class_name: 'FeedbackCategory', optional: true

  ## --------------------- Validations --------------------- ##

  validates :employee_id, uniqueness: { scope: :offer_id, message: :already_gave_feedback }
  validates :category_id, presence: { message: :required_if_rating_low },   if: -> { rating < 3 }
  validates :category_id, absence:  { message: :required_only_rating_low }, if: -> { rating >= 3 }
  validates :comment,     presence: { message: :required_if_rating_low },   if: -> { rating < 3 }
  validates :rating, presence: true,  numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 5, only_integer: true }
  ## ---------------------- Call Backs ----------------------- ##

  after_create_commit :infrom_admins

  ## ------------------------ Methods ------------------------ ##

  def infrom_admins
    Rails.application.secrets.feedback_email.each do |email|
      OfferFeedbackMailer.infrom_admins(self.id, email).deliver_later
    end
  end
end
