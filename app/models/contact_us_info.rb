# == Schema Information
#
# Table name: contact_us_infos
#
#  id         :integer          not null, primary key
#  option     :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ContactUsInfo < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  include ContactUsInfoPresenter

  ## -------------------- Validations --------------------- ##
  validates :option, presence: true, uniqueness: true
  validates :value, presence: true
  ## --------------------- Callbacks ---------------------- ##
  before_save do
    option.downcase!
  end
end
