# frozen_string_literal: true
# == Schema Information
#
# Table name: offers
#
#  id                       :integer          not null, primary key
#  sub_category_id          :integer          not null
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  expire_on                :datetime         not null
#  discount                 :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  discount_type            :string
#  featured                 :boolean          default(FALSE), not null
#  email                    :string
#  phone_number             :string
#  views_counter            :integer          default(0), not null
#

class Offer < ApplicationRecord
  ## -------------------- Requirements -------------------- ##
  translates :title, :description
  include OfferPresenter
  include Imageable
  include IncreaseViews
  include SearchableTranslation
  include Notifiable
  # include MultiLocations
  include SendNotifiactionAdmin
  include CreatedBy

  attr_accessor :locations

  ## -------------------- Associations -------------------- ##

  has_many :notifications_histories, as: :notifiable, dependent: :destroy

  ## ---------------------- Associations ----------------------- ##

  belongs_to :sub_category

  has_many :feedbacks, class_name: 'OfferFeedback', dependent: :destroy
  has_many :attachments, class_name: 'OfferAttachment', dependent: :destroy
  has_many :offer_locations

  has_and_belongs_to_many :regions, dependent: :destroy
  accepts_nested_attributes_for :attachments
  accepts_nested_attributes_for :offer_locations, allow_destroy: true
  has_paper_trail

  ## -------------------- Validations --------------------- ##
  validates :email, email: true
  validates :title_translations, presence: true, translation: { required_keys: %i[ar en] }
  validates :description_translations, translation: { allow_nil: true }
  validates_datetime :expire_on, on_or_after: (-> { Date.today }), if: :expire_on_changed?

  ## ---------------------- Scopes ------------------------ ##

  scope :active_offers, -> { where('expire_on >= ?', Date.today) }

  ## ---------------------- Methods ----------------------- ##

  # before_validation :update_locations

  def average_rating
    ratings = feedbacks.pluck(:rating).compact
    return '0.0' if ratings.empty?
    format('%.1f', ratings.sum.to_f / ratings.size.to_f)
  end

  # Send Email
  def send_reminder_email(employee_id)
    NotificationOfferMailer.reminder(employee_id, id).deliver_later
  end

  # push reminder notification
  def push_reminder_notification(employee_id)
    %w[ar en].each do |locale|
      message = [I18n.t('notifications.offer.reminder', title: send("title_#{locale}"), date_time: expire_on.to_date, locale: locale.to_sym)]
      Resque.enqueue(Publisher, employee_id, message, 'OFFERS', id, nil, locale)
    end
  end
end
