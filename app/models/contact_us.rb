# frozen_string_literal: true
# == Schema Information
#
# Table name: contact_us
#
#  id            :integer          not null, primary key
#  employee_id   :integer          not null
#  phone_number  :string           not null
#  message       :text             not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  admin_id      :integer
#  reply_message :text
#  replied_at    :datetime
#  locale        :string           default("en"), not null
#

class ContactUs < ApplicationRecord
  ## -------------------- Requirements -------------------- ##

  include ContactUsPresenter
  include Localeable

  ## -------------------- Associations -------------------- ##

  belongs_to :employee
  belongs_to :admin, class_name: 'Employee', optional: true

  ## --------------------- Callbacks ---------------------- ##

  after_create_commit :send_email
  after_update_commit :send_reply_email
  before_validation do
    self.replied_at = Time.zone.now if replied? && replied_at.nil?
  end

  ## -------------------- Validations --------------------- ##

  validates :phone_number,  presence: true, length: { minimum: 7 }
  validates :message,       presence: true, length: { minimum: 10 }
  validates :admin,         presence: true, if: -> { reply_message.present? }
  validates :reply_message, presence: true, if: -> { admin_id.present? }

  ## ---------------------- Methods ----------------------- ##

  def send_email
    ContactUsMailer.contact(id).deliver_later
  end

  def send_reply_email
    ContactUsMailer.reply(id).deliver_later if replied?
  end

  def status
    replied? ? :replied : :pending
  end

  def replied?
    reply_message.present? && admin_id.present?
  end

  ## --------------------- Exporters ---------------------- ##

  def self.exportable_headers
    [
      'ID',
      'Employee ID',
      'MOH ID',
      'Full Name',
      'Phone Number',
      'Email',
      'Message',
      'Contact Number',
      'Submitted At',
      'Status',
      'Reply Message',
      'Replied By',
      'Replied At'
    ]
  end

  def exportable_data
    [
      id,
      employee.id,
      employee.employee_id,
      employee.full_name,
      employee.phone_number,
      employee.email,
      message,
      phone_number,
      created_at.in_time_zone('Riyadh').strftime('%F %r'),
      status.to_s.titleize,
      reply_message,
      admin&.full_name,
      replied_at&.in_time_zone('Riyadh')&.strftime('%F %r')
    ]
  end
end
