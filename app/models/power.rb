# frozen_string_literal: true

class Power
  include Consul::Power

  def initialize(user, access = 'app')
    @current_user = user
    @access       = access
  end

  # check if the current users is an admin
  def super_admin?
    return true if @current_user&.super_admin? && @access == 'admin'
    false
  end

  def admin?
    return true if @current_user&.admin? && @access == 'admin'
    false
  end

  def admins?
    super_admin? || admin?
  end

  def sub_admin?
    return true if @current_user&.sub_admin? && @access == 'admin'
    false
  end


  # Generate powers for all tables and by default prevent them all from access
  ActiveRecord::Base.connection.tables.map(&:to_sym) - %i[schema_migrations ar_internal_metadata].each do |model|
    power model do
      false
    end
  end

  # allow evryone to user sessions api
  power :sessions do
    true
  end

  ##------------------------------ ProfilesController -----------------------------------##

  power :dashboard_counters do
    true if admins? || sub_admin?
  end

  power :dashboard_statistics do
    true if admins?
  end

  ##------------------------------ ProfilesController -----------------------------------##

  power :profiles do
    return @current_user if @current_user.active?
    false
  end

  power :profile_events do
    return @current_user.events if @current_user.active?
    false
  end

  power :profile_trips do
    return @current_user.trips.where(trip_registrations: {super_admin_approve: 1}) if @current_user.active?
    false
  end

  power :profile_chalets do
    return @current_user.chalets.where(chalet_registrations: {super_admin_approve: 1}) if @current_user.active?
    false
  end

  ##------------------------------ EmployeesController -----------------------------------##

  power :employee_index do
    return Employee if admins?
    Employee.active.employee
  end

  power :employee_show do
    return Employee if admins?
    Employee.active.employee
  end

  power :creatable_employee do
    # return true if admins?
    false
  end

  power :updatable_employee do
    return Employee if admins?
    false
  end

  power :destroyable_employee do
    return Employee if admins?
    Employee.none
  end

  power :exportable_employees do
    return Employee if admins?
    false
  end

  ##------------------------------ CategoriesController -----------------------------------##

  power :category_index do
    return Category if admins?
    Category.active
  end

  power :category_show do
    return Category if admins?
    Category.active
  end

  power :creatable_category do
    return true if admins?
    false
  end

  power :updatable_category do
    return Category if admins?
    Category.none
  end

  power :destroyable_category do
    return Category if admins?
    Category.none
  end

  ##------------------------------ eventsController -----------------------------------##

  power :event_index do
    Event.includes(:pictures, :cover_picture).select((Event.column_names - ['locations']).map(&:to_sym))
  end

  power :event_show do
    Event
  end

  power :creatable_event do
    return true if admins? || sub_admin?
    false
  end

  power :updatable_event do
    return Event if admins? || sub_admin?
    Event.none
  end

  power :destroyable_event do
    return Event if admins? || sub_admin?
    Event.none
  end

  power :events_calendar do
    Event
  end

  power :export_event_registerations do
    return Event if admins? || sub_admin?
    false
  end

##------------------------------ offersController -----------------------------------##

  power :offer_index do
    offers = Offer.includes(:pictures, :cover_picture, :offer_locations)
    return offers if admins? || sub_admin?
    offers.active_offers
  end

  power :offer_show,
        :generate_pdf do
    return Offer if admins? || sub_admin?
    Offer
  end

  power :creatable_offer do
    return true if admins? || sub_admin?
    false
  end

  power :updatable_offer do
    return Offer if admins? || sub_admin?
    Offer.none
  end

  power :destroyable_offer do
    return Offer if admins? || sub_admin?
    Offer.none
  end

##------------------------------ SubCategoryController -----------------------------------##


  power :sub_category_index do
    return SubCategory if admins?
    SubCategory
  end

  power :sub_category_show do
    return SubCategory if admins?
    SubCategory
  end

  power :creatable_sub_category do
    return true if admins?
    false
  end

  power :updatable_sub_category do
    return SubCategory if admins?
    SubCategory.none
  end

  power :destroyable_sub_category do
    return SubCategory if admins?
    SubCategory.none
  end

  ##------------------------------ EventRegistrationController -----------------------------------##

  power :event_registration_index do |event_id|
    return EventRegistration.includes(:employee).where(event_id: event_id) if admins? || sub_admin?
    false
  end

  power :creatable_event_registration do |event_id|
    return false if admins? || sub_admin?
    true
  end

  power :destroyable_event_registration do |event_id|
    EventRegistration.where(event_id: event_id)
  end

  power :send_emailable_event_registration do |event_id|
    EventRegistration.where(event_id: event_id) if admins? || sub_admin?
  end

  ##------------------------------ OfferFeedbacksController -----------------------------------##
  power :offer_feedbacks_index do |offer_id|
    Offer.find(offer_id).feedbacks.includes(:employee)
  end

  power :offer_feedbacks_show do |offer_id|
    Offer.find(offer_id).feedbacks
  end

  power :creatable_offer_feedback do |offer_id|
    return true
  end

  ##------------------------------ NewController -----------------------------------##

  power :new_index do
    return New.includes(:pictures, :cover_picture) if admins? || sub_admin?
    New.includes(:pictures, :cover_picture).active_news
  end

  power :new_show do
    New
  end
  power :creatable_new do
    return true if admins? || sub_admin?
    false
  end

  power :updatable_new do
    return New if admins? || sub_admin?
    New.none
  end

  power :destroyable_new do
    return New if admins? || sub_admin?
    New.none
  end

##------------------------------ MemoController -----------------------------------##

  power :memo_index do
    Memo.includes(:pictures, :cover_picture)
  end

  power :memo_show do
    Memo
  end

  power :creatable_memo do
    return true if admins? || sub_admin?
    false
  end

  power :updatable_memo do
    return Memo if admins? || sub_admin?
    Memo.none
  end

  power :destroyable_memo do
    return Memo if admins? || sub_admin?
    Memo.none
  end

  ##------------------------------ MagazinesController -----------------------------------##
  power :magazine_index do
    Magazin.includes(:pictures, :cover_picture)
  end

  power :magazine_show do
    Magazin
  end

  power :creatable_magazine do
    return true if admins? || sub_admin?
    false
  end

  power :updatable_magazine do
    return Magazin if admins? || sub_admin?
    Magazin.none
  end

  power :destroyable_magazine do
    return Magazin if admins? || sub_admin?
    Magazin.none
  end

  ##------------------------------ OfferFeedbacksController -----------------------------------##

  power :offer_feedbacks_index do |offer_id|
    Offer.find(offer_id).feedbacks.includes(:employee)
  end

  power :offer_feedbacks_show do |offer_id|
    Offer.find(offer_id).feedbacks
  end

  power :creatable_offer_feedback do |offer_id|
    return true
  end

  ##------------------------------ OfferAttachmentsController -----------------------------------##

  power :offer_attachments_index do |offer_id|
    return unless admins? || sub_admin?
    Offer.find(offer_id).attachments
  end

  power :creatable_offer_attachment do |offer_id|
    return true if admins? || sub_admin?
    false
  end

  power :destroyable_offer_attachment do |offer_id|
    return unless admins? || sub_admin?
    Offer.find(offer_id).attachments
  end

##------------------------------ RegionController -----------------------------------##

  power :region_index do
    Region
  end

  power :region_show do
    return Region if admins?
    Region.none

  end

  power :creatable_region do
    return true if admins?
    false
  end


  power :updatable_region do
    return Region if admins?
    Region.none
  end

  power :destroyable_region do
    return Region if admins?
    Region.none
  end
##------------------------------ TrtipController -----------------------------------##

  power :trip_index do
    return Trip.includes(:pictures, :cover_picture).with_pendings(@current_user.role) if admins? || sub_admin?
    Trip.includes(:pictures, :cover_picture)
  end

  power :trip_show do
    return Trip.with_pendings(@current_user.role) if admins? || sub_admin?
    Trip
  end

  power :creatable_trip do
    return true if admins? || sub_admin?
    false
  end

  power :updatable_trip do
    return Trip if admins? || sub_admin?
    Trip.none
  end

  power :destroyable_trip do
    return Trip if admins? || sub_admin?
    Trip.none
  end

  power :trips_calendar do
    Trip
  end

  power :export_trip_registerations do
    return Trip if admins? || sub_admin?
    false
  end

##------------------------------ AssignmentController -----------------------------------##
  power :assignment_index do
    Assignment.includes(:pictures, :cover_picture)
  end

  power :assignment_show do
    Assignment
  end

  power :creatable_assignment do
    return true if admins? || sub_admin?
    false
  end


  power :updatable_assignment do
    return Assignment if admins? || sub_admin?
    Assignment.none
  end

  power :destroyable_assignments do
    return Assignment if admins? || sub_admin?
    Assignment.none
  end

##------------------------------ RelationshipController -----------------------------------##

  power :relationship_index do
    Relationship
  end

  power :relationship_show do
    return Relationship if admins?
    Relationship.none
  end

  power :creatable_relationship do
    return true if admins?
    false
  end

  power :updatable_relationship do
    return Relationship if admins?
    Relationship.none
  end

  power :destroyable_relationship do
    return Relationship if admins?
    Relationship.none
  end

  ##------------------------------ TripRegistrationController -----------------------------------##

  power :trip_registration_index do |trip_id|
    return TripRegistration.includes(:employee, :relationship, :attachments).where(trip_id: trip_id) if admins? || sub_admin?
    false
  end

  power :creatable_trip_registration do |trip_id|
    return false if admins? || sub_admin?
    true
  end

  power :destroyable_trip_registration do |trip_id|
    TripRegistration.where(trip_id: trip_id)
  end

  power :updataable_trip_registration do |trip_id|
    return TripRegistration.where(trip_id: trip_id) if admins? || sub_admin?
    false
  end

  power :send_emailable_trip_registration do |trip_id|
    TripRegistration.where(trip_id: trip_id) if admins? || sub_admin?
  end


  ##------------------------------ OccasionController -----------------------------------##

  power :occasion_index do
    Occasion
  end

  power :occasion_show do
    return Occasion if admins?
    Occasion.none
  end

  power :creatable_occasion do
    return true if admins?
    false
  end

  power :updatable_occasion do
    return Occasion if admins?
    Occasion.none
  end

  power :destroyable_occasion do
    return Occasion if admins?
    Occasion.none
  end

  ##------------------------------ WorkLocationsController -----------------------------------##
  power :work_locations_index do
    return WorkLocation.includes(:region) if admins? || sub_admin?
    WorkLocation
  end

  power :work_location_show do
    WorkLocation
  end

  power :creatable_work_location do
    return true if admins?
    false
  end

  power :updatable_work_location do
    return WorkLocation if admins?
    WorkLocation.none
  end

  power :destroyable_work_location  do
    return WorkLocation if admins?
    WorkLocation.none
  end

  ##------------------------------ AshanaksController -----------------------------------##

  power :ashanaks_index do
    return Ashanak.includes(:employee, :attachments, :occasion, :region, :work_location) if admins? || sub_admin?
    Ashanak.none
  end

  power :ashanak_show do
    return Ashanak if admins? || sub_admin?
    Ashanak.none
  end

  power :creatable_ashanak do
    return false if admins? || sub_admin?
    true
  end

  power :updatable_ashanak do
    return Ashanak if admins? || sub_admin?
    Ashanak.none
  end

  power :destroyable_ashanak  do
    return Ashanak if admins? || sub_admin?
    Ashanak.none
  end

  power :updataable_approval do
    return Ashanak if admins? || sub_admin?
    Ashanak.none
  end

  power :requestable_ashanaks do
    return Ashanak if admins? || sub_admin?
    Ashanak.none
  end

  power :exportable_ashanaks do
    return Ashanak.includes(:employee, :occasion, :region, :work_location) if admins? || sub_admin?
    false
  end

  power :buildable_ashanak do
    return Ashanak if admins? || sub_admin?
    Ashanak.none
  end

  ##------------------------------ Notification Setting -----------------------------------##

  # Allow active user to notifications setting
  power :notifications_settings do
    true
  end
  ##------------------------------ Notification History -----------------------------------##

  power :notifications_index do
    @current_user.notifications_histories
  end

  power :notifications_show do
    @current_user.notifications_histories
  end

  power :unread_notiifications do
    @current_user.notifications_histories
  end

  ## -------------------------------------------------- ##
  ## Passwords Controller

  power :updatable_password do
    return true if admins? || sub_admin?
  end

  power :forgotable_password do
    true unless @current_user
  end

  power :recoverable_password do
    true unless @current_user
  end

  ## -------------------------------------------------- ##
  ## FeedbackCateogryController

  power :feedback_category_index do
    FeedbackCategory
  end

  power :feedback_category_show do
    return FeedbackCategory if admins?
    FeedbackCategory.none
  end

  power :creatable_feedback_category do
    true if admins?
  end

  power :updatable_feedback_category do
    return FeedbackCategory if admins?
    FeedbackCategory.none
  end

  power :destroyable_feedback_category do
    return FeedbackCategory if admins?
    FeedbackCategory.none
  end

  ## -----------------------ContactUsInfoController--------------------------- ##

  power :contact_us_infos_index do
    ContactUsInfo
  end

  power :contact_us_infos_show do
    ContactUsInfo
  end

  power :creatable_contact_us_infos do
    true if admins?
  end

  power :updatable_contact_us_infos do
    return ContactUsInfo if admins?
    ContactUsInfo.none
  end

  power :destroyable_contact_us_infos do
    return ContactUsInfo if admins?
    ContactUsInfo.none
  end

  ## -----------------------ContactUsController--------------------------- ##

  power :contact_us_index do
    ContactUs.includes(:employee) if admins? || sub_admin?
  end

  power :contact_us_show do
    ContactUs if admins? || sub_admin?
  end

  power :creatable_contact_us do
    true
  end

  power :updatable_contact_us do
    return ContactUs if admins? || sub_admin?
    ContactUs.none
  end

  power :destroyable_contact_us do
    ContactUs.none
  end

  power :exportable_contact_us do
    ContactUs.includes(:employee) if admins? || sub_admin?
  end

  ## -----------------------TripBlackListController--------------------------- ##

  power :trip_black_lists_index do
    TripBlackList.includes(:employee, :blocker) if admins? || sub_admin?
  end

  power :creatable_trip_black_list do
    admins? || sub_admin?
  end

  power :destroyable_trip_black_list do
    TripBlackList if admins? || sub_admin?
  end

  ## ------------------- AdminNotificationsController ------------------- ##

  power :admin_notifications_index, :admin_notifications_show do
    return AdminNotification.includes(:noticeable, :employee) if super_admin?
    false
  end


  ##------------------------------ chaletsController -----------------------------------##

  power :chalet_index do
    Chalet.includes(:pictures, :cover_picture).select((Chalet.column_names - ['locations']).map(&:to_sym))
  end

  power :chalet_show do
    Chalet
  end

  power :creatable_chalet do
    return true if admins? || sub_admin?
    false
  end

  power :updatable_chalet do
    return Chalet if admins? || sub_admin?
    Chalet.none
  end

  power :destroyable_chalet do
    return Chalet if admins? || sub_admin?
    Chalet.none
  end

  power :chalets_calendar do
    Chalet
  end

  power :export_chalet_registerations do
    return Chalet if admins? || sub_admin?
    false
  end
  ##------------------------------ ChaletRegistrationController -----------------------------------##

  power :chalet_registration_index do |chalet_id|
    return ChaletRegistration.includes(:employee).where(chalet_id: chalet_id) if admins? || sub_admin?
    false
  end

  power :creatable_chalet_registration do |chalet_id|
    return false if admins? || sub_admin?
    true
  end

  power :destroyable_chalet_registration do |chalet_id|
    ChaletRegistration.where(chalet_id: chalet_id)
  end

  power :updataable_chalet_registration do |chalet_id|
    return ChaletRegistration.where(chalet_id: chalet_id) if admins? || sub_admin?
    false
  end

  power :send_emailable_chalet_registration do |chalet_id|
    ChaletRegistration.where(chalet_id: chalet_id) if admins? || sub_admin?
  end

  ##------------------------------ MbadratController -----------------------------------##
  power :mbadrat_index do
    Mbadrat.includes(:pictures, :cover_picture)
  end

  power :mbadrat_show do
    Mbadrat
  end

  power :creatable_mbadrat do
    return true if admins? || sub_admin?
    false
  end

  power :updatable_mbadrat do
    return Mbadrat if admins? || sub_admin?
    Mbadrat.none
  end

  power :destroyable_mbadrat do
    return Mbadrat if admins? || sub_admin?
    Mbadrat.none
  end

  ##------------------------------ EmployeeEmployeeController -----------------------------------##
  power :employee_voice_index do
    return EmployeeVoice if admins?
    EmployeeVoice.none
  end

  power :employee_voice_show do
    return EmployeeVoice if admins?
    EmployeeVoice.none
  end

  power :creatable_employee_voice do
    return false if admins? || sub_admin?
    true
  end

  power :updatable_employee_voice do
    return EmployeeVoice if admins? || sub_admin?
    EmployeeVoice.none
  end

  power :destroyable_employee_voice do
    return EmployeeVoice if admins? || sub_admin?
    EmployeeVoice.none
  end
  ##------------------------------ NationalityController -----------------------------------##
  power :nationality_index do
    return Nationality if admins? || sub_admin?
    Nationality.active
  end

  power :nationality_show do
    Nationality
  end
  power :creatable_nationality do
    return true if admins? || sub_admin?
    false
  end

  power :updatable_nationality do
    return Nationality if admins? || sub_admin?
    Nationality.none
  end

  power :destroyable_nationality do
    return Nationality if admins? || sub_admin?
    Nationality.none
  end
  ##------------------------------ InstallmentsController -----------------------------------##

  power :installment_index do
    return Installment.includes( :attachments) if admins? || sub_admin?
    Installment.none
  end

  power :installment_show do
    return Installment if admins? || sub_admin?
    Installment.none
  end

  power :creatable_installment do
    return false if admins? || sub_admin?
    true
  end

  power :updatable_installment do
    return Installment if admins? || sub_admin?
    Installment.none
  end

  power :destroyable_installment  do
    return Installment if admins? || sub_admin?
    Installment.none
  end

  power :updatable_installment_approval do
    return Installment if admins? || sub_admin?
    Installment.none
  end

  power :exportable_installments do
     return Installment.includes(:employee, :occasion, :region, :work_location) if admins? || sub_admin?
     false
  end
  ##------------------------------ MessagesController -----------------------------------##

  power :messages_index do
    return Message.all if admins? || sub_admin?
    Message.none
  end

  power :message_show do
    return Message if admins? || sub_admin?
    Message.none
  end

  power :creatable_message do
    admins? || sub_admin?
  end

  power :updatable_message do
    return Message if admins? || sub_admin?
    Message.none
  end

  power :destroyable_message  do
    return Message if admins? || sub_admin?
    Message.none
  end

  ##------------------------------ VersionsController -----------------------------------##

  power :versions_index do
    return Version if admins?
    Version.none
  end

  power :version_show,
        :version_options do
    return Version if admins?
    Version.none
  end

  ##------------------------------ PageController -----------------------------------##

  power :pages_index,
        :updatable_page do
    return Page if admins? || sub_admin?
    Page.none
  end

  power :page_show do
    Page
  end

end
