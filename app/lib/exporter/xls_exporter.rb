# frozen_string_literal: true

require 'spreadsheet'

module Exporter
  class XlsExporter
    attr_reader :options, :headers, :data

    def initialize(headers:, data:, **options)
      @headers = headers
      @data    = data
      @options = options

      Spreadsheet.client_encoding = 'UTF-8'
      @book = Spreadsheet::Workbook.new
      @sheet1 = @book.create_worksheet name: 'Sheet 1'
    end

    def export
      # Add Headers
      @sheet1.row(0).concat(@headers)

      # Add Rows
      @data.each_with_index do |row, index|
        @sheet1.row(index + 1).concat(row)
      end

      set_style
      set_width

      file_contents = StringIO.new
      @book.write file_contents

      file_contents.string.force_encoding('binary')
    end

    def set_style
      # Formats
      format = Spreadsheet::Format.new(size: 12)
      bold   = Spreadsheet::Format.new(size: 12, weight: :bold)

      # Set Default format
      @data.size.times do |x|
        @sheet1.row(x).size.times do |y|
          @sheet1.row(x + 1).set_format(y, format)
        end
      end

      # Set Header format
      @sheet1.row(0).size.times do |y|
        @sheet1.row(0).set_format(y, bold)
      end
    end

    def set_width
      lengths = []
      max_width = 50

      @sheet1.rows.each do |row|
        lengths << row.map { |x| x.to_s.length }
      end

      @headers.size.times do |col|
        w = lengths.map { |i| i[col] }.max
        @sheet1.column(col).width = (w > max_width ? max_width : w) + 3
      end
    end
  end
end
