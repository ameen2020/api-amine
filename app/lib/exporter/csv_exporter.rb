# frozen_string_literal: true

module Exporter
  class CsvExporter
    attr_reader :options, :headers, :data

    def initialize(headers:, data:, **options)
      @headers = headers
      @data    = data
      @options = options
    end

    def export
      CSV.generate(@options) do |csv|
        csv << @headers
        @data.map { |row| csv << row }
        csv << @footers unless @footers.empty?
      end
    end
  end
end
