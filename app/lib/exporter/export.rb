# frozen_string_literal: true

module Exporter
  class Export
    attr_reader :headers, :data, :footers

    def initialize(collection:, type: :xls, **options)
      @headers    = collection.exportable_headers
      @data       = collection.map { |row| prosess_row(row) }
      @type       = type
      @options    = options
    end

    def prosess_row(row)
      row.exportable_data.map { |r| r.is_a?(String) ? r&.gsub("\n", ', ')&.gsub("\t", ', ') : r }
    end

    def export
      exporter_class.new(headers: @headers, data: @data, options: @options).export
    end

    def exporter_class
      case @type
      when :csv then Exporter::CsvExporter
      when :xls then Exporter::XlsExporter
      end
    end
  end
end
