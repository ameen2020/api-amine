require 'net/http'
class UserRegistration

  ENDPOINT         = Rails.application.secrets.moh_login[:ashanak_login_api]
  SERVICE_USERNAME = Rails.application.secrets.moh_login[:servie_username]
  SERVICE_PASSWORD = Rails.application.secrets.moh_login[:servie_password]
  GRANT_TYPE       = Rails.application.secrets.moh_login[:grant_type]
  APP_ID           = Rails.application.secrets.moh_login[:app_id]
  SUPER_ADMIN      = Rails.application.secrets.super_admin
  USER_CLASS       = Employee

  # this method will be used in the controller
  def self.authenticate(email_or_username, password)
    new(email_or_username, password).perform
  end

  # initialize the object
  def initialize(email_or_username, password)
    email_or_username.include?('@') ? @email = check_domain_eamil(email_or_username) : @username = email_or_username
    @password = password
  end
  # Check end domain email if end MOH or Lean
  # and return string
  def check_domain_eamil(email)
    return email if email.include?('moh.gov.sa') && get_username(email)
    email if email.include?('lean.sa')
  end
  # Get user name 
  def get_username(email)
    @username = email.split('@')[0] if email.include?('moh.gov.sa')
  end
  # the process happen here
  # @return [Object]  if user is valid and insenter in our system, or have validation error in our system
  # @return [Boolean] return false is user and pass in not valid in 3rd party servcie
  def perform
    return perform_super_admin  if is_super_admin?
    return check_db_user?       if check_db_user? && @email.present?
    return create_user          if check_user?    && @username.present?
    false
  end

  def is_super_admin?
    SUPER_ADMIN.present? && @username == SUPER_ADMIN[:username] && @password == SUPER_ADMIN[:password]
  end

  def perform_super_admin
    @response = {
      'username'   => @username,
      "employeeId" => "mohxxxxx",
      "fullName"   => SUPER_ADMIN[:username],
      "mobileNo"   => SUPER_ADMIN[:mobile],
      "email"      => 'admin@clickapps.co'
    }
    return create_user
  end

  # Hit 3rd party API and get user details
  # and validate if the request is valid or not
  # @return [Boolean]
  def check_user?
    uri = URI.parse(ENDPOINT)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(uri.path)
    request.body = "grant_type=password&username=#{@username}&password=#{@password}&appId=#{APP_ID}&serviceUsername=#{SERVICE_USERNAME}&servicePassword=#{SERVICE_PASSWORD}"

    Rails.logger.info("\e[96;1mSend Auth Request to : #{ENDPOINT}, username: #{@username}, password: [FILTERED]\e[0m")

    response = http.request(request)

    if response.is_a?(Net::HTTPSuccess)
      @response = JSON.parse(response.body)
      Rails.logger.info("\e[96;1m Auth Request Succeeded\e[0m : #{response.body}")
      return true
    elsif response.is_a?(Net::HTTPBadRequest)
      Rails.logger.info("\e[91;1m Auth Request Failed\e[0m : #{response.body}")
      return false
    else
      raise(CustomException::AuthServiceError)
      return false
    end
  end

  # Parse and map the user object
  # @return [Hash]
  def user_details
    {
      username:     @username,
      employee_id:  process_data(@response["employeeId"]),
      first_name:   process_data(@response["fullName"]),
      email:        process_data(@response["email"]),
      phone_number: process_data(@response["mobileNo"]),
      status:       'active',
    }
  end

  # Nullfy attribute
  def process_data(value)
    value == 'N/A' ? nil : value&.downcase
  end

  # Create user in our db
  # @return [Object]
  def create_user
    # CASE: if the user is from clickapps, then assign an email to the data , since
    # =>    email is required and , and our account dont have email in the Active Directory
    # =>    so the account have custom profile, and force it to be super admin
    user_data = is_super_admin? ? ashanak_vpn_account : user_details

    user = USER_CLASS.find_by('lower(email) = ?', user_data[:email])

    return user if user.present?

    USER_CLASS.create(user_data)
  end

  # our account dont have an email ,
  def ashanak_vpn_account
    {
      username:     @username,
      employee_id:  process_data(@response["employeeId"]),
      first_name:   process_data(@response["fullName"]),
      email:        process_data(@response["email"]),
      phone_number: process_data(@response["mobileNo"]),
      status:       'active',
      role:         0
    }
  end
  # Authinticate if user already exist in db with password (used for test users)
  def check_db_user?
    keyword = @email.present? ? @email.downcase : @username.downcase
    USER_CLASS.where('password_digest is not null')
              .find_by("lower(username) = :keyword OR email = :keyword", keyword: keyword)
              &.authenticate(@password) || false
  end
end