
module Pdf

  class Build
    attr_reader :data, :type

    def initialize(data:, type:)
      @data   = data
      @type   = type
    end

    def build
      builder_class.build(path_template ,assigns)
    end

    def builder_class
      case @type
      when :e_card      then Pdf::ECardBuilder
      when :sticker     then Pdf::StickerBuilder
      when :invitation  then Pdf::InvitationToOffer
      end
    end

    def template_exist?
      File.exist?(Rails.root.join('app', "views/pdf/#{@type}/#{type_occasions}.pdf.erb"))
    end

    def path_template
      return "pdf/offer.pdf.erb" if @type == :invitation
      template_exist? ? "pdf/#{@type}/#{type_occasions}.pdf.erb" : "pdf/defualt.pdf.erb"
    end

    def assigns
      if @type == :invitation
        {
          relative_name:  @data[:relative_name],
          employee:       @data[:employee],
          offer:          @data[:offer],
          path:           "#{Rails.root}/public/invitation.png"
        }
      else
        {
          ashanak:    @data,
          occasions:  @data.occasion,
          employee:   @data.employee,
          path:       "#{Rails.root}/public/#{type_occasions}.png",
          style:      @data.occasion.title_en.snakecase
        }
      end
    end

    def type_occasions
      @data.occasion.title_en.snakecase
    end
  end

end
