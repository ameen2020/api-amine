module Pdf
  class StickerBuilder

    # build Pdf string according to passed parameters
    def self.build(template, assigns={}, pdf_options=pdf_default_options)
      WickedPdf.new.pdf_from_string(
        ActionController::Base.new().render_to_string(
          :template => template,
          :assigns  => assigns,
        ),
        pdf_options
      )
    end

    # Set PDF default options
    def self.pdf_default_options
      {
        # available orientation: Landscape and portrait
        orientation: :portrait,
        page_size:   'A4',
        background: true
      }
    end
  end
end
