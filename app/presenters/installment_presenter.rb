# frozen_string_literal: true

module InstallmentPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :nationality, template: :base
      t.add :company_name
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add :attachments, template: :v1_admin_index
    end

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :super_admin_approve_before_type_cast, as: :super_admin_approve
      t.add :sub_admin_approve_before_type_cast, as: :sub_admin_approve
      t.add ->(r) { r.created_at.strftime('%F %r') }, as: :created_at
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.add :super_admin_comment
      t.add :sub_admin_comment
      t.add :attachments, template: :v1_admin_index
    end
  end
end
