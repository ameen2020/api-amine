module RegionPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :title
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :title_translations
      t.add :position
      t.add :ashanak
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index

    api_accessible :notification_settings, extend: :base do |t|
      t.add lambda{|e, o| o[:region_ids].include?(e.id)}, as: :value
    end
  end
end
