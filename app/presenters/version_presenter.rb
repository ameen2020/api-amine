module VersionPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :item_type
      t.add :item_id
      t.add :event
      t.add :employee, template: :base
      t.add ->(e) { e.created_at.strftime('%Y-%m-%d') }, as: :created_at

    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.add :changeset
    end

  end
end
