module EventRegistrationPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add 'event.remaining', as: :remaining
    end

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :id
      t.add ->(r) { r.created_at.strftime('%F %r') }, as: :created_at
      t.add 'employee.id', as: :employee_id
      t.add 'employee.full_name', as: :employee_name
      t.add :register_code
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index

  end
end
