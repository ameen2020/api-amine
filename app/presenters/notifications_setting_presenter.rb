module NotificationsSettingPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      # t.add :id
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add :categories, template: :notification_settings
      t.add lambda{|e, o| e.regions(o[:current_user])}, as: :regions, template: :notification_settings
      t.add lambda{|e, o| e.sub_categories(o[:current_user])}, as: :sub_categories, template: :notification_settings
    end

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base

    api_accessible :v1_admin_show, extend: :v1_admin_index
  end
end
