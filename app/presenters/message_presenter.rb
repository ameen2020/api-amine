module MessagePresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :title
      t.add :message
      t.add :created_at
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :base

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :sender,  template: :base
    end

    api_accessible :v1_admin_show, extend: :base 

    ## ----------------------- Others ----------------------- ##

    api_accessible :options, extend: :base

  end
end
