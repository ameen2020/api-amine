# frozen_string_literal: true

module TripPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :title
      t.add :description
      t.add ->(e) { e.start_date.strftime('%Y-%m-%d') }, as: :start_date
      t.add ->(e) { e.end_date&.strftime('%Y-%m-%d') }, as: :end_date
      t.add ->(e) { e.cover(:thumb) }, as: :logo
      t.add :location
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add :remaining
      t.add :images
      t.add ->(e, o) { e.registration_tag(o[:current_user]) }, as: :registration_tag
    end

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add :remaining
      t.add :images
    end

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :views_counter
      t.add :pending_registrations_count, as: :pending_registrations
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.remove :title
      t.remove :description
      t.remove :location
      t.add :title_translations
      t.add :description_translations
      t.add :location_translations
      t.add :registered_count
      t.add :capacity
      t.add :pictures, as: :images, template: :v1_admin_show
      t.add ->(e) { e.created_at.strftime('%F %r') }, as: :created_at
      t.add ->(e) { e.last_register_date.strftime('%Y-%m-%d') }, as: :last_register_date
      t.add ->(e) { e.last_cancel_date&.strftime('%Y-%m-%d') }, as: :last_cancel_date
      t.add :created_by, template: :base
      t.add :created_at
    end
  end

  def registration_tag(current_user)
    registered_employees = employees.pluck(:id)
    registered           = registered_employees.include?(current_user.id)
    employee_blacked     = TripBlackList.exists?(employee_id: current_user.id)
    {
      registered: registered,
      can_register: can_register? && !registered && !employee_blacked,
      can_unregister: can_cancel? && registered,
      is_rejected: registered && rejected(current_user),
      is_blocked: employee_blacked
    }
  end

  def registered_count
    trip_registrations.count || 0
  end

  def rejected(current_user)
    registr = current_user.trip_registrations.find_by(trip_id: id)
    (registr.super_admin_rejected? && registr.sub_admin_rejected?) || registr.super_admin_rejected? ||
      (registr.sub_admin_rejected? && !registr.super_admin_accepted?)
  end
end
