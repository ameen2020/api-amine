# frozen_string_literal: true

module EmployeePresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :first_name
      t.add :last_name
      t.add :full_name
    end

    ## ----------------------- Employees ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add 'status.capitalize',      as: :status_label
      t.add :status_before_type_cast, as: :status
      t.add 'role.titlecase',         as: :role_label
      t.add :role_before_type_cast,   as: :role
      t.add :avatar
      t.add ->(r) { r.created_at.strftime('%F %r') }, as: :created_at
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.add :email
      t.add :phone_number
      t.add :employee_id
    end

    ## ---------------------- login ------------------------##

    api_accessible :v1_profile do |t|
      t.add :id
      t.add :full_name
      t.add :employee_id
      t.add :phone_number
      t.add :email
      t.add :avatar
      t.add 'events.count', as: :events_count
      t.add :trips_count
      t.add :chalets_count
    end

    api_accessible :statistics do |t|
      t.add :id
      t.add :key
      t.add :value
      t.add :percentage
    end

    def trips_count
      trips.where(trip_registrations: { super_admin_approve: 1 })&.count
    end

    def chalets_count
      chalets.where(chalet_registrations: { super_admin_approve: 1 })&.count
    end

    def full_name
      [first_name, last_name].compact.join(' ').remove(' -')&.titlecase
    end
  end
end
