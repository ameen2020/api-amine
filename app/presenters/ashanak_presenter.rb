# frozen_string_literal: true

module AshanakPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :id
      t.add :region, template: :base
      t.add :occasion, template: :base
      t.add :work_location, template: :base
      t.add :employee, template: :base
      t.add :date
      t.add :address
      t.add :phone_number
      t.add :super_admin_approve_before_type_cast, as: :super_admin_approve
      t.add :sub_admin_approve_before_type_cast, as: :sub_admin_approve
      t.add ->(r) { r.created_at.strftime('%F %r') }, as: :created_at
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.add :attachments, template: :v1_admin_index
      t.add :super_admin_comment
      t.add :sub_admin_comment
    end
  end
end
