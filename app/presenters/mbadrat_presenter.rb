module MbadratPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :title
      t.add :description
      t.add lambda{ |e| e.cover(:thumb) }, as: :logo
      t.add lambda{ |e| e&.created_at&.strftime('%Y-%m-%d')}, as: :date_of_adding

    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add :images
      t.add :mbadrat_faqs, template: :base
      t.add :video
      t.add :video_thumbnail
      t.add :attachment_url, as: :attachment
    end


    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :views_counter
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.remove :title
      t.add :title_translations
      t.add :description_translations
      t.add :mbadrat_faqs, template: :v1_admin_show
      t.add :video
      t.add :video_thumbnail
      t.add :pictures, as: :images, template: :v1_admin_show
      t.add :attachment_url, as: :attachment
      t.add :created_by, template: :base
      t.add :created_at
    end
  end
end
