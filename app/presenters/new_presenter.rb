module NewPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :title
      t.add :description
      t.add lambda{|e| e.cover(:thumb) }, as: :logo
      t.add :important
      t.add lambda{|e| e.date&.strftime('%Y-%m-%d') }, as: :date
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add :images
    end

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :views_counter
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.remove :title
      t.remove :description
      t.add :title_translations
      t.add :description_translations
      t.add :created_by, template: :base
      t.add :created_at
      t.add :pictures, as: :images, template: :v1_admin_show
      
    end
  end
end
