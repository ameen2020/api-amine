module AssignmentPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :title
      t.add :description
      t.add lambda{|e| e.cover(:thumb) }, as: :logo
      t.add lambda{|e| e.date&.strftime('%Y-%m-%d') }, as: :date
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add :default_email, as: :contact_email
      t.add :images
    end

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.remove :titile
      t.remove :description
      t.add :description_translations
      t.add :title_translations
      t.add :views_counter
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.add :email
      t.add :created_by, template: :base
      t.add :created_at
      t.add :pictures, as: :images, template: :v1_admin_show
    end

    def default_email
      email? ? email : Rails.application.secrets.default_email
    end
  end
end
