module OfferPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :title
      t.add 'discount.to_s' , as: :discount
      t.add :discount_type
      t.add lambda{|e| e.cover(:thumb) }, as: :logo
      t.add lambda{|e| e.expire_on&.strftime('%Y-%m-%d')}, as: :expire_on
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base do |t|
      t.add :offer_locations, template: :v1_show, as: :locations
      t.add :description
    end

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add :average_rating
      t.add :images
      t.add :feedback_count
      t.add :attachments,       template: :offer_attachments
      t.add :leatest_feedbacks, as: :feedbacks, template: :v1_leatest_feedbacks
      t.add lambda{|e, o| e.can_add_feedback(o[:current_user])}, as: :can_add_feedback
      t.add :email
      t.add :phone_number
    end

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :featured
      t.add :views_counter
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.remove :title
      t.remove :description
      t.add :title_translations
      t.add :description_translations
      t.add :average_rating
      t.add :feedback_count
      t.add :pictures,      as: :images, template: :v1_admin_show
      t.add :sub_category,  template: :base
      t.add :regions,       template: :base
      t.add :attachments,   template: :offer_attachments
      t.add :featured
      t.add :offer_locations, template: :v1_show, as: :locations
      t.add :email
      t.add :phone_number
      t.add :created_by, template: :base
      t.add :created_at
    end
  end

  ## ---------------------- Methods related to presenter ----------------------- ##

  def leatest_feedbacks
    feedbacks.order(created_at: :desc).limit(3)
  end

  def feedback_count
    feedbacks.count
  end

  def can_add_feedback(current_user)
    !feedbacks.pluck(:employee_id).include?(current_user.id)
  end

end
