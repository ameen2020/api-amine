module OfferFeedbackPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :rating
      t.add :comment
      t.add :category, template: :base
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base do |t|
      t.add 'employee.full_name' , as: :name
    end

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add 'employee.id' ,        as: :employee_id
      t.add 'employee.full_name' , as: :employee_name
    end

    api_accessible :v1_leatest_feedbacks, extend: :base do |t|
      t.add 'employee.full_name' , as: :name
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index

  end
end
