module NotificationsHistoryPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api
    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base do |t|
      t.add :push_type
      t.add 'notifiable', template: :v1_index, as: :data
      t.add :read_before_type_cast, as: :read
      t.add lambda{|e| e.created_at&.strftime('%Y-%m-%d %H:%M:%S') }, as: :time
    end

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base

    api_accessible :v1_admin_show, extend: :v1_admin_index

  end
end
