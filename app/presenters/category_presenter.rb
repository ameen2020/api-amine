module CategoryPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base do |t|
      t.add :title
      t.add :description
      t.add :logo_url, as: :logo
      t.add :category_constant, as: :const
      t.add :category_counter, as: :new
      t.add :category_color, as: :color
    end

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :title_translations
      t.add :description_translations
      t.add :logo_url, as: :logo
      t.add :category_constant
      t.add :position
      t.add :status
      t.add :category_color
    end

    api_accessible :notification_settings do |t|
      t.add :title
      t.add :category_key, as: :key
      t.add lambda{ |e, o| o[:active_settings].include?(e.category_key)}, as: :value
    end

    def category_key
      category_constant.downcase.singularize
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index
  end
end
