# frozen_string_literal: true

module AdminNotificationPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add ->(n) { n.noticeable&.title }, as: :title
      t.add ->(n) { n.noticeable&.id },    as: :record_id
      t.add ->(n) { n.noticeable&.model_name&.human }, as: :category
      # t.add :noticeable, template: :base, as: :record
      t.add :status_before_type_cast, as: :status
      t.add :employee, template: :base
      t.add :created_at
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index
  end
end
