module EmployeeVoicePresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :message
      t.add :voice_category
      t.add :attachment_url
      t.add :phone_number
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##
    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :replied?, as: :replied
      t.add :employee, template: :base
      t.add ->(r) { r.created_at.strftime('%F %r') }, as: :created_at
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.add :employee_id
      t.add :admin, template: :base
      t.add :reply_message
      t.add :replied_at
      t.add ->(r) { r.replied_at&.strftime('%F %r') }, as: :replied_at
      t.add :attachment_admin_url
    end
  end
end
