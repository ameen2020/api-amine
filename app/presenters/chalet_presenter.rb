module ChaletPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :title
      t.add :description
      t.add lambda{|e| e.start_date&.strftime('%Y-%m-%d')}, as: :start_date
      t.add lambda{|e| e.end_date&.strftime('%Y-%m-%d')}, as: :end_date
      t.add lambda{|e| e.cover(:thumb) }, as: :logo
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base do |t|
      t.add :city
      t.add lambda{|e| e.created_at&.strftime('%Y-%m-%d')}, as: :date_of_adding
    end

    api_accessible :v1_show, extend: :v1_index do |t|
      t.add :city
      t.add :address
      t.add :location
      t.add :latitude
      t.add :longitude
      t.add :attachment_url, as: :attachment
      t.add :images, template: :v1_show
      t.add ->(e, o) { e.registration_tag(o[:current_user]) }, as: :registration_tag
    end

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :city
      t.add :views_counter
      t.add :pending_registrations_count, as: :pending_registrations
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.remove :title
      t.remove :description
      t.add :title_translations
      t.add :description_translations
      t.add :city_translations
      t.add :address_translations
      t.add :location_translations
      t.add :registered_count
      t.add :latitude
      t.add :longitude
      t.add :regions,       template: :base
      t.add :attachment_url, as: :attachment
      t.add :attachment_file_name,    as: :file_name
      t.add :attachment_content_type, as: :file_type
      t.add :pictures, as: :images, template: :v1_admin_show
      t.add :created_by, template: :base
      t.add :created_at
      t.add ->(e) { e.created_at.strftime('%F %r') }, as: :date_of_adding
    end
  end

  # Registration status (if user can register, registered or can cancel)
  def registration_tag(current_user)
    registered_employees = employees.pluck(:id)
    registered           = registered_employees.include?(current_user.id)

    {
      registered:     registered,
      can_register:   can_register? && !registered,
      can_unregister: can_cancel? && registered,
      is_rejected: registered && rejected(current_user)
    }
  end
  def registered_count
    chalet_registrations.count || 0
  end

  def rejected(current_user)
    registr = current_user.chalet_registrations.find_by(chalet_id: id)
    (registr.super_admin_rejected? && registr.sub_admin_rejected?) || registr.super_admin_rejected? ||
      (registr.sub_admin_rejected? && !registr.super_admin_accepted?)
  end
end
