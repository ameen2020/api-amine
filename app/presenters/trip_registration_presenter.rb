# frozen_string_literal: true

module TripRegistrationPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add 'employee.id', as: :employee_id
      t.add 'employee.full_name', as: :employee_name
      t.add :full_name
      t.add :id_number
      t.add :super_admin_approve_before_type_cast, as: :super_admin_approve
      t.add :sub_admin_approve_before_type_cast, as: :sub_admin_approve
      t.add :attachments, template: :v1_admin_index
      t.add :companion
      t.add :register_code
      t.add ->(r) { r.created_at.strftime('%F %r') }, as: :created_at
      t.add ->(x) { x.relationship&.title }, as: :relationship
      t.add ->(r, o) { o[:blocked_employees].include?(r.employee_id) }, as: :is_blocked
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index
  end
end
