# frozen_string_literal: true

module EventPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
      t.add :title
      t.add :description
      t.add ->(e) { e.cover(:thumb) }, as: :logo
      t.add ->(e) { e.start_date.strftime('%Y-%m-%d') }, as: :start_date
      t.add ->(e) { e.end_date&.strftime('%Y-%m-%d') }, as: :end_date
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base do |t|
      t.add :city
    end

    api_accessible :v1_show, extend: :v1_index do |t|
      t.remove :date
      t.add :location
      t.add :address
      t.add :latitude
      t.add :longitude
      t.add :remaining
      t.add :locations
      t.add :regions, template: :base
      t.add :images, template: :v1_show
      t.add ->(e, o) { e.registration_tag(o[:current_user]) }, as: :registration_tag
    end

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :city
      t.add :views_counter
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index do |t|
      t.remove :title
      t.remove :description
      t.remove :date
      t.remove :city
      t.add :title_translations
      t.add :description_translations
      t.add :address_translations
      t.add :city_translations
      t.add :location_translations
      t.add :remaining
      t.add :capacity
      t.add 'event_registrations.count', as: :registered_count
      t.add :latitude
      t.add :longitude
      t.add :regions,  template: :base
      t.add :pictures, as: :images, template: :v1_admin_show
      t.add :cover, as: :logo, template: :v1_show
      t.add ->(e) { e.last_cancel&.strftime('%Y-%m-%d') }, as: :last_cancel
      t.add ->(e) { e.created_at.strftime('%F %r') }, as: :created_at
      t.add :locations
      t.add :created_by, template: :base
      t.add :created_at
    end
  end

  # Registration status (if user can register, registered or can cancel)
  def registration_tag(current_user)
    registered_employees = employees.pluck(:id)
    registered           = registered_employees.include?(current_user.id)

    {
      registered:     registered,
      can_register:   can_register? && !registered,
      can_unregister: can_cancel? && registered
    }
  end

  def latitude
    locations[0]&.fetch('latitude', 0.0)
  end

  def longitude
    locations[0]&.fetch('longitude', 0.0)
  end
end
