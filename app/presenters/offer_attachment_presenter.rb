module OfferAttachmentPresenter
  extend ActiveSupport::Concern

  included do
    acts_as_api

    ## ------------------ APIs Accessible ------------------- ##

    api_accessible :base do |t|
      t.add :id
    end

    ## ----------------------- Users ------------------------ ##

    api_accessible :v1_index, extend: :base

    api_accessible :v1_show, extend: :v1_index

    ## ----------------------- Admin ------------------------ ##

    api_accessible :v1_admin_index, extend: :base do |t|
      t.add :attachment_url,          as: :file_url
      t.add :attachment_file_name,    as: :file_name
      t.add :attachment_content_type, as: :file_type
    end

    api_accessible :v1_admin_show, extend: :v1_admin_index

    api_accessible :offer_attachments, extend: :base do |t|
      t.add :attachment_file_name,    as: :file_name
      t.add :attachment_content_type, as: :mime_type
      t.add :attachment_url,          as: :file_url
    end
  end
end
