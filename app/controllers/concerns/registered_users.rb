module RegisteredUsers
  extend ActiveSupport::Concern

  ## ------------------------------------------------------------ ##

  # POST : /v1/events/:id/registration/send_email
  def send_email
    return if missing_params!([:message])

    employees = get_scope.pluck(:employee_id).uniq if params[:all_employees]
    employee_ids = employees || params[:employee_ids]
    return render_error(message: I18n.t('registrations.not_found_employee_id')) unless employee_ids

    employee_ids.each do |id|
      EmployeeMailer.send_message_to_email(id, current_user, params[:message]).deliver_later
    end
    render_success(I18n.t('registrations.send_email_success'))
  end

end
