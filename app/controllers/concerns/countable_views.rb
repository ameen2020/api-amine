module CountableViews
  extend ActiveSupport::Concern

  included do
    before_action :increase_views,  only: %i[show], if: -> { @access == 'app' }
  end

  protected

  def increase_views
    get_resource.increase_views!
  end

end
