module PicturesActions
  extend ActiveSupport::Concern
  included do
    before_action :set_resource, only: %i[show update destroy set_cover add_image remove_image]
  end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/events/:id/set_cover
  def set_cover
    return if missing_params!([:image_id])
    if get_resource.set_cover(params[:image_id])
      message = action_message[:set_cover] || I18n.t(:x_update_successfully, name: 'Cover')
      render_data({ resource_node.singularize.to_sym => get_resource.as_api_response(show_template, template_injector) }, message: message)
    else
      render_unprocessable_entity(error: 1010, message: 'Cant Change Cover')
    end
  end

  ## ------------------------------------------------------------ ##

  # POST : /v1/events/:id/images
  def add_image
    if get_resource.update(image_params)
      message = action_message[:set_cover] || I18n.t(:x_update_successfully, name: 'Images')
      render_data({ resource_node.singularize.to_sym => get_resource.as_api_response(show_template, template_injector) }, message: message)
    else
      render_unprocessable_entity(error: get_resource)
    end
  end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/events/:id/images
  def remove_image
    return if missing_params!([:image_id])
    if get_resource.remove_picture(params[:image_id])
      message = action_message[:remove_image] || I18n.t(:x_deleted_successfully, name: 'Image')
      render_data({ resource_node.singularize.to_sym => get_resource.as_api_response(show_template, template_injector) }, message: message)
    else
      render_unprocessable_entity(error: 1010, message: 'Cant Remove Image')
    end
  end

  ## ------------------------------------------------------------ ##

  protected

  def image_params
    result = params.permit(image_files: [])

    result[:pictures_attributes] = []
    if params[:image_files].present?
      params[:image_files].each do |file|
        result[:pictures_attributes] << {attachment: process_base64(file)}
      end
      result.delete(:image_files)
    end

    result.permit!
  end

  def pictures_params_processed
    resource_params

    if params[:cover_file].present?
      resource_params[:cover_picture_attributes] = {attachment: process_base64(params[:cover_file])}
      resource_params.delete(:cover_file)
    end

    if params[:action] == "create"
      resource_params[:pictures_attributes] = []
      if params[:image_files].present?
        params[:image_files].each do |file|
          resource_params[:pictures_attributes] << {attachment: process_base64(file)}
        end
        resource_params.delete(:image_files)
      end
    end

    resource_params
  end
end
