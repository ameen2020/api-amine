class V1::AdminNotificationsController < V1::BaseController
  power :admin_notifications, map: {
    [:index]   => :admin_notifications_index,
    [:show]    => :admin_notifications_show
  }, as: :admin_notifications_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/admin_notifications/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/admin_notifications/:id
  # Inherited from V1::BaseController
  def show
    return render_not_found if get_resource.nil?
    get_resource.seen!
    render_data(resource_node.singularize.to_sym => get_resource.as_api_response(show_template, template_injector))
  end

  ## ------------------------------------------------------------ ##

  private

  # Search filters
  def search_params
    {
      id_eq: params[:id]
    }
  end

  # Custom ordering and sorting
  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
