class V1::ChaletRegistrationController < V1::BaseController
  include RegisteredUsers

  power :chalet_registrations, context: :chalet_id, map: {
    [:index]   => :chalet_registration_index,
    [:create]  => :creatable_chalet_registration,
    [:destroy] => :destroyable_chalet_registration,
    [:approve] => :updataable_chalet_registration,
    [:send_email] => :send_emailable_chalet_registration
  }, as: :chalet_registrations_scope

  before_action :set_resource, only: [:approve]
  ## ------------------------------------------------------------ ##

  # GET : /v1/chalet_registrations/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/chalet_registrations/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/chalet_registrations/:id
  def destroy
    return if @access == 'admin' && missing_params!([:employee_id])
    chalet_registrations = get_scope.find_by!(employee_id: employee_id)
    if chalet_registrations.destroy
      data = { resource_node => { remaining: chalet_registrations.chalet} }
      render_success(action_message[:destroy] || I18n.t(:x_deleted_successfully, name: resource_name.titleize), data: data)
    else
      render_bad_request(error: chalet_registrations)
    end
  end
  ## ------------------------------------------------------------ ##

  # POST: v1/chalets/:id/registration/:id/approve
  def approve
    if get_resource.update(approval_params)
      message = 'Registration approval updated'
      render_data({ resource_node.singularize.to_sym => get_resource.as_api_response(show_template, template_injector) }, message: message)
    else
      render_unprocessable_entity(error: get_resource)
    end
  end


  ## ------------------------------------------------------------ ##

  private

  def chalet_id
    params[:chalet_id] || ''
  end

  def employee_id
    @access == 'admin' ? params[:employee_id] : @current_user.id
  end

  def params_processed
    {
      attachment:  params[:attachment],
      chalet_id:    chalet_id,
      employee_id: @current_user.id
    }
  end

  def action_message
    {
      create:  I18n.t('registrations.register_success'),
      destroy: I18n.t('registrations.cancel_success')
    }
  end

  def approval_params
    if @current_user.admin? || @current_user.admin2?
      return {
        super_admin_approve: params[:status],
        super_admin_comment: params[:comment],
      }
    elsif @current_user.sub_admin?
      return {
        sub_admin_approve: params[:status],
        sub_admin_comment: params[:comment]
      }
    else
      return {}
    end
  end

  def search_params
    {
      g: [
        {
          sub_admin_approve_eq:   params[:sub_admin],
          super_admin_approve_eq: params[:super_admin],
          register_code_eq:       params[:register_code],
        },
        {
          employee_id_eq:           params[:employee],
          employee_first_name_cont: params[:employee],
          employee_last_name_cont:  params[:employee],
          m: 'or'
        }
      ]
    }
  end

  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end

  def resource_node
    params[:action] != 'index' ? 'chalet' : super
  end
end
