class V1::AssignmentsController < V1::BaseController
  include PicturesActions
  include CountableViews

  power :assignments, map: {
  [:index]   => :assignment_index,
  [:show]    => :assignment_show,
  [:create]  => :creatable_assignment,
  [:destroy] => :destroyable_assignments,
  [:update, :set_cover, :add_image, :remove_image]  => :updatable_assignment
}, as: :assignments_scope


  ## ------------------------------------------------------------ ##

  # GET : /v1/assignments/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/assignments/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/assignments/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/assignments/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##
  def assignment_params
    attrs = params.permit( :date, :email, title_translations: I18n.available_locales,
                   description_translations: I18n.available_locales )
    attrs.merge(
      current_user: @current_user
    )
  end

  def params_processed
    pictures_params_processed.permit!
  end

  def search_params
    search = {}
    search[:date_eq]           = params[:date]

    if @access == 'admin'
      search[:id_eq]              = params[:id]

      if params[:status].present?
        search[:date_lt]   = Date.today if params[:status].to_i == 0
        search[:date_gteq] = Date.today if params[:status].to_i == 1
      end

      search[:translations_cont]  = params[:search]
    end

    search
  end

  def get_order
    return "title_translations->>'#{I18n.locale}' #{sort_direction}" if params[:sort] == 'title'
    { sort_column(:date) => sort_direction(:desc) }
  end
end
