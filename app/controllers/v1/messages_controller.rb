class V1::MessagesController < V1::BaseController
  power :messages, map: {
    [:index]   => :messages_index,
    [:show]    => :message_show,
    [:create]  => :creatable_message,
    [:update]  => :updatable_message,
    [:destroy] => :destroyable_message
  }, as: :messages_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/messages/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/messages/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/messages/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/messages/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/messages/:id
  # Inherited from V1::BaseController
  # def destroy; end

  ## ------------------------------------------------------------ ##

  private

  # Whitelist parameters
  def messages_params
    params.permit(
      :title,
      :message,
      :all_employees,
      employee_ids: []
    )
  end

  def params_processed
    messages_params.merge!(
      sender_id: current_user.id
    )
  end

  # Search filters
  def search_params
    {
      id_eq:            params[:id],
      tilte_cont:       params[:title],
      sender_id_eq:     params[:sender_id]
    }
  end

  # Custom ordering and sorting
  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
