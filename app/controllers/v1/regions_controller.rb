class V1::RegionsController < V1::BaseController
  power :regions, map: {
    [:index]   => :region_index,
    [:show]    => :region_show,
    [:create]  => :creatable_region,
    [:update]  => :updatable_region,
    [:destroy] => :destroyable_region
  }, as: :regions_scope
    ## ------------------------------------------------------------ ##
    # GET : /v1/regions/
    # Inherited from V1::BaseController
    # def index; end

    ## ------------------------------------------------------------ ##

    # POST : /v1/regions/
    # Inherited from V1::BaseController
    # def create; end

    ## ------------------------------------------------------------ ##

    # GET : /v1/regions/:id
    # Inherited from V1::BaseController
    # def show; end

    ## ------------------------------------------------------------ ##

    # PUT : /v1/regions/:id
    # Inherited from V1::BaseController
    # def update; end

    ## ------------------------------------------------------------ ##
    private

    def search_params
      search = {}
      search[:id_eq]              = params[:id]
      search[:ashanak_eq]         = params[:ashanak]
      search[:translations_cont]  = params[:search]
      search
    end

    def region_params
      params.permit(:position, :ashanak, title_translations: I18n.available_locales)
    end

    def get_order
      { sort_column(:position) => sort_direction }
    end
end
