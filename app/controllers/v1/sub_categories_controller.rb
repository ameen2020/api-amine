class V1::SubCategoriesController < V1::BaseController
  power :sub_categories, map: {
    [:index]   => :sub_category_index,
    [:show]    => :sub_category_show,
    [:create]  => :creatable_sub_category,
    [:update]  => :updatable_sub_category,
    [:destroy] => :destroyable_sub_category
  }, as: :sub_categories_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/sub_categories/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/sub_categories/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/sub_categories/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/sub_categories/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  private

  def search_params
    search = {}
    search[:id_eq]              = params[:id]
    search[:translations_cont]  = params[:search]
    search
  end

  def get_order
    # { sort_column(:position) => sort_direction }
    return "#{params[:sort]}_translations->>'#{I18n.locale}' #{sort_direction}" if ['title'].include?(params[:sort])

    # In case position is multi leagues shares the same position number, sort them by name
    "#{sort_column(:position)} #{sort_direction}, \
    title_translations->>'#{I18n.locale}' #{sort_direction}"
  end

  def sub_category_params
    params.permit(:position, logo: [:name, :type, :size, :base64],
                  title_translations: I18n.available_locales)
  end

  # Process params and add logo after processing
  def params_processed
    resource_params
    resource_params[:logo] = process_base64(params[:logo]) if params[:logo].present?
    resource_params
  end
end
