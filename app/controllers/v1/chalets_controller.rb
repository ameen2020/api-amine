class V1::ChaletsController < V1::BaseController

  before_action :set_resource, only: %i[show update destroy set_cover add_image remove_image export_registerations]

  include PicturesActions
  include CountableViews

  power :chalets, map: {
    [:index]   => :chalet_index,
    [:show]    => :chalet_show,
    [:create]  => :creatable_chalet,
    [:update]  => :updatable_chalet,
    [:destroy] => :destroyable_chalet,
    [:update, :set_cover, :add_image, :remove_image]  => :updatable_chalet,
    [:export_registerations] => :export_chalet_registerations
  }, as: :chalets_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/chalets/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/chalets/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/chalets/:id
  # Inherited from V1::BaseController
  # def show; end

  ## -----------set_resource------------------------------------------------- ##

  # PUT : /v1/chalets/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/chalets/:id
  # Inherited from V1::BaseController
  # def destroy; end

  ## ------------------------------------------------------------ ##

  private

  # Whitelist parameters
  def chalet_params
      attrs = params.permit(:start_date, :end_date,
              :longitude,:latitude, region_ids: [],
              attachment_file: [],
              city_translations: I18n.available_locales,
              title_translations: I18n.available_locales,
              location_translations: I18n.available_locales,
              description_translations: I18n.available_locales,
              address_translations: I18n.available_locales)
      attrs.merge(
        current_user: @current_user
      )
  end

  def params_processed
    results = pictures_params_processed
    results[:attachment] = process_base64(params[:attachment_file]) if params[:attachment_file].present?
    results.delete(:attachment_file)

    results.permit!
  end

  def date_param
    params[:date] || Date.today.to_s
  end

  # search for events by date
  def search_params
    search = {}
    search[:start_date_eq] = date_param if @current_user.employee? || params[:date].present?
    search[:regions_id_in] = params[:regions].is_a?(String) ? params[:regions]&.split(',') : params[:regions]

    if @access == 'admin'
      search[:id_eq]             = params[:id]

      if params[:status].present?
        search[:start_date_lt]   = Date.today if params[:status].to_i == 0
        search[:start_date_gteq] = Date.today if params[:status].to_i == 1
      end

      search[:translations_cont] = params[:search]
    end

    search
  end

  # Inject render template with current user
  def template_injector
    {
      current_user: @current_user
    }
  end

  def get_collection
    super.distinct
  end

  def get_order
    return "chalets.title_translations->>'#{I18n.locale}' #{sort_direction}" if params[:sort] == 'title'
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
