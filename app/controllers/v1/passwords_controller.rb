# frozen_string_literal: true
class V1::PasswordsController < V1::ApiController
  power :passwords, map: {
    [:update]  => :updatable_password,
    [:forgot]  => :forgotable_password,
    [:recover] => :recoverable_password
  }

  skip_before_action :authenticate_request!, except: [:update]

  ## ------------------------------------------------------------ ##

  # PUT : /v1/passwords/
  def update
    return if missing_params!([:old_password, :password])
    return render_bad_request(error: 1404) unless @current_user.authenticate(params[:old_password])
    if @current_user.update(password_params)
      return render_success('Password Updated Successfully')
    else
      return render_bad_request(error: @current_user)
    end
  end

  ## ------------------------------------------------------------ ##

  # POST : /v1/passwords/forgot
  def forgot
    return if missing_params!([:email])
    @user = Employee.find_by_email(params[:email])

    if @user.present? && @user.forgot_password
      return render_success('Recover password email has been sent.')
    else
      return render_bad_request(error: 1401)
    end
  end

  ## ------------------------------------------------------------ ##

  # POST : /v1/passwords/recover
  def recover
    return if missing_params!([:password_token, :password])

    @user = Employee.find_by(reset_password_token: params[:password_token])

    return render_bad_request(error: 1402) unless @user.present?

    return render_bad_request(error: 1403) unless @user.password_token_valid?

    if @user.update(password_params) && @user.clean_reset_password!
      return render_success('Password Updated Successfully, please login')
    else
      return render_bad_request(error: @user)
    end
  end

  ## ------------------------------------------------------------ ##

  private

  def password_params
    params.permit(:password)
  end
end
