class V1::ProfilesController < V1::BaseController
  power :profiles, map: {
    [:events]   => :profile_events,
    [:trips]   => :profile_trips,
    [:chalets] => :profile_chalets
  }, as: :profiles_scope

  before_action :set_pagination, only: [:events]

  ## -------------------------------------------------- ##

  # GET : v1/profiles
  def index
    render_data(employee: get_scope.as_api_response(:v1_profile))
  end

  ## -------------------------------------------------- ##

  # GET : v1/profiles/events
  def events
    data = { events: get_collection.as_api_response(index_template, template_injector) }
    render_data(data, meta: pagination(get_collection), message: events_message(get_collection))
  end

  #GET: v1/profiles/trips
  def trips
    data = { trips: get_collection.as_api_response(index_template, template_injector) }
    render_data(data, meta: pagination(get_collection), message: trips_message(get_collection))
  end

  def chalets
    data = { chalets: get_collection.as_api_response(index_template, template_injector) }
    render_data(data, meta: pagination(get_collection), message: chalets_message(get_collection))
  end

  ## -------------------------------------------------- ##

  # POST : /v1/profiles/change_phone_number
  def change_phone_number
    return if missing_params!([:new_phone_number])
    get_scope.generate_otp
    get_scope.new_phone_number = params[:new_phone_number]
    if get_scope.save && get_scope.send_otp!
      data = Rails.env.production? ? {} : { otp: get_scope.otp }
      render_success(I18n.t('user.otp.generated'), data: data)
    else
      render_unprocessable_entity(error: get_scope) if get_scope.errors.any?
    end
  end

  ## -------------------------------------------------- ##

  # POST : /v1/profiles/confirm_otp
  # This method will confirm otp that sent while phone number changing and update phone number
  def confirm_otp
    return if missing_params!([:otp])
    @employee = Employee.confirm_otp(get_scope.phone_number, params[:otp])
    return render_bad_request(error: 1307) unless @employee.present?

    @employee.phone_number = @employee.new_phone_number
    @employee.new_phone_number = nil

    if @employee.save
      render_success(I18n.t('user.phone_number.changed'), data: @employee.phone_number)
    else
      render_bad_request(error: 1405)
    end
  end

  ## -------------------------------------------------- ##

  private

  def events_message(collection)
    I18n.t(collection.size.zero? ? 'user.events.no_events' : 'data_found')
  end

  def trips_message(collection)
    I18n.t(collection.size.zero? ? 'user.trips.no_trips' : 'data_found')
  end

  def chalets_message(collection)
    I18n.t(collection.size.zero? ? 'user.chalets.no_chalets' : 'data_found')
  end
end
