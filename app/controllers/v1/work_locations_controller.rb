class V1::WorkLocationsController < V1::BaseController
  power :work_locations, map: {
    [:index]   => :work_locations_index,
    [:show]    => :work_location_show,
    [:create]  => :creatable_work_location,
    [:update]  => :updatable_work_location,
    [:destroy]  => :destroyable_work_location
  }, as: :work_locations_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/regions/:region_id/work_locations
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/regions/:region_id/work_locations
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/regions/:region_id/work_locations/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/regions/:region_id/work_locations/:id
  # Inherited from V1::BaseController
  # def update; end

  # DELETE : /v1/regions/:region_id/work_locations/:id
  # Inherited from V1::BaseController
  # def update; end

  private

  def work_location_params
    params.permit(:region_id, title_translations: I18n.available_locales)
  end

  def search_params
    search = {}
    search[:id_eq]             = params[:id]
    search[:region_id_eq]      = params[:region_id]
    search[:translations_cont] = params[:search]
    search
  end
end
