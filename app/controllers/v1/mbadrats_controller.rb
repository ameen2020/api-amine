class V1::MbadratsController < V1::BaseController
  before_action :set_resource, only: %i[show update destroy set_cover add_image remove_image]

  include PicturesActions
  include CountableViews

  power :mbadrats, map: {
    [:index]   => :mbadrat_index,
    [:show]    => :mbadrat_show,
    [:create]  => :creatable_mbadrat,
    [:update]  => :updatable_mbadrat,
    [:destroy] => :destroyable_mbadrat,
    [:update, :set_cover, :add_image, :remove_image]  => :updatable_mbadrat
  }, as: :mbadrats_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/mbadrats/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/mbadrats/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/mbadrats/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/mbadrats/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/mbadrats/:id
  # Inherited from V1::BaseController
  # def destroy; end

  ## ------------------------------------------------------------ ##

  private

  # Whitelist parameters
  def mbadrat_params
  params.permit(:video, attachment_file: [],
                title_translations: I18n.available_locales,
                description_translations: I18n.available_locales,
                mbadrat_faqs_attributes: [:id, :_destroy, title_translations: I18n.available_locales,description_translations: I18n.available_locales])
  end

  def params_processed
    results = pictures_params_processed
    results[:attachment] = process_base64(params[:attachment_file]) if params[:attachment_file].present?
    results.delete(:attachment_file)

    results.permit!
  end

  # Search filters
  def search_params
    search = {}
    search[:id_eq]             = params[:id]
    search[:translations_cont] = params[:search]
    search
  end

  # Custom ordering and sorting
  def get_order
    return "title_translations->>'#{I18n.locale}' #{sort_direction}" if params[:sort] == 'title'
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
