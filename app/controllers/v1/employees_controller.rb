class V1::EmployeesController < V1::BaseController
  power :employees, map: {
    [:index]   => :employee_index,
    [:show]    => :employee_show,
    [:create]  => :creatable_employee,
    [:update]  => :updatable_employee,
    [:destroy] => :destroyable_employee,
    [:export]  => :exportable_employees
  }, as: :employees_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/employees/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/employees/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/employees/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/employees/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/employees/export
  def export
    @limit = 999_999_999
    data = Exporter::Export.new(collection: get_collection, encoding: 'UTF-8').export
    send_data(data, filename: 'members.xls', type: 'application/xls', disposition: 'inline')
  end

  ## ------------------------------------------------------------ ##

  private

  # refer to current employees if route id = self
  def id_parameter
    params[:id] == 'self' ? @current_user.id : params[:id]
  end

  def employee_params
    main_params = %i[first_name last_name email]
    main_params.push(:status, :role) if @access == 'admin'
    params.permit(main_params)
  end

  # search for employees by params
  def search_params
    {
      g: [
        {
          status_eq:         params[:status],
          role_eq:           params[:role],
          created_at_gteq:   params[:from].present? ? Date.parse(params[:from])&.beginning_of_day : nil,
          created_at_lteq:   params[:to].present?   ? Date.parse(params[:to])&.end_of_day         : nil
        },
        {
          id_eq:             params[:id],
          first_name_cont:   params[:search],
          last_name_cont:    params[:search],
          email_cont:        params[:search],
          phone_number_cont: params[:search],
          m: 'or'
        }
      ]
    }
  end

  def get_order
    { sort_column(:id) => sort_direction(:asc) }
  end
end
