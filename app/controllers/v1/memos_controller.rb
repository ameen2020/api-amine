class V1::MemosController < V1::BaseController

  include PicturesActions
  include CountableViews

  power :momos, map: {
  [:index]   => :memo_index,
  [:show]    => :memo_show,
  [:create]  => :creatable_memo,
  [:destroy] => :destroyable_memo,
  [:update, :set_cover, :add_image, :remove_image] => :updatable_memo
}, as: :memos_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/memos/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/memos/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/memos/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/memos/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##
  private

  def memo_params
    attrs = params.permit( :date, title_translations: I18n.available_locales,
                   description_translations: I18n.available_locales )
    attrs.merge(
      current_user: @current_user
    )
  end

  def params_processed
    pictures_params_processed.permit!
  end

  def search_params
    search = {}
    search[:date_eq] = params[:date]

    if @access == 'admin'
      search[:id_eq]              = params[:id]

      if params[:status].present?
        search[:date_lt]   = Date.today if params[:status].to_i == 0
        search[:date_gteq] = Date.today if params[:status].to_i == 1
      end

      search[:translations_cont]  = params[:search]
    end

    search
  end

  def get_order
    return "title_translations->>'#{I18n.locale}' #{sort_direction}" if params[:sort] == 'title'
    { sort_column(:date) => sort_direction(:desc) }
  end
end
