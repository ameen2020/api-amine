class V1::MagazinesController < V1::BaseController

  include PicturesActions
  include CountableViews

  power :magazines, map: {
    [:index]   => :magazine_index,
    [:show]    => :magazine_show,
    [:create]  => :creatable_magazine,
    [:destroy] => :destroyable_magazine,
    [:update, :set_cover, :add_image, :remove_image]  => :updatable_magazine,
  }, as: :magazines_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/magazines/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/magazines/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/magazines/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/magazines/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  private

  def resource_class
    @resource_class ||= Magazin
  end

  def magazine_params
    attrs = params.permit(attachment_file: [], title_translations: I18n.available_locales,
                   description_translations: I18n.available_locales )
    attrs.merge(
      current_user: @current_user
    )
  end

  def params_processed
    results = pictures_params_processed
    results[:attachment] = process_base64(params[:attachment_file]) if params[:attachment_file].present?
    results.delete(:attachment_file)

    results.permit!
  end

  def search_params
    search = {}
    search[:id_eq]             = params[:id]
    search[:translations_cont] = params[:search]
    search
  end

  def get_order
    return "title_translations->>'#{I18n.locale}' #{sort_direction}" if params[:sort] == 'title'
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
