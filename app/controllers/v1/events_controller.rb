class V1::EventsController < V1::BaseController

  before_action :set_resource, only: %i[show set_cover add_image remove_image export_registerations]

  include PicturesActions
  include CountableViews

  power :events, map: {
    [:index]    => :event_index,
    [:calendar] => :events_calendar,
    [:show]    => :event_show,
    [:create]  => :creatable_event,
    [:destroy] => :destroyable_event,
    [:update, :set_cover, :add_image, :remove_image]  => :updatable_event,
    [:export_registerations] => :export_event_registerations
  }, as: :events_scope


  ## ------------------------------------------------------------ ##
  # GET : /v1/events/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/events/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/events/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/events/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/events/calendar
  def calendar
    return if missing_params!([:year, :month])
    dates = Event.get_calender(params[:year], params[:month])
    render_data(
      current_date: Date.today.strftime('%Y-%m-%d'),
      dates:        dates.map{ |t| t.strftime('%Y-%m-%d') }.sort
    )
  end

  ## ------------------------------------------------------------ ##

  # GET : /v1/events/:id/export_registerations
  def export_registerations
    data = Exporter::Export.new(collection: get_resource.event_registrations.includes(:employee), encoding: 'UTF-8').export
    send_data(data, filename: "event-#{params[:id]}-registerations.xls", type: 'application/xls', disposition: 'inline')
  end

  ## ---------------------private methods------------------------------ ##

  private

  # Override the main method in BaseController
  def index_message(collection)
    collection.to_a.size.zero? ? I18n.t('events.no_events_found') : I18n.t('data_found')
  end

  def event_params
    attrs = params.permit(:start_date, :end_date, :last_cancel, :capacity,# :latitude, :longitude,
                  region_ids: [],
                  locations: [:latitude, :longitude],
                  city_translations: I18n.available_locales,
                  title_translations: I18n.available_locales,
                  address_translations: I18n.available_locales,
                  location_translations: I18n.available_locales,
                  description_translations: I18n.available_locales)
    attrs.merge(
      current_user: @current_user
      )
  end

  def params_processed
    pictures_params_processed.permit!
  end

  def date_param
    params[:date] || Date.today.to_s
  end

  # search for events by date
  def search_params
    search = {}
    search[:start_date_eq] = date_param if @current_user.employee? || params[:date].present?
    search[:regions_id_in] = params[:regions].is_a?(String) ? params[:regions]&.split(',') : params[:regions]

    if @access == 'admin'
      search[:id_eq]             = params[:id]

      if params[:status].present?
        search[:start_date_lt]   = Date.today if params[:status].to_i == 0
        search[:start_date_gteq] = Date.today if params[:status].to_i == 1
      end

      search[:translations_cont] = params[:search]
    end

    search
  end

  # Inject render template with current user
  def template_injector
    {
      current_user: @current_user
    }
  end

  def get_collection
    super.distinct
  end

  def get_order
    return "events.title_translations->>'#{I18n.locale}' #{sort_direction}" if params[:sort] == 'title'
    { sort_column(:start_date) => sort_direction(:asc) }
  end
end
