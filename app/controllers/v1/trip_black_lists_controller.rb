class V1::TripBlackListsController < V1::BaseController
  power :trip_black_lists, map: {
    [:index]   => :trip_black_lists_index,
    [:create]  => :creatable_trip_black_list,
    [:destroy] => :destroyable_trip_black_list
  }, as: :trip_black_lists_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/trip_black_lists/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/trip_black_lists/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/trip_black_lists/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/trip_black_lists/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/trip_black_lists/:id
  # Inherited from V1::BaseController
  # def destroy; end

  ## ------------------------------------------------------------ ##

  private

  # Whitelist parameters
  def trip_black_lists_params
    params.permit(
      :employee_id
    )
  end

  def params_processed
    trip_black_lists_params.merge(blocker_id: @current_user.id)
  end

  # Search filters
  def search_params
    {
      g: [
        {
          id_eq:           params[:id],
        },
        {
          employee_id_eq:           params[:search],
          employee_first_name_cont: params[:search],
          employee_last_name_cont:  params[:search],
          m: 'or'
        }
      ]
    }
  end

  # Custom ordering and sorting
  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
