class V1::NotificationsSettingsController < V1::ApiController

  power :notifications_settings
  before_action :create_notifications_setting, only: %i[show update]

  # GET - v1/notifications_settings
  def show
    render_data(@current_user.notifications_setting.as_api_response(:v1_show, template_injector))
  end

  # PUT - v1/notifications_settings
  def update
    @current_user.notifications_setting.update(notifications_setting_params)
    render_data(@current_user.notifications_setting.as_api_response(:v1_show, template_injector),
                message: I18n.t('notifications.updated'))
  end

  private

  def notifications_setting_params
    params.permit(:employee_id, :event, :offer, :trip, :news, :memo, :chalet, :job, :mbadrat, :employee_voice, :installment, :magazine, region:[], sub_category: [])
  end

  # Inject render template with current user
  def template_injector
    {
      current_user: @current_user
    }
  end

  # Create notification settings for any employee who has not settings
  def create_notifications_setting
    @current_user.create_notifications_setting if @current_user.notifications_setting.nil?
  end
end
