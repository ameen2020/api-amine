class V1::VersionsController < V1::BaseController
  power :versions, map: {
    [:index]   => :versions_index,
    [:show]    => :version_show,
    [:options] => :version_options
    # [:create]  => :creatable_versions,
    # [:update]  => :updatable_versions,
    # [:destroy] => :destroyable_versions,

  }, as: :versions_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/versions/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/versions/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/versions/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/versions/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/versions/:id
  # Inherited from V1::BaseController
  # def destroy; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/{resource}/options
  def options
    render_data(get_scope.options(@current_user))
  end

  private


  # Search filters
  def search_params
    {
      g: [
        {
          id_eq:                    params[:id],
          item_type_eq:             params[:category_type],
          event_eq:                 params[:event]
        },
        {
          employee_first_name_cont:   params[:search],
          employee_last_name_cont:    params[:search],
          employee_email_cont:        params[:search],
          employee_phone_number_cont: params[:search],
          m: 'or'
        }
      ]
    }
  end

  # Custom ordering and sorting
  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
