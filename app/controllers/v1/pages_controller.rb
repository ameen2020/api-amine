class V1::PagesController < V1::BaseController

  power :pages, map: {
    [:index]   => :pages_index,
    [:show]    => :page_show,
    [:update]  => :updatable_page
  }, as: :pages_scope

  skip_before_action :authenticate_request!, only: :show
  ## ------------------------------------------------------------ ##

  # GET : /v1/pages/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/pages/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/pages/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/pages/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/pages/:id
  # Inherited from V1::BaseController
  # def destroy; end

  ## ------------------------------------------------------------ ##

  private

  # Whitelist parameters
  def page_params
    params.permit(
      title_translations:   I18n.available_locales,
      content_translations: I18n.available_locales
    )
  end

  # Search filters
  def search_params
    {
      id_eq: params[:id]
    }
  end

  # Custom ordering and sorting
  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
