class V1::OffersController < V1::BaseController
  before_action :authenticate_request!, except: [:generate_pdf]
  before_action :authenticate_by_params!, only: [:generate_pdf]

  before_action :set_resource, only: %i[show update destroy generate_pdf]
  
  include PicturesActions
  include CountableViews

  power :offers, map: {
    [:index]                => :offer_index,
    [:show, :generate_pdf]  => :offer_show,
    [:create]               => :creatable_offer,
    [:destroy]              => :destroyable_offer,
    [:update, :set_cover, :add_image, :remove_image]  => :updatable_offer
  }, as: :offers_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/offers/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/offers/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/offers/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/offers/:id
  # Inherited from V1::BaseController
  # def update; end
  
  ## ------------------------------------------------------------ ##

  # PUT : /v1/offers/:id/generate_pdf
  # Inherited from V1::BaseController
  def generate_pdf
    return if missing_params!([:relative_name])
    data = {
      relative_name:  params[:relative_name],
      employee:       @current_user,
      offer:          set_resource
    }
    pdf = Pdf::Build.new(data: data, type: :invitation).build
    send_data(pdf, filename: 'invitation.pdf', type: 'application/pdf', disposition: 'inline')
  end

  ## ------------------------------------------------------------ ##
  
  def offer_params
    params.permit(
      :sub_category_id, :discount, :expire_on, :discount_type,
      :featured, :email, :phone_number, region_ids: [],
      title_translations: I18n.available_locales,
      description_translations: I18n.available_locales
    )
  end

  def search_params
    search = {}
    search[:featured_eq]        = params[:featured]
    search[:sub_category_id_in] = params[:categories].is_a?(String) ? params[:categories]&.split(',') : params[:categories]
    # params_set_location_or_region(search)
    search[:regions_id_in]      = set_region if params[:regions]

    if @access == 'admin'
      search[:id_eq] = params[:id]

      if params[:status].present?
        search[:expire_on_lt]   = Date.today if params[:status].to_i == 0
        search[:expire_on_gteq] = Date.today if params[:status].to_i == 1
      end

      search[:translations_cont] = params[:search]
    end
    search
  end

  def params_processed
    attrs = pictures_params_processed.permit!
    attrs.merge!(handle_locations).permit!
    attrs.merge!(current_user: @current_user)
    attrs
  end

  def handle_locations
    locations = params.permit(locations: [:latitude, :longitude]).to_h['locations']

    a = get_resource&.offer_locations&.pluck(:id) || []
    e = []
    x = locations.map do |l|
      id = get_resource&.offer_locations&.find_by(l)&.id
      e << id if id.present?
      l.merge(id: id)
    end
    d = a - e
    d.map{ |did| x << {id: did, _destroy: '1' }}
    ActionController::Parameters.new(offer_locations_attributes: x)
                                .permit(offer_locations_attributes: [:latitude, :longitude, :id, :_destroy])
  end

  # Inject render template with current user
  def template_injector
    {
      current_user: @current_user
    }
  end

  def get_collection
    return super.distinct unless params[:longitude] && params[:latitude] && !params[:regions]
    Kaminari.paginate_array(set_offers).page(@page).per(@limit)
  end

  def get_order
    {
      :featured               => :desc,                # Show Featured First
      sort_column(:expire_on) => sort_direction(:asc)  # Apply other sorting
    }
  end

  # Override the main method in BaseController
  def index_message(collection)
    I18n.t(collection.to_a.size.zero? ? 'offers.no_offers' : 'data_found')
  end

  def set_region
    params[:regions].is_a?(String) ? params[:regions]&.split(',') : params[:regions]
  end

  # Set offers locations near it
  # @ Return offers ids
  def set_offers
    @nearby ||= OfferLocation.includes(offer: [:pictures, :cover_picture])
                             .near([params[:latitude], params[:longitude]], 10000, units: :km)
                             .ransack(locations_search_params)
                             .result
                             .map(&:offer)
                             .flatten
                             .uniq
  end

  def locations_search_params
    {
      offer_featured_eq:        params[:featured],
      offer_expire_on_gteq:     Date.today,
      offer_sub_category_id_in: params[:categories].is_a?(String) ? params[:categories]&.split(',') : params[:categories],
    }
  end
end
