class V1::NationalitiesController < V1::BaseController
  power :nationalities, map: {
    [:index]   => :nationality_index,
    [:show]    => :nationality_show,
    [:create]  => :creatable_nationality,
    [:update]  => :updatable_nationality,
    [:destroy] => :destroyable_nationality
  }, as: :nationalities_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/nationalities/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/nationalities/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/nationalities/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/nationalities/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/nationalities/:id
  # Inherited from V1::BaseController
  # def destroy; end

  ## ------------------------------------------------------------ ##

  private

  # Whitelist parameters
  def nationality_params
    params.permit(name_translations: I18n.available_locales)
  end

  # Search filters
  def search_params
    {
      id_eq: params[:id]
    }
  end

  # Custom ordering and sorting
  def get_order
      return "name_translations->>'#{I18n.locale}' #{sort_direction}" if params[:sort] == 'name'
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
