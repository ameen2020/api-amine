class V1::FeedbackCategoriesController < V1::BaseController
  power :feedback_categories, map: {
    [:index]   => :feedback_category_index,
    [:show]    => :feedback_category_show,
    [:create]  => :creatable_feedback_category,
    [:update]  => :updatable_feedback_category,
    [:destroy] => :destroyable_feedback_category
  }, as: :feedback_categories_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/feedback_categories/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/feedback_categories/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/feedback_categories/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/feedback_categories/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  private

  def search_params
    search = {}
    search[:id_eq]              = params[:id]
    search[:translations_cont]  = params[:search]
    search
  end

  def get_order
    { sort_column(:position) => sort_direction }
  end

  def feedback_category_params
    params.permit(:position, title_translations: I18n.available_locales)
  end
end
