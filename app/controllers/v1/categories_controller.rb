class V1::CategoriesController < V1::BaseController
  power :categories, map: {
    [:index]   => :category_index,
    [:show]    => :category_show,
    [:create]  => :creatable_category,
    [:update]  => :updatable_category,
    [:destroy] => :destroyable_category
  }, as: :categories_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/categories/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/categories/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/categories/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/categories/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  private

  # Apply Search filters
  def search_params
    search = {}
    search[:id_eq]              = params[:id]
    search[:translations_cont]  = params[:search]
    search[:status_eq]          = params[:status] if @access == 'admin'
    search
  end

  # Accepted params
  def category_params
    params.permit(:category_constant, :position, :category_color, :category_type, :status,
                  logo: [:name, :type, :size, :base64],
                  title_translations: I18n.available_locales,
                  description_translations: I18n.available_locales)
  end

  # Process params and add logo after processing
  def params_processed
    resource_params
    resource_params[:logo] = process_base64(params[:logo]) if params[:logo].present?
    resource_params
  end

  # Order records
  def get_order
    return "#{params[:sort]}_translations->>'#{I18n.locale}' #{sort_direction}" if %w(title description).include?(params[:sort])

    # In case position is multi leagues shares the same position number, sort them by name
    "#{sort_column(:position)} #{sort_direction}, \
    title_translations->>'#{I18n.locale}' #{sort_direction}"
  end
end
