class V1::NewsController < V1::BaseController

  include PicturesActions
  include CountableViews

  power :news, map: {
    [:index]   => :new_index,
    [:show]    => :new_show,
    [:create]  => :creatable_new,
    [:destroy] => :destroyable_new,
    [:update, :set_cover, :add_image, :remove_image]  => :updatable_new
  }, as: :news_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/news/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/news/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/news/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/news/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  private

  def resource_class
    @resource_class ||= New
  end

  def news_params
    attrs = params.permit( :date, :important, title_translations: I18n.available_locales,
                   description_translations: I18n.available_locales)
    attrs.merge(
      current_user: @current_user
    )
  end

  def params_processed
    pictures_params_processed.permit!
  end

  def search_params
    search = {}
    search[:date_eq] = params[:date]

    if @access == 'admin'
      search[:id_eq]              = params[:id]

      if params[:status].present?
        search[:date_lteq] = Date.today if params[:status].to_i == 0
        search[:date_gt]   = Date.today if params[:status].to_i == 1
      end

      search[:translations_cont]  = params[:search]
    end

    search
  end

  def get_order
    return "title_translations->>'#{I18n.locale}' #{sort_direction}" if params[:sort] == 'title'
    {
      sort_column(:date) => sort_direction(:desc),
      important: :desc
    }
  end
end
