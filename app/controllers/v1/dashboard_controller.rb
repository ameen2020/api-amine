class V1::DashboardController < V1::ApiController
  power :dashboard, map: {
    [:counters] => :dashboard_counters,
    [ :user_registrations,
      :trip_registered_users,
      :events_registered_users,
      :contact_us,
      :number_of_views,
      :offers_baseds,
      :offers,
      :testahels,
      :number_of
    ] => :dashboard_statistics
  }

  def counters
    data = {
      pending_ashanaks: Ashanak.pending_counter(@current_user.role),
      pending_trips:    TripRegistration.pending_counter(@current_user.role),
      pending_chalets:    ChaletRegistration.pending_counter(@current_user.role),
      pending_notifications: AdminNotification.unseen.where(employee_id: @current_user.id).count
    }

    render_data(data)
  end

  # ----------------------------------------------------------------------------------------------- #
  # GET : /v1/dashboard/user_registrations
  def user_registrations
    registrations = Employee.ransack(search_params).result.count

    data = {
      user_registrations: data_values(registrations, 250000)
    }
    render_data(data)
  end

  # ----------------------------------------------------------------------------------------------- #
  # GET : /v1/dashboard/trip_registered_users
  def trip_registered_users
    value = Employee.ransack(search_params).result
                      .joins(:trips)
                      .group(:trip_id, :key)
                      .select("trip_id AS id, trips.title_translations ->> '#{I18n.locale}' AS key","COUNT(*) AS value,ROUND((COUNT(*)/#{tre})*100, 2) AS percentage")
                      .order("percentage desc")

    render_data(trip_registered_users: value.as_api_response(:statistics))
  end

  # ----------------------------------------------------------------------------------------------- #
  # GET : /v1/dashboard/events_registered_users
  def events_registered_users
    value = Employee.ransack(search_params).result
                      .joins(:events)
                      .group(:event_id, :key)
                      .select("event_id AS id, events.title_translations ->> '#{I18n.locale}' AS key","COUNT(*) AS value,ROUND((COUNT(*)/#{tre})*100, 2) AS percentage")
                      .order("percentage desc")

    render_data(events_registered_users: value.as_api_response(:statistics))
  end

  # ----------------------------------------------------------------------------------------------- #
  # GET : /v1/dashboard/contact_us
  def contact_us
    contact_us = ContactUs.ransack(search_params).result
    pending    = contact_us.where(reply_message: nil).or(contact_us.where(admin_id: nil)).count
    inquiries  = contact_us.where.not(reply_message: nil).count

    render_data(
      {
        contact_us_pending:   data_values(pending, contact_us.count),
        contact_us_inquiries: data_values(inquiries, contact_us.count)
      }
    )
  end

  # ----------------------------------------------------------------------------------------------- #
  # GET : /v1/dashboard/number_of_views
  def number_of_views
    magazin     = Magazin.pluck(:views_counter).sum
    assignment  = Assignment.pluck(:views_counter).sum
    news        = New.pluck(:views_counter).sum
    mbadrat     = Mbadrat.pluck(:views_counter).sum

    data = {
      number_of_views: {
        magazin:    magazin,
        assignment: assignment,
        news:       news,
        mbadrat:    mbadrat
      }
    }
    render_data(data)
  end

  # ----------------------------------------------------------------------------------------------- #
  # GET : /v1/dashboard/offers_baseds
  def offers_baseds
    value = OfferFeedback.ransack(search_params).result.group(:rating).count.map do |k, v|
      {
        id:         k.to_i,
        value:      v
      }
    end
    render_data(offers_baseds: value.sort_by { |k| k[:id] })
  end

  # ----------------------------------------------------------------------------------------------- #
  # GET : /v1/dashboard/offers
  def offers
    value   = collection_offers.ransack(search_params).result
                               .where('expire_on > ?', DateTime.now.beginning_of_day).count

    data    = {
      offers_valids: data_values(value, collection_offers.count)
    }
    render_data(data)
  end

  # ----------------------------------------------------------------------------------------------- #
  # GET : /v1/dashboard/testahels
  def testahels
    value   = Ashanak.ransack(search_params).result
    pending = value.where(super_admin_approve: 'pending',sub_admin_approve: 'pending').or(
              value.where(sub_admin_approve: 'accepted' , super_admin_approve: 'pending')).count
    beneficiary = value.where(super_admin_approve: 'accepted').pluck(:employee_id).uniq.count

    data = {
        testahels_pending_requests:     data_values(pending, value.count),
        testahels_testahel_beneficiary: data_values(beneficiary, tre)
    }
    render_data(data)
  end

  # ----------------------------------------------------------------------------------------------- #
  # GET : /v1/dashboard/number_of
  def number_of
    trips    = Trip.ransack(search_params).result.count
    offers   = collection_offers.ransack(search_params).result.count
    chalets  = Chalet.ransack(search_params).result.count
    mbadrats = Mbadrat.ransack(search_params).result.count

    data = {
      number_of: {
        trips:    trips,
        offers:   offers,
        chalets:  chalets,
        mbadrats: mbadrats
      }
    }
    render_data(data)
  end

  private

  def search_params
    {
      created_at_lteq:              params[:end_date].present?   ? Date.parse(params[:end_date])&.end_of_day         : nil,
      created_at_gteq:              params[:start_date].present? ? Date.parse(params[:start_date])&.beginning_of_day : nil,
    }
  end

  def percentage_rounder(percent)
    percent.round(2)
  end

  # Total Count of Expected Employees
  def tee
    Employee.all.sum(:expected_employees).to_f || 1
  end

  # Total Count of Registred Employees
  def tre
    Employee.count.to_f
  end

  def data_values(value, divided)
    {
      count:        value,
      percentage:   percentage_rounder((value/ divided.to_f) *100),
    }
  end

  def collection_offers
    Offer.all
  end

end
