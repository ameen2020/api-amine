class V1::OfferFeedbacksController < V1::BaseController
  power :offer_feedbacks, context: :offer_id, map: {
    [:index]   => :offer_feedbacks_index,
    [:show]    => :offer_feedbacks_show,
    [:create]  => :creatable_offer_feedback
  }, as: :offer_feedbacks_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/offers/:offer_id/feedbacks
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/offers/:offer_id/feedbacks
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/offers/:offer_id/feedbacks/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/offers/:offer_id/feedbacks/:id
  # Inherited from V1::BaseController
  # def update; end

  # DELETE : /v1/offers/:offer_id/feedbacks/:id
  # Inherited from V1::BaseController
  # def update; end

  private

  ## ------------------------------------------------------------ ##

  def offer_feedback_params
    params.permit(:comment, :rating, :category_id)
  end

  def params_processed
      offer_feedback_params.merge({
        offer_id:    offer_id,
        employee_id: @current_user.id
      }).permit!
  end

  def offer_id
    params[:offer_id] || ""
  end

  def get_order
    { sort_column => sort_direction(:desc) }
  end
end
