class V1::OccasionsController <  V1::BaseController
  power :occasions, map: {
    [:index]   => :occasion_index,
    [:show]    => :occasion_show,
    [:create]  => :creatable_occasion,
    [:update]  => :updatable_occasion,
    [:destroy] => :destroyable_occasion
  }, as: :occasions_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/ocasions/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/ocasions/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/ocasions/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/ocasions/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  private

  def occasion_params
    params.permit(title_translations: I18n.available_locales,
                  description_translations: I18n.available_locales )
  end

  def search_params
    search = {}
    search[:id_eq]             = params[:id]
    search[:translations_cont] = params[:search]
    search
  end
end
