class V1::NotificationsHistoriesController < V1::BaseController
  power :notifications_histories, map: {
    [:index]   => :notifications_index,
    [:show]   => :notifications_show,
    [:unread_notiifications] => :unread_notiifications
  }, as: :notifications_histories_scope


  def employee_id
    @current_user.id || ""
  end

  def show
    get_resource.seen!
    render_data(get_resource.as_api_response(:v1_index))
  end

  def unread_notiifications
    render_data(count: notifications_histories_scope.unseen.count)
  end
  private

  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
