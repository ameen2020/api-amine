class V1::TripsController <  V1::BaseController

  before_action :set_resource, only: %i[show update destroy set_cover add_image remove_image export_registerations]

  include PicturesActions
  include CountableViews

  power :trips, map: {
    [:index]   => :trip_index,
    [:calendar] =>:trips_calendar,
    [:show]    => :trip_show,
    [:create]  => :creatable_trip,
    [:destroy] => :destroyable_trip,
    [:update, :set_cover, :add_image, :remove_image]  => :updatable_trip,
    [:export_registerations] => :export_trip_registerations
  }, as: :trips_scope


  ## ------------------------------------------------------------ ##
  # GET : /v1/trips/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/trips/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/trips/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/trips/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/trips/:id/export_registerations
  def export_registerations
    data = Exporter::Export.new(collection: get_resource.trip_registrations.includes(:employee), encoding: 'UTF-8').export
    send_data(data, filename: "trip-#{params[:id]}-registerations.xls", type: 'application/xls', disposition: 'inline')
  end

  ## ------------------------------------------------------------ ##

  # GET : /v1/trips/calendar
  def calendar
    return if missing_params!([:year, :month])
    dates = Trip.get_calender(params[:year], params[:month])
    render_data(
      current_date: Date.today.strftime('%Y-%m-%d'),
      dates:        dates.map{ |t| t.strftime('%Y-%m-%d') }.sort
    )
  end

  private

  def trip_params
    attrs = params.permit( :start_date, :end_date, :last_register_date,
                   :last_cancel_date, :capacity,
                   title_translations: I18n.available_locales,
                   description_translations: I18n.available_locales,
                   location_translations: I18n.available_locales)
    attrs.merge(
      current_user: @current_user
    )
  end

  def params_processed
    pictures_params_processed.permit!
  end

  def date_param
    params[:date] || Date.today.to_s
  end

  # search for trips by date
  def search_params
    search = {}
    search[:start_date_eq] = date_param if @current_user.employee? || params[:date].present?

    if @access == 'admin'
      search[:id_eq]             = params[:id]

      if params[:status].present?
        search[:start_date_lt]   = Date.today if params[:status].to_i == 0
        search[:start_date_gteq] = Date.today if params[:status].to_i == 1
      end

      search[:translations_cont] = params[:search]
    end

    search
  end

  # Inject render template with current user
  def template_injector
    {
      current_user: @current_user
    }
  end

  def get_order
    return "title_translations->>'#{I18n.locale}' #{sort_direction}" if params[:sort] == 'title'
    { sort_column(:start_date) => sort_direction(:asc) }
  end

  # Override the main method in BaseController
  def index_message(collection)
    I18n.t(collection.to_a.size.zero? ? 'trips.no_trips' : 'data_found')
  end
end
