class V1::EmployeeVoicesController < V1::BaseController
  power :employee_voices, map: {
    [:index]   => :employee_voice_index,
    [:show]    => :employee_voice_show,
    [:create]  => :creatable_employee_voice,
    [:update]  => :updatable_employee_voice,
    [:destroy] => :destroyable_employee_voice
  }, as: :employee_voices_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/employee_voices/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/employee_voices/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/employee_voices/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/employee_voices/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/employee_voices/:id
  # Inherited from V1::BaseController
  # def destroy; end

  ## ------------------------------------------------------------ ##

  private
  # Whitelist parameters
  def employee_voice_params
    params.permit(:phone_number, :voice_category, :attachment, :message)
  end

  def reply_params
    params.permit(:reply_message, attachment_file: [])
  end

  def reply_params_processed
    results = reply_params
    results[:attachment_admin] = process_base64(params[:attachment_file]) if params[:attachment_file].present?
    results.delete(:attachment_file)

    results.permit!
  end


  def params_processed
    if params[:action] == 'create'
      employee_voice_params.merge(employee_id: @current_user.id).permit!
    elsif params[:action] == 'update' && @access == 'admin'
      reply_params_processed.merge(admin_id: @current_user.id).permit!
    end
  end

  # Search filters
  def search_params
    search = {
       employee_first_name_or_last_name_cont:   params[:search],
       id_eq:              params[:id],
       voice_category_eq:  params[:type]
     }
    if params[:status].present?
      search[:reply_message_null]     = params[:status].to_s == '0'
      search[:reply_message_not_null] = params[:status].to_s == '1'
    end
    search
  end

  # Custom ordering and sorting
  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
