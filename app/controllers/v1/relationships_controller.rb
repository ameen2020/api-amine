class V1::RelationshipsController < V1::BaseController
  power :relationships, map: {
    [:index]   => :relationship_index,
    [:show]    => :relationship_show,
    [:create]  => :creatable_relationship,
    [:update]  => :updatable_relationship,
    [:destroy] => :destroyable_relationship
  }, as: :relationships_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/relationships/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/relationships/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/relationships/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/relationships/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  private

  def relationship_params
    params.permit(title_translations: I18n.available_locales)
  end
end
