class V1::ContactUsInfosController < V1::BaseController
  power :contact_us_infos, map: {
    [:index]   => :contact_us_infos_index,
    [:show]    => :contact_us_infos_show,
    [:create]  => :creatable_contact_us_infos,
    [:update]  => :updatable_contact_us_infos,
    [:destroy] => :destroyable_contact_us_infos
  }, as: :contact_us_infos_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/contact_us_infos
  def index
    @access == 'admin' ? super : render_data(Hash[ContactUsInfo.all.pluck(:option, :value)])
  end

  ## ------------------------------------------------------------ ##

  # POST : /v1/contact_us_infos
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/contact_us_infos/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/contact_us_infos/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/contact_us_infos/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  private

  def contact_us_info_params
    params.permit(:option, :value)
  end
end
