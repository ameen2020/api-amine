class V1::OfferAttachmentsController < V1::BaseController
  power :offer_attachments, context: :offer_id, map: {
    [:index]   => :offer_attachments_index,
    [:create]  => :creatable_offer_attachment,
    [:destroy] => :destroyable_offer_attachment
  }, as: :offer_attachments_scope

  ## ------------------------------------------------------------ ##
  # GET : /v1/offers/:offer_id/attachments
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/offers/:offer_id/attachments
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/offers/:offer_id/attachments/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  private

  def offer_id
    params[:offer_id] || ""
  end

  def offer_attachment_params
    params.permit(attachment_file: [])
  end

  def params_processed
    results = resource_params
    results[:offer_id] = offer_id

    resource_params[:attachment] = process_base64(params[:attachment_file])
    resource_params.delete(:attachment_file)

    results.permit!
  end

  def get_order
    { sort_column => sort_direction(:desc) }
  end
end
