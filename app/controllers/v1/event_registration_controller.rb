class V1::EventRegistrationController < V1::BaseController

  include RegisteredUsers

  power :employees, context: :event_id, map: {
    [:index]   => :event_registration_index,
    [:create]  => :creatable_event_registration,
    [:destroy] => :destroyable_event_registration,
    [:send_email] => :send_emailable_event_registration
  }, as: :event_registrations_scope

  skip_before_action :set_resource

  ## ------------------------------------------------------------ ##
  # GET : /v1/events/:id/registration
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/events/:id/registration
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/events/:id/registration
  def destroy
    return if @access == 'admin' && missing_params!([:employee_id])

    event_registrations = get_scope.find_by!(employee_id: employee_id)
    if event_registrations.destroy
      data = { resource_node => { remaining: event_registrations.event.remaining } }
      render_success(action_message[:destroy] || I18n.t(:x_deleted_successfully, name: resource_name.titleize), data: data)
    else
      render_bad_request(error: event_registrations)
    end
  end

  ## ------------------------------------------------------------ ##

  private

  def search_params
    {
      g: [
        {
          employee_first_name_cont:   params[:search],
          employee_last_name_cont:    params[:search],
          m: 'or'
        }
      ]
    }
  end

  def id_parameter
    params[:event_id]
  end

  def event_id
    id_parameter || ''
  end

  def employee_id
    @access == 'admin' ? params[:employee_id] : @current_user.id
  end

  def params_processed
    {
      event_id:    id_parameter,
      employee_id: @current_user.id
    }
  end

  def action_message
    {
      create:  I18n.t('registrations.register_success'),
      destroy: I18n.t('registrations.cancel_success')
    }
  end

  def resource_node
    params[:action] != 'index' ? 'event' : super
  end
end
