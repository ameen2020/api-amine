# frozen_string_literal: true

class V1::ContactUsController < V1::BaseController
  power :contact_us, map: {
    [:index]   => :contact_us_index,
    [:show]    => :contact_us_show,
    [:create]  => :creatable_contact_us,
    [:update]  => :updatable_contact_us,
    [:export]  => :exportable_contact_us
    # [:destroy] => :destroyable_contact_us
  }, as: :contact_us_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/contact_us/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/contact_us/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/contact_us/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/contact_us/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/contact_us/export
  def export
    @limit = 999_999_999
    data = Exporter::Export.new(collection: get_collection, encoding: 'UTF-8').export
    send_data(data, filename: 'contact_us.xls', type: 'application/xls', disposition: 'inline')
  end

  ## ------------------------------------------------------------ ##

  private

  # Used on Submitting new Contact Us Message
  def contact_us_params
    params.permit(:message, :phone_number)
  end

  # Used on Replying
  def reply_contact_us_params
    params.permit(:reply_message)
  end

  def params_processed
    if params[:action] == 'create'
      contact_us_params.merge(employee_id: @current_user.id).permit!
    elsif params[:action] == 'update' && @access == 'admin'
      reply_contact_us_params.merge(admin_id: @current_user.id).permit!
    end
  end

  def action_message
    { create: I18n.t('contact_us.sent_successfully') }
  end

  def search_params
    search = {
      g: [
        {
          id_eq:           params[:id],
          created_at_gteq: params[:from].present? ? Date.parse(params[:from])&.beginning_of_day : nil,
          created_at_lteq: params[:to].present?   ? Date.parse(params[:to])&.end_of_day         : nil,
        },
        {
          message_cont:               params[:search],
          employee_id_eq:             params[:search],
          employee_first_name_cont:   params[:search],
          employee_last_name_cont:    params[:search],
          employee_email_cont:        params[:search],
          employee_phone_number_cont: params[:search],
          m: 'or'
        }
      ]
    }

    if params[:status].present?
      search[:g][0][:reply_message_null]     = params[:status].to_s == '0'
      search[:g][0][:reply_message_not_null] = params[:status].to_s == '1'
    end

    search
  end

  def get_order
    "#{sort_column(:created_at)} #{sort_direction(:desc)}"
  end

  def sort_column(column)
    if params[:sort] == 'employee'
      'employees.first_name'
    else
      super(column)
    end
  end
end
