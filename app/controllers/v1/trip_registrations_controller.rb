class V1::TripRegistrationsController < V1::BaseController

  include RegisteredUsers

  power :trip_registrations, context: :trip_id, map: {
    [:index]   => :trip_registration_index,
    [:create]  => :creatable_trip_registration,
    [:destroy] => :destroyable_trip_registration,
    [:approve] => :updataable_trip_registration,
    [:send_email] => :send_emailable_trip_registration
  }, as: :trip_registrations_scope

  skip_before_action :set_resource
  before_action :set_resource, only: [:approve]

  ## ------------------------------------------------------------ ##

  # DELETE: v1/trips/:id/registration
  def destroy
    return if missing_params!([:employee_id]) if @access == 'admin'
    trip_registrations = get_scope.find_by!(employee_id: employee_id)
    if trip_registrations.destroy
      data = {trip: { remaining: trip_registrations.trip.remaining } }
      render_success(action_message[:destroy] || I18n.t(:x_deleted_successfully, name: resource_name.titleize), data: data)
    else
      render_bad_request(error: trip_registrations)
    end
  end

  ## ------------------------------------------------------------ ##

  # POST: v1/trips/:id/registration/:id/approve
  def approve
    if get_resource.update(approval_params)
      message = 'Registration approval updated'
      render_data({ resource_node.singularize.to_sym => get_resource.as_api_response(show_template, template_injector) }, message: message)
    else
      render_unprocessable_entity(error: get_resource)
    end
  end

  ## ------------------------------------------------------------ ##

  private

  def employee_id
    @access == 'admin' ? params[:employee_id] : @current_user.id
  end

  def params_processed
    trip_registration_params.merge({
      trip_id:     trip_id,
      employee_id: @current_user.id
    }).permit!
  end

  def trip_registration_params
    params.permit(:full_name, :relationship_id, :id_number, :companion,
                  attachments_attributes: [:attachment])
  end

  def trip_id
    params[:trip_id] || ""
  end

  def action_message
    {
      create:  I18n.t('registrations.register_success'),
      destroy: I18n.t('registrations.cancel_success')
    }
  end

  def approval_params
    if @current_user.admin? || @current_user.admin2?
      return {
        super_admin_approve: params[:status],
        super_admin_comment: params[:comment],
      }
    elsif @current_user.sub_admin?
      return {
        sub_admin_approve: params[:status],
        sub_admin_comment: params[:comment]
      }
    else
      return {}
    end
  end

  def search_params
    {
      g: [
        {
          companion_eq:           params[:companion],
          sub_admin_approve_eq:   params[:sub_admin],
          super_admin_approve_eq: params[:super_admin],
          register_code_eq:       params[:register_code],
        },
        {
          employee_id_eq:           params[:employee],
          employee_first_name_cont: params[:employee],
          employee_last_name_cont:  params[:employee],
          m: 'or'
        }
      ]
    }
  end

  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end

  def template_injector
    {
      blocked_employees: TripBlackList.pluck(:employee_id)
    }
  end
end
