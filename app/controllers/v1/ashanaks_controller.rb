class V1::AshanaksController < V1::BaseController
  power :ashanaks, map: {
    [:index]   => :ashanaks_index,
    [:show]    => :ashanak_show,
    [:create]  => :creatable_ashanak,
    [:update]  => :updatable_ashanak,
    [:destroy] => :destroyable_ashanak,
    [:approve] => :updataable_approval,
    [:export]  => :exportable_ashanaks,
    [:send_e_card, :generat_e_card, :sticker] => :buildable_ashanak,
    [:email_export]  => :requestable_ashanaks
  }, as: :ashanaks_scope

  before_action :set_resource, only: [:show, :update, :destroy, :approve, :send_e_card, :generat_e_card, :sticker]

  ## ------------------------------------------------------------ ##
  # GET : /v1/ashanaks
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/ashanaks
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/ashanaks/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/ashanaks/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/ashanaks/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # POST: v1/ashanaks/:id/approve
  def approve
    if get_resource.update(approval_params)
      message = 'Request approval updated'
      render_data({ resource_node.singularize.to_sym => get_resource.as_api_response(show_template, template_injector) }, message: message)
    else
      render_unprocessable_entity(error: get_resource)
    end
  end

  ## ------------------------------------------------------------ ##

  # GET : /v1/ashanaks/export
  def export
    @limit = 999_999_999
    data = Exporter::Export.new(collection: get_collection, encoding: 'UTF-8').export
    send_data(data, filename: 'testahil.xls', type: 'application/xls', disposition: 'inline')
  end

  ## ------------------------------------------------------------ ##

  # POST : /v1/ashanaks/email_export
  def email_export
    return if missing_params!([:ids, :message, :emails])

    return render_not_found unless get_scope.where(id: params[:ids]).any?

    params[:emails].each do |email|
      AshanakMailer.export_ashanaks(email, params[:ids], params[:message]).deliver_later
    end

    render_success(I18n.t('ashanaks.export_email_success'))
  end

  ## ------------------------------------------------------------ ##
  # GET : /v1/ashanaks/id/send_e_card
  def send_e_card
    occasion_name = get_resource.occasion.title_en.snakecase
    EmployeeMailer.send_e_card(get_resource.employee_id, occasion_name).deliver_later
    render_success(I18n.t('ashanaks.send_e_card_success'))
  end

  ## ------------------------------------------------------------ ##
  # GET : /v1/ashanaks/id/generat_e_card
  def generat_e_card
    pdf = Pdf::Build.new(data: get_resource, type: :e_card).build
    send_data(pdf, filename: 'ecard_testahil.pdf', type: 'application/pdf', disposition: 'inline')
  end

  ## ------------------------------------------------------------ ##

  # GET : /v1/ashanaks/id/sticker
  def sticker
    pdf = Pdf::Build.new(data: get_resource, type: :sticker).build
    send_data(pdf, filename: 'sticker_testahil.pdf', type: 'application/pdf', disposition: 'inline')
  end


  private

  def action_message
    {
      create:  I18n.t('ashanaks.created'),
    }
  end

  def ashanak_params
    params.permit(:region_id, :occasion_id, :work_location_id, :date, :phone_number, :address,
                  attachments_attributes: %i[attachment])
  end

  def params_processed
    ashanak_params.merge({
      employee_id: @current_user.id
    }).permit!
  end

  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end

  def search_params
    created_at = params[:created_at].present? ? Date.parse(params[:created_at]) : nil
    {
      g: [
        {
          id_in:                  params[:id],
          date_eq:                params[:date],
          region_id_eq:           params[:region],
          occasion_id_eq:         params[:occasion],
          work_location_id_eq:    params[:work_location],
          sub_admin_approve_eq:   params[:sub_admin],
          super_admin_approve_eq: params[:super_admin],
          created_at_gteq:        created_at&.beginning_of_day,
          created_at_lt:          created_at&.end_of_day,
        },
        {
          employee_id_eq:           params[:employee],
          employee_first_name_cont: params[:employee],
          employee_last_name_cont:  params[:employee],
          m: 'or'
        }
      ]
    }
  end

  def approval_params
    if @current_user.admin? || @current_user.admin2?
      return {
        super_admin_approve:    params[:status],
        super_admin_comment:    params[:comment],
        attachments_attributes: attachments_attributes
      }
    elsif @current_user.sub_admin?
      return {
        admin_id:               @current_user.id,
        sub_admin_approve:      params[:status],
        sub_admin_comment:      params[:comment],
        attachments_attributes: attachments_attributes
      }
    else
      return {}
    end
  end

  def attachments_attributes
    return [] unless params[:attachments].present?

    results = []
    params[:attachments].each do |file|
      results << { by_admin: true, attachment: process_base64(file) }
    end

    results
  end
end
