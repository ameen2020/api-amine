class V1::ApiController < ApplicationController
  include Consul::Controller

  before_action :set_locale
  before_action :set_pagination, only: [:index]
  before_action :authenticate_request!, except: [:export, :export_registerations, :generat_e_card, :sticker]
  before_action :authenticate_by_params!, only: [:export, :export_registerations, :generat_e_card, :sticker]

  require_power_check

  # Authenticate the request by headers
  def authenticate_request!
    return if missing_headers!(['Authorization'])
    @token ||= AuthenticateRequest.get(Employee, request.headers['Authorization'].split(' ').last)
    set_curret_user_with_access
  end

  # Authenticate the request by params
  def authenticate_by_params!
    return if missing_params!(['token'])
    @token ||= AuthenticateRequest.get(Employee, params[:token])
    set_curret_user_with_access
  end

  private

  # Set Power and inject it with current user
  current_power do
    Power.new(current_user, @access)
  end

  # Set request locale
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  # Set Pagination, by definging
  def set_pagination
    @page  = (params[:page] || 1).to_i
    @limit = params[:limit].to_s == '-1' ? 999_999 : (params[:limit] || 10).to_i
    render_smart_error(1003) && return if @page < 1 || @limit < 1
  end

  # Set Current User and Access type or reject the request if user is forbidden
  def set_curret_user_with_access
    @current_user = @token[:user]
    @access       = @token[:access]

    # Use 1201 so mobile will automatically redirect diactivated users to login screen
    return render_forbidden(error: 1201, message: I18n.t('errors.1305')) if @current_user.inactive? # reject inactive/disabled users
  end
end
