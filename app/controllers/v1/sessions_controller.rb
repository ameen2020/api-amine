class V1::SessionsController < V1::BaseController
  power :sessions
  skip_before_action :authenticate_request!

  ## -------------------------------------------------- ##
  # POST : v1/sessions
  def create
    return if missing_params!([:username, :password])

    @employee = UserRegistration.authenticate(params[:username], params[:password])

    return render_bad_request(error: 1301)       unless @employee.present?
    return render_bad_request(error: @employee)  if @employee.errors.any?
    return render_bad_request(error: 1311)       if params[:access] == 'admin' && @employee.employee?
    return render_bad_request(error: 1305)       if @employee.inactive?

    data = { employee: @employee.as_api_response(:v1_profile) }

    render_data(data, meta: generate_token(@employee), message: I18n.t('sessions.login_successfully'))
  rescue Net::HTTPError => e
    Rails.logger.error { {message: e.message, backtrace: e.backtrace.join("\n\r")} }
    render_bad_request(error: 1312)
  end

  ## -------------------------------------------------- ##

  private

  # Session Meta Payload (token + role)
  def generate_token(employee)
    payload = employee.login_payload
    payload.merge!(access: params[:access] || 'app')
    {
      role: employee.role,
      token: JsonWebToken.encode(payload, token_expire)
    }
  end

  # token expir timing
  def token_expire
    params[:access] == 'admin' ?  10.hours.from_now : 720.hours.from_now
  end
end
