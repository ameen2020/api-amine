class V1::InstallmentsController < V1::BaseController
  power :installments, map: {
    [:index]   => :installment_index,
    [:show]    => :installment_show,
    [:create]  => :creatable_installment,
    [:update]  => :updatable_installment,
    [:destroy] => :destroyable_installment,
    [:approve] => :updatable_installment_approval
    # [:export]  => :exportable_installments
  }, as: :installments_scope

  before_action :set_resource, only: [:show, :update, :destroy, :approve]

  ## ------------------------------------------------------------ ##
  # GET : /v1/installments
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/installments
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/installments/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/installments/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/installments/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # POST: v1/installments/:id/approve
  def approve
    if get_resource.update(approval_params)
      message = 'Request approval updated'
      render_data({ resource_node.singularize.to_sym => get_resource.as_api_response(show_template, template_injector) }, message: message)
    else
      render_unprocessable_entity(error: get_resource)
    end
  end

  ## ------------------------------------------------------------ ##

  # GET : /v1/installments/export
  # def export
  #   @limit = 999_999_999
  #   data = Exporter::Export.new(collection: get_collection, encoding: 'UTF-8').export
  #   send_data(data, filename: 'testahil.xls', type: 'application/xls', disposition: 'inline')
  # end

  ## ------------------------------------------------------------ ##

  private

  # def action_message
  #   {
  #     create:  I18n.t('installments.created'),
  #   }
  # end

  def installment_params
    params.permit(:nationality_id, :company_name,
                  attachments_attributes: %i[attachment])
  end

  def params_processed
    installment_params.merge({
      employee_id: @current_user.id
    }).permit!
  end

  def get_order
    return sort_column(:company_name) => sort_direction(:asc) if params[:sort] == 'company_name'
    { sort_column(:created_at) => sort_direction(:desc) }
  end

  def search_params
    created_at = params[:created_at].present? ? Date.parse(params[:created_at]) : nil
    {
      g: [
        {
          id_eq:                  params[:id],
          nationality_id_eq:      params[:nationality_id],
          company_name_cont:      params[:company_name],
          sub_admin_approve_eq:   params[:sub_admin],
          super_admin_approve_eq: params[:super_admin],
          created_at_gteq:        created_at&.beginning_of_day,
          created_at_lt:          created_at&.end_of_day,
        }
      ]
    }
  end

  def approval_params
    if @current_user.admin? || @current_user.admin?
      return {
        super_admin_approve:    params[:status],
        super_admin_comment:    params[:comment]
      }
    elsif @current_user.sub_admin?
      return {
        admin_id:               @current_user.id,
        sub_admin_approve:      params[:status],
        sub_admin_comment:      params[:comment]
      }
    else
      return {}
    end
  end

  def attachments_attributes
    return [] unless params[:attachments].present?

    results = []
    params[:attachments].each do |file|
      results << { by_admin: true, attachment: process_base64(file) }
    end

    results
  end
end
