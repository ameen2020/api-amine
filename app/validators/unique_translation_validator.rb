# frozen_string_literal: true
class UniqueTranslationValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors.add(attribute, :already_taken) if record.class.
      where("#{attribute}->>'en' = ? OR #{attribute}->>'ar' = ?", value["en"], value["ar"]).
      where.not(id: record.id).exists?
  end
end
