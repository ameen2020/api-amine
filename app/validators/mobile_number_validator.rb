# frozen_string_literal: true
class MobileNumberValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value&.match?(/\+[1-9]{1}[0-9]{3,14}/)
      record.errors[attribute] << (options[:message] || I18n.t('activerecord.errors.messages.invalid_mobile_number'))
    end
  end
end
