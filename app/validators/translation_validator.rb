class TranslationValidator < ActiveModel::EachValidator
  def initialize(options = {})
    super
    options[:allow_nil]     = false unless options[:allow_nil].present?
    options[:required_keys] = [:en] unless options[:required_keys].present?
  end

  def validate_each(record, attribute, value)
    # Check if the value is a valid Hash
    return record.errors.add(attribute, (options[:message] || :invalid_json)) unless value.is_a?(Hash)

    # Skip the check if the allow_nil is true
    return if options[:allow_nil]

    # Check the the required_keys in the value
    options[:required_keys].each do |locale|
      unless value[locale.to_sym].present? || value[locale.to_s].present?
        return record.errors.add(attribute, (options[:message] || "required_keys_#{locale}".to_sym))
      end
    end if options[:required_keys].is_a?(Array)
  end
end
