class UsernameValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A[a-zA-Z0-9\-\_\.]+\Z/
      record.errors[attribute] << (options[:message] || I18n.t('activerecord.errors.messages.invalid_username'))
    end
  end
end
