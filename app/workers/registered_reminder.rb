class RegisteredReminder
  @queue = :reminders

  def self.perform(x_days = 3)
    start_time = (x_days.to_i).days.from_now.beginning_of_day
    end_time   = (x_days.to_i).days.from_now.end_of_day

    events = Event.where('start_date between ? and ?', start_time, end_time)
    events.each do |event|
      event.event_registrations.each do |er|
        er.send_reminder_email(x_days)
        er.push_reminder_notification(x_days)
      end
    end

    trips = Trip.where( 'start_date between ? and ?', start_time, end_time)
    trips.each do |trip|
      trip.trip_registrations.each do |tr|
        tr.send_reminder_email(x_days)
        tr.push_reminder_notification(x_days)
      end
    end
  end
end
