class AdminsOffersReminder
  @queue = :reminders

  def self.perform(x_days = 14)
    start_time = x_days.to_i.days.from_now.beginning_of_day
    end_time   = x_days.to_i.days.from_now.end_of_day

    offers = Offer.where(expire_on: start_time..end_time)
    admins = Employee.where(role: [0, 3])

    offers.each do |offer|
      admins.each do |admin|
        offer.send_reminder_email(admin.id)
        offer.push_reminder_notification(admin.id)
      end
    end
  end
end
