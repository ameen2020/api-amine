require 'resque-history'
class PushNotificationWorker
  extend Resque::Plugins::History
  @queue = :push_notification

  # Will be accessed by Resque
  def self.perform(*args)
    new(*args).perform
  end

  def initialize(category_model, record_id, employee_ids = nil)
    @record_id      = record_id
    @category_model = category_model
    @employee_ids   = employee_ids
  end

  # The magic happen here
  def perform
    find_record

    ids = @employee_ids || employees.pluck(:employee_id)

    ids.each do |emp_id|
      @record.notifications_histories.create!(employee_id: emp_id.to_i)
    end
  end

  # find the record
  def find_record
    @record ||= @category_model.classify.constantize.find(@record_id)
  end

  # get the notification setting name
  def notifications_setting
    case @category_model
    when 'new'        then 'news'
    when 'assignment' then 'job'
    when 'magazin'    then 'magazine'
    else
      @category_model
    end
  end

  # Get the subscribed employees
  def employees
    if notifications_setting == "offer"
      reg = @record.regions.pluck(:region_id)
      query = reg.map {|i| "#{i} = ANY(region)"}.join(" OR ")
      NotificationsSetting.where(notifications_setting.to_sym => true)
                          .where(query)
                          .where("'#{@record.sub_category_id}' = ANY(sub_category)")
    elsif notifications_setting == "chalet"
      reg = @record.regions.pluck(:region_id)
      query = reg.map {|i| "#{i} = ANY(region)"}.join(" OR ")
      NotificationsSetting.where(notifications_setting.to_sym => true)
                          .where(query)
    else
      NotificationsSetting.where(notifications_setting.to_sym => true)
    end
  end
end
