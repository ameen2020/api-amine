require 'resque-history'
class Publisher
  extend Resque::Plugins::History
  @queue = :push_notification

  # Will be accessed by Resque
  def self.perform(*args)
    new(*args).perform
  end

  def initialize(employee_id, messages, push_type, record_id, notification_id, locale = :en)
    @employee_id     = employee_id
    @messages        = messages
    @push_type       = push_type
    @record_id       = record_id
    @notification_id = notification_id
    @locale          = locale.to_s
  end

  def perform
    $pubnub.publish(
      channel: channel_name,
      message: push_package,
    ).call

    log_the_package
  end

  def channel_name
    "employee_#{@employee_id}_#{@locale}"
  end

  # Parse the notification package
  def push_package
    {
      aps: {
        alert: {
          "loc-key": "REQUEST_FORMAT",
          "loc-args": @messages
        },
        sound:           "default",
        id:              @record_id,
        notification_id: @notification_id,
        push_type:       @push_type
      },
      pn_gcm: {
        data: {
          message: @messages
        }
      }
    }
  end

  def log_the_package
    log = " ========================================== \n" +
          "\e[1mPush Notification to channel\e[22m : \e[31m#{channel_name}\e[0m\n" +
          push_package.to_json.to_s +
          "\n ========================================== \n"
    Rails.logger.debug(log)
    puts log
  end
end
