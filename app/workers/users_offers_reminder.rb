# frozen_string_literal: true

class UsersOffersReminder
  @queue = :reminders

  def self.perform(x_days = 7)
    start_time = x_days.to_i.days.from_now.beginning_of_day
    end_time   = x_days.to_i.days.from_now.end_of_day

    offers      = Offer.includes(:regions)
                       .where(expire_on: start_time..end_time)
    emp_per_off = NotificationsSetting.where(offer: true)
                                      .joins(:employee)
                                      .where(employees: { role: 1 })

    emp_per_off.each do |notification|
      offers.each do |offer|
        if notification.sub_category.include?(offer.sub_category_id) || !(notification.region & offer.region_ids).empty?
          offer.send_reminder_email(notification.employee_id)
          offer.push_reminder_notification(notification.employee_id)
        end
      end
    end
  end
end
