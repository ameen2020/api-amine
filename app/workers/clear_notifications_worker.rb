require 'resque-history'

class ClearNotificationsWorker
  extend Resque::Plugins::History
  @queue = :helpers

  def self.perform(x_days = 7)
    NotificationsHistory.where('created_at <= ?', (x_days.to_i).days.ago).destroy_all
  end
end
