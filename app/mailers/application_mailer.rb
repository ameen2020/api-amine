class ApplicationMailer < ActionMailer::Base
  default from: 'Ashanak <ashanak@moh.gov.sa>'
  layout 'mailer'
  before_action :add_inline_attachments!

  def mail(to:, **options)
    to = to.gsub('@moh.gov.sa', '@mailinator.com') unless Rails.env.production?
    super(to: to, **options)
  end

  private

  def add_inline_attachments!
    attachments['logo.png'] = File.read("#{Rails.root}/public/logo.png")
  end

  def en_locale
    I18n.locale = :en
  end
end
