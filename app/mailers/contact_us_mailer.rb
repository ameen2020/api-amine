class ContactUsMailer < ApplicationMailer
  before_action :en_locale

  def contact(id)
    @contact_us = ContactUs.find(id)
    @employee   = @contact_us.employee

    @intro   = I18n.t('emails.dear_user', user: 'Admin')
    @title   = I18n.t('emails.contact_us.contact.title', employee: @contact_us.employee.full_name)
    @message = I18n.t('emails.contact_us.contact.message', employee: @contact_us.employee.full_name).html_safe

    mail(to: Rails.application.secrets.contact_email, subject: @title)
  end

  def reply(id)
    @contact_us = ContactUs.find(id)
    I18n.locale = @contact_us.locale.to_sym
    @employee   = @contact_us.employee

    @intro   = I18n.t('emails.dear_user', user: @employee.full_name)
    @title   = I18n.t('emails.contact_us.reply.title')
    @message = I18n.t('emails.contact_us.reply.message')

    mail(to: @employee.email, subject: @title)
  end
end
