class EmployeeVoiceMailer < ApplicationMailer
  before_action :en_locale

  def reply(id, attachment_path )
    attachments['reply_message'] = File.read(attachment_path) if attachment_path
    @employee_voice = EmployeeVoice.find(id)
    I18n.locale = @employee_voice.locale.to_sym
    @employee   = @employee_voice.employee

    @intro   = I18n.t('emails.dear_user', user: @employee.full_name)
    @title   = I18n.t('emails.employee_voice.reply.title')
    @message = I18n.t('emails.employee_voice.reply.message')

    mail(to: @employee.email, subject: @title)
  end
end
