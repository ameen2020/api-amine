class TripRegistrationMailer < ApplicationMailer
  before_action :en_locale, only: [:cancel_by_user, :inform_super_admin, :new_registration]

  def send_approval_email(id)
    @registration = TripRegistration.find(id)
    I18n.locale   = @registration.locale.to_sym
    @status       = @registration.rejected? ? I18n.t(:rejection) : I18n.t(:approval)

    @title   = I18n.t('emails.trips.approval.title', title: @registration.trip.title, status: @status)
    @intro   = I18n.t('emails.dear_user', user: @registration.employee.full_name)
    @message = I18n.t('emails.trips.approval.message', title: @registration.trip.title, status: @status)
    @message = @message + "<br><br>" + I18n.t('emails.reason') + " " +  @registration.comments if @registration.rejected?
    @message = @message.html_safe

    mail(to: @registration.employee.email, subject: @title)
  end

  ## ------------------------------------------------------------ ##

  def reminder(id, days)
    @registration = TripRegistration.find(id)
    I18n.locale   = @registration.locale.to_sym
    @employee     = @registration.employee

    @title   = I18n.t('emails.trips.reminder.title', title: @registration.trip.title)
    @intro   = I18n.t('emails.dear_user', user: @employee.full_name)
    @message = I18n.t('emails.trips.reminder.message', title: @registration.trip.title, count: days).html_safe

    mail(to: @employee.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end

  ## ------------------------------------------------------------ ##

  def new_registration(id, admin_id)
    @registration = TripRegistration.find(id)
    @admin        = Employee.find(admin_id)

    @title   = I18n.t('emails.trips.new_registration.title', title: @registration.trip.title)
    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @message = I18n.t('emails.trips.new_registration.message', employee: @registration.employee.full_name, title: @registration.trip.title)

    mail(to: @admin.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end

  ## ------------------------------------------------------------ ##

  def inform_super_admin(id, admin_id)
    @registration = TripRegistration.find(id)
    @admin        = Employee.find(admin_id)

    @title   = I18n.t('emails.trips.inform_super_admin.title', title: @registration.trip.title)
    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @message = I18n.t('emails.trips.inform_super_admin.message', employee: @registration.employee.full_name, title: @registration.trip.title)

    mail(to: @admin.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end

  ## ------------------------------------------------------------ ##

  def inform_user_of_delete(employee_id, trip_title, locale = :en)
    I18n.locale = locale.to_sym
    @employee   = Employee.find(employee_id)

    @title   = I18n.t('emails.trips.inform_user_of_delete.title', title: trip_title)
    @intro   = I18n.t('emails.dear_user', user: @employee.full_name)
    @message = I18n.t('emails.trips.inform_user_of_delete.message', title: trip_title)

    mail(to: @employee.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end

  ## ------------------------------------------------------------ ##

  def cancel_by_user(admin_id, employee_id, trip_id, registration)
    @admin        = Employee.find(admin_id)
    @employee     = Employee.find(employee_id)
    @trip         = Trip.find(trip_id)
    @registration = registration
    @relationship = @registration['relationship_id'] ? Relationship.find(@registration['relationship_id'])&.title : '-'

    @title   = I18n.t('emails.trips.cancel_by_user.title', title: @trip.title, employee: @employee.full_name)
    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @message = I18n.t('emails.trips.cancel_by_user.message', title: @trip.title, employee: @employee.full_name)

    mail(to: @admin.email, subject: "Trip Canceled by #{@employee.full_name}")
  end
end
