class TestMailer < ApplicationMailer
  default from: 'ashanak@moh.gov.sa'
  layout false
  skip_before_action :add_inline_attachments!

  def test(email = 'm.sheiba@clickapps.co')
    @greeting = "Hi"
    mail to: email
  end
end
