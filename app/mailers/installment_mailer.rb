class InstallmentMailer < ApplicationMailer
  before_action :en_locale, only: [:installment_email_to_admin, :inform_super_admin]

  ## ------------------------------------------------------------ ##

  def installment_email_to_admin(id, admin_id)
    @installment  = Installment.find(id)
    @employee = @installment.employee
    @admin    = Employee.find(admin_id)

    @title   = I18n.t('emails.installment.to_admin.title')
    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @message = I18n.t('emails.installment.to_admin.message', user: @installment.employee.full_name)

    mail(to: @admin.email, subject: @title)
  end

  ## ------------------------------------------------------------ ##

  def send_approval_email(id)
    @installment = Installment.find(id)
    I18n.locale = @installment.locale.to_sym
    @employee = @installment.employee

    @intro   = I18n.t('emails.dear_user', user: @employee.full_name)
    @title   = I18n.t("emails.installment.approval_email.title.#{@installment.status}")
    @message = I18n.t("emails.installment.approval_email.message.#{@installment.status}")

    @installment.attachments.each do |file|
      attachments[file.attachment_file_name.to_s] = File.read(file.attachment.path) if file.attachment.exists?
    end

    mail(to: @employee.email, subject: @title)
  end

  ## ------------------------------------------------------------ ##

  def inform_super_admin(id, admin_id, sub_admin_id)
    I18n.locale = :en
    @installment   = Installment.find(id)
    @employee  = @installment.employee
    @admin     = Employee.find(admin_id)
    @sub_admin = Employee.find(sub_admin_id)

    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @title   = I18n.t("emails.installment.inform_super_admin.title")
    @message = I18n.t('emails.installment.inform_super_admin.message', user: @sub_admin.full_name)

    mail(to: @admin.email, subject: @title)
  end
end
