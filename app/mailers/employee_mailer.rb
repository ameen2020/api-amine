class EmployeeMailer < ApplicationMailer
  def forgot_password(employee_id, token)
    @employee = Employee.find(employee_id)
    @recover_link = URI.join(Rails.application.secrets.forgot_password_link, token.to_s).to_s

    mail to: @employee.email
  end

  # Send Password to email
  def send_password(employee_id, password)
    attachments['ios_app.png']    = File.read("#{Rails.root}/public/ios_app.png")
    attachments['play_store.png'] = File.read("#{Rails.root}/public/play_store.png")
    @employee = Employee.find(employee_id)
    @password = password
    mail(to: @employee.email)
  end

  def send_message_to_email(id, admin, message)
    @employee = Employee.find(id)
    @message  = message
    @name = @employee.full_name

    mail(to: @employee.email, from: admin.email)
  end

  def send_e_card(id, occasion_name)
    @image_path = "#{Rails.root}/public/#{occasion_name}.png"
    @employee   = Employee.find(id)
    @name       = @employee.full_name
    @style      = occasion_name


    mail(to: @employee.email)
  end
end
