class EventRegistrationMailer < ApplicationMailer
  before_action :en_locale, only: [:new_registration, :cancel_by_user]

  def send_registration_code(id)
    @registration = EventRegistration.find(id)
    I18n.locale   = @registration.locale.to_sym
    @employee     = @registration.employee

    @title    = I18n.t('emails.events.registration_code.title', title: @registration.event.title)
    @intro    = I18n.t('emails.dear_user', user: @employee.full_name)
    @message  = I18n.t('emails.events.registration_code.message', title: @registration.event.title)

    mail(to: @employee.email, subject: @title)
  end

  def reminder(id, days)
    @registration = EventRegistration.find(id)
    I18n.locale   = @registration.locale.to_sym
    @employee     = @registration.employee

    @title    = I18n.t('emails.events.reminder.title', title: @registration.event.title)
    @intro    = I18n.t('emails.dear_user', user: @employee.full_name)
    @message  = I18n.t('emails.events.reminder.message', title: @registration.event.title, count: days).html_safe

    mail(to: @employee.email, subject: @title)
  end

  def new_registration(id, admin_id)
    @registration = EventRegistration.find(id)
    @admin        = Employee.find(admin_id)
    @employee     = @registration.employee

    @title    = I18n.t('emails.events.new_registration.title', title: @registration.event.title)
    @intro    = I18n.t('emails.dear_user', user: @admin.full_name)
    @message  = I18n.t('emails.events.new_registration.message', title: @registration.event.title, employee: @employee.full_name)

    mail(to: @admin.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end

  def cancel_by_user(admin_id, employee_id, event_id, registration)
    @admin        = Employee.find(admin_id)
    @employee     = Employee.find(employee_id)
    @event        = Event.find(event_id)
    @registration = registration

    @title   = I18n.t('emails.events.cancel_by_user.title', employee: @employee.full_name)
    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @message = I18n.t('emails.events.cancel_by_user.message', title: @event.title, employee: @employee.full_name)

    mail(to: @admin.email, subject: @title)
  end

  def inform_user_of_delete(employee_id, event_title, locale = :en)
    I18n.locale = locale.to_sym
    @employee   = Employee.find(employee_id)
    @title      = I18n.t('emails.events.inform_user_of_delete.title', title: event_title)
    @intro      = I18n.t('emails.dear_user', user: @employee.full_name)
    @message    = I18n.t('emails.events.inform_user_of_delete.message', title: event_title)

    mail(to: @employee.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end
end
