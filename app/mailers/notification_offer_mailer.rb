class NotificationOfferMailer < ApplicationMailer
  def reminder(id, offer)
    @offer    = Offer.find(offer)
    @employee = Employee.find(id)
    # I18n.locale   = @offer.locale.to_sym

    @title   = I18n.t('emails.offers.reminder.title', title: @offer.title)
    @intro   = I18n.t('emails.dear_user', user: @employee.full_name)
    @message = I18n.t('emails.offers.reminder.message', title: @offer.title, date_time: @offer.expire_on.to_date).html_safe

    mail(to: @employee.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end
end
