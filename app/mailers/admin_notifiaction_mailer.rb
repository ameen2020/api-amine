# frozen_string_literal: true

class AdminNotifiactionMailer < ApplicationMailer
  def new_post(to, notification)
    @admin    = to
    @employee = notification.employee
    @record   = notification.noticeable

    @title   = I18n.t('emails.notifiaction_admin.new_post.title', title: @record.title, model_name: @record.model_name.human)
    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @message = I18n.t('emails.notifiaction_admin.new_post.message', title: @record.title, model_name: @record.model_name.human, employee: @employee.full_name).html_safe

    mail(to: @employee.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end
end
