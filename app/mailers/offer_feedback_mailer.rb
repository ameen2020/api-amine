class OfferFeedbackMailer < ApplicationMailer
  before_action :en_locale, only: [:infrom_admins]

  def infrom_admins(id, email)
    @feedback = OfferFeedback.find(id)
    @employee = @feedback.employee

    @title   = I18n.t('emails.feedbacks.infrom_admins.title', title: @feedback.offer.title_en)
    @intro   = I18n.t('emails.dear_user', user: 'Admin')
    @message = I18n.t('emails.feedbacks.infrom_admins.message',
      employee: @feedback.employee.full_name,
      offer:    @feedback.offer.title,
      rating:   @feedback.rating,
      comment:  @feedback.comment
    ).html_safe

    mail(to: email, subject: 'Ashanak Request')
  end
end
