class ChaletRegistrationMailer < ApplicationMailer
  before_action :en_locale, only: [:new_registration, :cancel_by_user, :inform_super_admin]

  def send_approval_email(id)
    @registration = ChaletRegistration.find(id)
    I18n.locale   = @registration.locale.to_sym
    @status       = @registration.rejected? ? I18n.t(:rejection) : I18n.t(:approval)

    @title   = I18n.t('emails.chalets.approval.title', title: @registration.chalet.title, status: @status)
    @intro   = I18n.t('emails.dear_user', user: @registration.employee.full_name)
    @message = I18n.t('emails.chalets.approval.message', title: @registration.chalet.title, status: @status)
    @message = @message + "<br><br>" + I18n.t('emails.reason') + " " +  @registration.comments if @registration.rejected?
    @message = @message.html_safe

    mail(to: @registration.employee.email, subject: @title)
  end


  def send_registration_code(id)
    @registration = ChaletRegistration.find(id)
    I18n.locale   = @registration.locale.to_sym
    @employee     = @registration.employee

    @title    = I18n.t('emails.chalets.registration_code.title', title: @registration.chalet.title)
    @intro    = I18n.t('emails.dear_user', user: @employee.full_name)
    @message  = I18n.t('emails.chalets.registration_code.message', title: @registration.chalet.title)

    mail(to: @employee.email, subject: @title)
  end

  def reminder(id, days)
    @registration = ChaletRegistration.find(id)
    I18n.locale   = @registration.locale.to_sym
    @employee     = @registration.employee

    @title    = I18n.t('emails.chalets.reminder.title', title: @registration.chalet.title)
    @intro    = I18n.t('emails.dear_user', user: @employee.full_name)
    @message  = I18n.t('emails.chalets.reminder.message', title: @registration.chalet.title, count: days).html_safe

    mail(to: @employee.email, subject: @title)
  end

  def new_registration(id, admin_id)
    @registration = ChaletRegistration.find(id)
    @admin        = Employee.find(admin_id)
    @employee     = @registration.employee

    @title    = I18n.t('emails.chalets.new_registration.title', title: @registration.chalet.title)
    @intro    = I18n.t('emails.dear_user', user: @admin.full_name)
    @message  = I18n.t('emails.chalets.new_registration.message', title: @registration.chalet.title, employee: @employee.full_name)

    mail(to: @admin.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end

  ## ------------------------------------------------------------ ##

  def inform_super_admin(id, admin_id)
    @registration = ChaletRegistration.find(id)
    @admin        = Employee.find(admin_id)

    @title   = I18n.t('emails.chalets.inform_super_admin.title', title: @registration.chalet.title)
    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @message = I18n.t('emails.chalets.inform_super_admin.message', employee: @registration.employee.full_name, title: @registration.chalet.title)

    mail(to: @admin.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end

  ## ------------------------------------------------------------ ##


  def cancel_by_user(admin_id, employee_id, chalet_id, registration)
    @admin        = Employee.find(admin_id)
    @employee     = Employee.find(employee_id)
    @chalet        = Chalet.find(chalet_id)
    @registration = registration

    @title   = I18n.t('emails.chalets.cancel_by_user.title', employee: @employee.full_name)
    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @message = I18n.t('emails.chalets.cancel_by_user.message', title: @chalet.title, employee: @employee.full_name)

    mail(to: @admin.email, subject: @title)
  end

  def inform_user_of_delete(employee_id, chalet_title, locale = :en)
    I18n.locale = locale.to_sym
    @employee   = Employee.find(employee_id)
    @title      = I18n.t('emails.chalets.inform_user_of_delete.title', title: chalet_title)
    @intro      = I18n.t('emails.dear_user', user: @employee.full_name)
    @message    = I18n.t('emails.chalets.inform_user_of_delete.message', title: chalet_title)

    mail(to: @employee.email, subject: @title, template_path: 'layouts', template_name: 'mailer_message')
  end
end
