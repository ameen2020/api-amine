class AshanakMailer < ApplicationMailer
  before_action :en_locale, only: [:ashanak_email_to_admin, :inform_super_admin]

  ## ------------------------------------------------------------ ##

  def ashanak_email_to_admin(id, admin_id)
    @ashanak  = Ashanak.find(id)
    @employee = @ashanak.employee
    @admin    = Employee.find(admin_id)

    @title   = I18n.t('emails.ashanak.to_admin.title')
    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @message = I18n.t('emails.ashanak.to_admin.message', user: @ashanak.employee.full_name, occasion: @ashanak.occasion.title)

    mail(to: @admin.email, subject: @title)
  end

  ## ------------------------------------------------------------ ##

  def send_approval_email(id)
    @ashanak = Ashanak.find(id)
    I18n.locale = @ashanak.locale.to_sym
    @employee = @ashanak.employee

    @intro   = I18n.t('emails.dear_user', user: @employee.full_name)
    @title   = I18n.t("emails.ashanak.approval_email.title.#{@ashanak.status}")
    @message = I18n.t("emails.ashanak.approval_email.message.#{@ashanak.status}", occasion: @ashanak.occasion.title).html_safe

    @ashanak.attachments.by_admins.each do |file|
      attachments[file.attachment_file_name.to_s] = File.read(file.attachment.path) if file.attachment.exists?
    end

    mail(to: @employee.email, subject: @title)
  end

  ## ------------------------------------------------------------ ##

  def inform_super_admin(id, admin_id, sub_admin_id)
    I18n.locale = :en
    @ashanak   = Ashanak.find(id)
    @employee  = @ashanak.employee
    @admin     = Employee.find(admin_id)
    @sub_admin = Employee.find(sub_admin_id)

    @intro   = I18n.t('emails.dear_user', user: @admin.full_name)
    @title   = I18n.t("emails.ashanak.inform_super_admin.title")
    @message = I18n.t('emails.ashanak.inform_super_admin.message', user: @sub_admin.full_name, occasion: @ashanak.occasion.title)

    mail(to: @admin.email, subject: @title)
  end

  ## ------------------------------------------------------------ ##

  def export_ashanaks(email, ids, message)
    records = Ashanak.where(id: ids)

    data = Exporter::Export.new(collection: records, encoding: 'UTF-8').export
    attachments['testahil-records.xls'] = data

    employee = Employee.find_by(email: email)
    @name = employee.present? ? employee.full_name : email
    @message = message
    mail(to: email)
  end
end
