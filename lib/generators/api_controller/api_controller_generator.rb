class ApiControllerGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  desc 'Generate api controller'
  def create_presenter_file
    create_file "app/controllers/#{name}_controller.rb", <<-FILE
class #{class_name}Controller < V1::BaseController
  power :#{file_name.pluralize}, map: {
    [:index]   => :#{file_name.pluralize}_index,
    [:show]    => :#{file_name}_show,
    [:create]  => :creatable_#{file_name},
    [:update]  => :updatable_#{file_name},
    [:destroy] => :destroyable_#{file_name}
  }, as: :#{file_name.pluralize}_scope

  ## ------------------------------------------------------------ ##

  # GET : /v1/#{file_name.pluralize}/
  # Inherited from V1::BaseController
  # def index; end

  ## ------------------------------------------------------------ ##

  # POST : /v1/#{file_name.pluralize}/
  # Inherited from V1::BaseController
  # def create; end

  ## ------------------------------------------------------------ ##

  # GET : /v1/#{file_name.pluralize}/:id
  # Inherited from V1::BaseController
  # def show; end

  ## ------------------------------------------------------------ ##

  # PUT : /v1/#{file_name.pluralize}/:id
  # Inherited from V1::BaseController
  # def update; end

  ## ------------------------------------------------------------ ##

  # DELETE : /v1/#{file_name.pluralize}/:id
  # Inherited from V1::BaseController
  # def destroy; end

  ## ------------------------------------------------------------ ##

  private

  # Whitelist parameters
  def #{file_name}_params
    params.permit()
  end

  # Search filters
  def search_params
    {
      id_eq: params[:id]
    }
  end

  # Custom ordering and sorting
  def get_order
    { sort_column(:created_at) => sort_direction(:desc) }
  end
end
FILE
  end
end
