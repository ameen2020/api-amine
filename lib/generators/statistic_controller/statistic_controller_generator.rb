class StatisticControllerGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  desc 'Generate statistic controller'
  def create_presenter_file
    create_file "app/controllers/v1/dashboard/#{name}_controller.rb", <<-FILE
class V1::Dashboard::#{class_name}Controller < V1::Dashboard::StatisticsController

  ## ------------------------------------------------------------ ##

  # GET : /v1/dashboard/#{file_name.pluralize}/
  # Inherited from V1::Dashboard::StatisticsController
  # def index; end

  ## ------------------------------------------------------------ ##

  private

  # Override model in parent
  # Model name
  def model
    ""
  end

  # Model joins
  def model_joins
    []
  end

  # Group attributes
  def group_attributes
    []
  end

  # Select query
  # Returns array query of strings
  def query
    []
  end

  # Search filters
  def search_params
    {}
  end

  # Override percentage rounder in parent
  # Select between collection_percentage_rounder or percentage_rounder
  def rounder(percent)

  end
end
FILE
  end
end
