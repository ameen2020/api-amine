namespace :test do
  desc "Test different configs for mailers"
  task emails: :environment do
    ips     = ['10.0.225.99', '10.0.225.26', '10.0.225.27', '10.0.225.51', '10.0.255.52']
    users   = ['ashanak@moh.gov.sa', 'moh\ashanak', 'moh/ashanak', 'ashanak']
    domains = [nil, 'moh.gov.sa']
    auths   = [:plain, :login, :authentication, :cram_md5, 'ntlm']
    ssls    = [true, false]
    pass    = Rails.application.secrets.email[:password]
    counter = 0

    ips.each do |ip|
      users.each do |user|
        auths.each do |auth|
          ssls.each do |ssl|
            domains.each do |domain|
              configs = {
                address: ip,
                port: 25,
                user_name: user,
                password: pass ,
                authentication: auth,
                enable_starttls_auto: ssl,
              }
              configs.merge!(domain: domain) if domain

              ActionMailer::Base.smtp_settings = configs

              begin
                TestMailer.test.deliver_now!
                puts "\e[32;1m[Success]\e[0m - #{configs.as_json}"
              rescue => e
                puts "\e[41;1m[Fail]\e[0m #{e.message} - #{configs.as_json}"
              end

              counter += 1
            end
          end
        end
      end
    end
    puts counter
  end
end
