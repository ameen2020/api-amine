# Import Employee in CSV File
# and save in datbase
require 'csv'
namespace :import_sheets do
  employees          = File.join Rails.root, 'public/Lean Employees.csv'
  desc 'Import Employees Task'
  task employees: :environment do
    Rake::Task['import_sheets:employees'].invoke
  end
  desc 'Import Employees from sheet'
  task employees_create: :environment do
    ActiveRecord::Base.transaction do
      CSV.foreach(employees) do |row|
        full_name = row[2].split(' ')
        password = generate_password
        employee = Employee.create(
          last_name:          full_name.pop,                  # Select Last Name in Full Name
          first_name:         full_name.join(' '),            # Select First Name and middle name
          email:              row[4].downcase,                # convert email to downcase
          password:           password,              # generate password
          phone_number:       row[3],                         # phone number
          username:           username(row[4]).to_s.downcase  # choose username like this => MOHlean
        )
        employee.save
        # Send Password after create employee
        EmployeeMailer.send_password(employee.id, password).deliver_later
      end
    end
  end
  # Split email used @ & . , after that choose email id and join domian 
  # Return string 
  def username(row)
    row.split('@')[0] + row.split('@')[1].split('.')[0]
  end
  # Generate random password contain on 8 characters
  def generate_password
    SecureRandom.hex(4)
  end
end


