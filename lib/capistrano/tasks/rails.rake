# frozen_string_literal: true

namespace :rails do
  desc 'Remote console'
  task :console do
    on roles(:app) do |h|
      run_interactively "bundle exec rails console #{fetch(:rails_env)}", h.user
    end
  end

  desc 'Remote dbconsole'
  task :dbconsole do
    on roles(:app) do |h|
      run_interactively "bundle exec rails dbconsole #{fetch(:rails_env)}", h.user
    end
  end

  desc 'Remote dbconsole'
  task :logs do
    on roles(:app) do |h|
      run_interactively "tail -f -n 100 log/#{fetch(:rails_env)}.log", h.user
    end
  end

  def run_interactively(command, user)
    info "Running `#{command}` as #{user}@#{host}"
    exec %(ssh #{user}@#{host} -t "bash --login -c 'cd #{fetch(:deploy_to)}/current && #{command}'")
  end
  # To Run the comands in console
  # cap dev rails:console  -> To run console in specefic server
  # cap dev rails:dbconsole -> To run query inside the your database
  # cap dev rails:log -> To see the logo

  desc 'Tail rails logs from server'
  task :log, :param do |_task, args|
    SSHKit.config.output_verbosity = Logger::DEBUG
    on roles(:app) do
      execute "tail #{args[:param]} -f #{shared_path}/log/#{fetch(:rails_env)}.log"
    end
  end

  desc 'Tail custom logs from server'
  task :log_file, :file do |_task, args|
    SSHKit.config.output_verbosity = Logger::DEBUG
    on roles(:app) do
      execute "tail -f #{shared_path}/log/#{args[:file]}.log"
    end
  end

  desc 'Run Rake task on server'
  task :rake, :param do |_task, args|
    on roles(:app) do
      within current_path.to_s do
        with rails_env: fetch(:stage).to_s do
          execute :rake, args[:param]
        end
      end
    end
  end

  desc 'Stop Everything'
  task :stop_all, :param do |_task, _args|
    on roles(:app) do
      within current_path.to_s do
        with rails_env: fetch(:stage).to_s do
          invoke 'puma:stop'
          invoke 'resque:stop'
          invoke 'resque:scheduler:stop'
        end
      end
    end
  end

  desc 'Start Everything'
  task :start_all, :param do |_task, _args|
    on roles(:app) do
      within current_path.to_s do
        with rails_env: fetch(:stage).to_s do
          invoke 'puma:start'
          invoke 'resque:start'
          invoke 'resque:scheduler:start'
        end
      end
    end
  end

  namespace :db do
    desc 'Backup Database'
    task :backup, :param do |_task, _args|
      on roles(:app) do
        within current_path.to_s do
          with rails_env: fetch(:stage).to_s do
            execute "cat #{shared_path}/config/database.yml"
            set :database_name, ask('Enter database Name')
            set :database_user, ask('Enter database User')
            set :database_pass, ask('Enter database Password')
            set :backup_name,   "#{fetch(:application)}-#{Time.now.strftime('%Y%m%d-%H%M%S')}-#{fetch(:stage)}"
            execute "PGPASSWORD=#{fetch(:database_pass)} pg_dump -h localhost -U #{fetch(:database_user)} -d #{fetch(:database_name)} >> ~/#{fetch(:backup_name)}.sql"
            execute "gzip ~/#{fetch(:backup_name)}.sql"
          end
        end
      end
    end

    desc 'Import Backup'
    task :import, :param do |_task, _args|
      on roles(:app) do
        execute 'ls ~'
        within current_path.to_s do
          with rails_env: fetch(:stage).to_s do
            execute "cat #{shared_path}/config/database.yml"
            set :database_name, ask('Enter database Name')
            set :database_user, ask('Enter database User')
            set :database_pass, ask('Enter database Password')
            set :backup_name,   ask('Enter Backup SQL File Name')

            invoke 'rails:stop_all'
            execute :rake, 'db:drop'
            execute :rake, 'db:create'

            execute "PGPASSWORD=#{fetch(:database_pass)} psql -U #{fetch(:database_user)} -h localhost -d #{fetch(:database_name)} -f ~/#{fetch(:backup_name)}"

            invoke 'rails:start_all'
          end
        end
      end
    end
  end
end
