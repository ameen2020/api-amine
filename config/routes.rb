require 'resque/server'
require 'resque-history/server'
require 'resque/scheduler/server'

Resque::Server.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [Rails.application.secrets.resque[:username], Rails.application.secrets.resque[:password]]
end

Rails.application.routes.draw do
  # mount resque web interface
  mount Resque::Server.new, at: "/resque"

  root to: ->(_env) { [200, { 'Content-Type' => 'application/json' }, [{ success: true, message: 'Ashanak Server is UP'}.to_json]] }

  namespace :v1, constraints: { format: 'json' } do

    resources :dashboard, only: [] do
      collection do
        get :counters
        get :user_registrations
        get :trip_registered_users
        get :events_registered_users
        get :contact_us
        get :number_of_views
        get :offers_baseds
        get :offers
        get :testahels
        get :number_of
      end
    end

    resources :employees do
      get :export, on: :collection
    end
    resources :sessions, only: [:create]

    resources :passwords, only: [] do
      collection do
        put  :update, path: nil
        post :forgot
        post :recover
      end
    end

    resources :categories
    resources :sub_categories
    resources :relationships
    resources :feedback_categories

    resources :profiles, only: :index do
      collection do
        post :change_phone_number
        post :confirm_otp
        get  :events
        get  :trips
        get  :chalets
      end
    end

    resources :events do
      collection do
        get :calendar
      end
      member do
        get    :export_registerations
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
      end
      resources :event_registration, path: :registration, only: [:index] do
        collection do
          post   :create
          delete :destroy
          post   :send_email
        end
      end
    end
    resources :chalets do
      # collection do
      #   get :calendar
      # end
      member do
        get    :export_registerations
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
      end
      resources :chalet_registration, path: :registration, only: [:index] do
        collection do
          post   :create
          delete :destroy
          post   :send_email
        end
        post :approve, on: :member
      end
    end

    resources :offers do
      member do
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
        post   :generate_pdf
      end
      resources :offer_feedbacks,   path: :feedbacks,   only: %i[index show create]
      resources :offer_attachments, path: :attachments, only: %i[index create destroy]
    end

    resources :regions do
      member do
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
      end
    end

    resources :work_locations
    resources :memos do
      member do
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
      end
    end

    resources :magazines do
      member do
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
      end
    end

    resources :news do
      member do
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
      end
    end

    resources :assignments do
      member do
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
      end
    end

    resources :trips do
      collection do
        get :calendar
      end
      resources :trip_registrations, path: :registration, only: [:index] do
        collection do
          post   :create
          delete :destroy
          post   :send_email
        end
        post :approve, on: :member
      end
      member do
        get    :export_registerations
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
      end
    end

    resources :relationships
    resources :occasions
    resources :ashanaks do
      post :approve, on: :member
      get  :export,  on: :collection
      get  :send_e_card,  on: :member
      get  :generat_e_card,  on: :member
      get  :sticker, on: :member
      post :email_export, on: :collection
    end

    resources :notifications_settings, only: [] do
      collection do
        get :show
        put :update
      end
    end

    resources :notifications_histories, path: :notifications, only: %i[index show] do
      get :unread_notiifications, on: :collection
    end

    resources :contact_us_infos

    resources :contact_us, except: :destroy do
      get :export, on: :collection
    end

    resources :trip_black_lists
    resources :admin_notifications, only: %i[index show]

    resources :mbadrats do
      member do
        put    :set_cover
        post   :add_image,    path: :images
        delete :remove_image, path: :images
      end
    end

    resources :employee_voices
    resources :nationalities
    resources :installments do
      post :approve, on: :member
      # get  :export,  on: :collection
    end
    resources :messages
    resources :versions, only: [:index, :show, :update] do
      get :options, on: :collection
    end
    resources :pages, only: %i[index show update]

  end
end
