require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
# require "action_cable/engine"
# require "sprockets/railtie"
# require "rails/test_unit/railtie"
require 'csv'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MOH
  class Application < Rails::Application

    # Assets Host Base URL
    config.action_controller.asset_host = Rails.application.secrets.assets_host

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Only loads a smaller set of middleware suitable for API only apps.
    config.api_only = true

    # Load all locale files recursively
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    # Load RakeAttack middleware for api throttling
    config.middleware.use Rack::Attack

    # Accepted and supportd locales
    config.i18n.available_locales = [:en, :ar]

    # The default timezone
    config.time_zone = 'UTC'

    # Set Resque as queue adapter .
    config.active_job.queue_adapter = :resque
  end
end
