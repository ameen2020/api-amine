Resque.redis = $redis
Resque.redis.namespace = "ashanak:#{Rails.env}"
Resque.logger = Logger.new(Rails.root.join('log', "resque_#{Rails.env}.log"))
Resque.schedule = YAML.load_file(Rails.root.join('config', 'schedule.yml'))
