$redis = Redis.new(
  host: Rails.application.secrets.redis[:host],
  port: Rails.application.secrets.redis[:port],
  db:   Rails.application.secrets.redis[:db]
)
