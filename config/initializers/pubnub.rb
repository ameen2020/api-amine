
$pubnub = Pubnub.new(
  subscribe_key: Rails.application.secrets.pubnub[:subscribe_key],
  publish_key: Rails.application.secrets.pubnub[:publish_key]
)
