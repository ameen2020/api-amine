# frozen_string_literal: true

# server-based syntax
# ======================
set :branch, 'staging'
set :resque_rails_env, 'staging'
role :resque_worker, '52.220.140.207'
role :resque_scheduler, '52.220.140.207'
set :nginx_server_name, "api.staging.moh.clicksandbox1.com #{fetch(:application)}.local"

set :deploy_to, "/var/www/html/#{fetch(:application)}-#{fetch(:branch)}"
set :workers, '*' => 1

server '52.220.140.207', user: 'ubuntu', roles: %w[web app db]
