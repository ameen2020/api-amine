# frozen_string_literal: true

# server-based syntax
# ======================
set :branch, 'staging'
set :resque_rails_env, 'production'
role :resque_worker, '127.0.0.1:2222'
role :resque_scheduler, '127.0.0.1:2222'

set :deploy_to, "/var/www/html/#{fetch(:application)}"

server '127.0.0.1:2222', user: 'ubuntu', roles: %w[web app db]
