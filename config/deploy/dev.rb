# frozen_string_literal: true

# server-based syntax
# ======================
set :branch, 'dev'
set :resque_rails_env, 'dev'

role :resque_worker, '52.220.140.207'
role :resque_scheduler, '52.220.140.207'

set :nginx_server_name, 'api.dev.moh.clicksandbox.com'
set :workers, '*' => 1

server '52.220.140.207', user: 'ubuntu', roles: %w[web app db]
