# config valid only for current version of Capistrano
lock '3.9.0'

set :application, 'moh'
set :repo_url, 'git@bitbucket.org:clickapps2015/moh-api.git'
set :rails_env, fetch(:stage)
append :rvm_map_bins, 'rails'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/var/www/html/#{fetch(:application)}"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :false

set :rvm_type, :user
set :rvm_ruby_version, '2.4.1@moh'

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system", "public/upload", "public/dummy"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
set :keep_releases, 3

set :puma_threads, [1, 16]
set :puma_workers, 1

set :workers, '*' => 2
set :interval, '1'
set :resque_environment_task, true

# Restart Rescue after finishing the Deployment
namespace :deploy do
  # restart resque
  after :finishing, :restart_resque do
    invoke 'resque:restart'
    invoke 'resque:scheduler:restart'
  end
end
