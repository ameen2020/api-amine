class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.jsonb    :title_translations, null: false
      t.jsonb    :content_translations, null: false

      t.timestamps
    end
    Page.create(
      {
        title_translations: {
          ar: "شروط عشانك",
          en: "Ashanak terms"
        },
        content_translations: {
          ar: "# شروط الاستخدام  ---  ## شروط برنامج تستاهل (للزواج):\n 1. أن يكون الزواج هو الأول للمتقدم.\n 2. رفع الطلب خلال شهر كحد أقصى من تاريخ عقد النكاح.\n 3. تحري الدقة والصحة في تعبئة كامل المعلومات مثل: المناسبة، تاريخ المناسبة، المنطقة، المدينة، رقم الجوال، العنوان.\n 4. إرفاق جميع الوثائق المطلوبة (عقد النكاح، بطاقة العمل).\n 5. لن يتم قبول أي طلب للمتقدم في حال عدم وضوح المرفقات.\n 6. نأمل إبراز بطاقة العمل عند استلام الهدية.  ## شروط برنامج تستاهل (للمولود):\n 1. رفع الطلب خلال شهر كحد أقصى من تاريخ الولادة.\n 2. تحري الدقة والصحة في تعبئة كامل المعلومات مثل: المناسبة، تاريخ المناسبة، المنطقة، المدينة، رقم الجوال، العنوان.\n 3. إرفاق جميع الوثائق المطلوبة (شهادة الميلاد، بطاقة العمل).\n 4. لن يتم قبول أي طلب للمتقدم في حال عدم وضوح المرفقات.\n 5. نأمل إبراز بطاقة العمل عند استلام الهدية.  ## شروط برنامج تستاهل (للترقية):\n 1. رفع الطلب خلال شهر كحد أقصى من تاريخ الترقية.\n 2. تحري الدقة والصحة في تعبئة كامل المعلومات مثل: المناسبة، تاريخ المناسبة، المنطقة، المدينة، رقم الجوال، العنوان.\n 3. إرفاق جميع الوثائق المطلوبة (القرار الإداري للترقية، بطاقة العمل).\n 4. لن يتم قبول أي طلب للمتقدم في حال عدم وضوح المرفقات.", 
          en: "# Ashanak Terms and Conditions  ---\n ## Testahel program conditions (Marriage):\n 1. Has to be the first marriage for the applicant.\n 2. Apply in one-month period maximum from marriage date.\n 3. Fill all the information correctly. For example: Occasion, Occasion date, Region, Mobile number and Address.\n Attached all the required documents (Marriage certificate and MOH Employee ID )\n 4. Note that all attachments should be readable (High Resolution pictures).\n 5. Show MOH employee ID when receiving the gift.  ## Testahel program conditions (Baby born):  1. Apply in one-month period maximum from baby born date.\n 2. Fill all the information correctly. For example: Occasion, Occasion date, Region, Mobile number and Address.\n 3. Attached all the required documents (MOH employee ID, Birth certificate).\n 4. Note that all attachments should be readable (High Resolution pictures).\n 5. Show MOH employee ID when you receive the gift.  ## Testahel program conditions (Promotion):  1. Apply in one-month period maximum from promotion date.\n 2. Occasion, Occasion date, Region, City and Mobile number.\n 3. Fill all information and attach the requirement (Administrative improvement for promotion, MOH employee ID)"
        }
      }
    )
    Page.create(
      {
        title_translations: {
          ar: "الخصوصيات",
          en: "Privcy"
        },
        content_translations: {
          ar: " # بيان الخصوصية  ---  وزارة الصحة صممت تطبيق عشانك باعتبارها التطبيق المجاني لموظفي وزارة الصحة.  يتم استخدام هذه الصفحة لإعلام مستخدمي التطبيق فيما يتعلق بسياساتنا من خلال جمع المعلومات الشخصية واستخدامها والكشف عنها إذا قرر أي شخص استخدام خدمتنا.  في حال اخترت استخدام التطبيق، فإنك توافق على جمع واستخدام المعلومات المتعلقة بهذه السياسة. يتم استخدام المعلومات الشخصية التي نجمعها لتوفير وتحسين الخدمة. لن نستخدم أو نشارك معلوماتك الشخصية مع أي شخص باستثناء ما هو موضح في سياسة الخصوصية هذه.  للحصول على تجربة أفضل، أثناء استخدام خدماتنا، قد نطلب منك تزويدنا ببعض المعلومات التعريفية الشخصية مثل اسم المستخدم وكلمة المرور الخاصة بوزارة الصحة من أجل الوصول إلى التطبيق .",
          en: "# Privacy Policy  ---  Ministry of Health has built Ashanak app as a free app for Ministry of Health employees around the kingdom.  This page is used to inform the app users regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.  If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your personal information with anyone except as described in this Privacy Policy.  ## Information Collection and Use  For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information such as your MoH username and password in order to access the app"
        }
      }
    )
    Page.create(
      {
        title_translations: {
          ar: "الشروط",
          en: "Terms"
        },
        content_translations: {
          ar: "# شروط التسجيل  ---  1.  أتحمل كافة المسؤولية بالمعلومات المسجلة وأنها تمثلني.\n 2. الالتزام بالأخلاق العامة والمحافظة على مقتنيات الجهة المقرر زيارتها والممتلكات (الباص – الفندق – الطيران .... إلخ).\n 3. الالتزام بالوقت المحدد للرحلة.\n 4. الالتزام بجدول الرحلة المعد من قبل الجهة المنظمة.\n 5. في حال عدم الالتزام بوقت الرحلة أو الجدول المعد لذلك يحق للجهة المسؤولة اتخاذ إجراء ضدي في حال مخالفة ذلك.\n 6. إشعار الجهة المنظمة في حال عدم الحضور قبل موعد الرحلة بثلاثة أيام حتى لا يتم إضافة اسمك في القائمة السوداء وحرمانك من المشاركات القادمة .",
          en: "# Terms and Conditions  ---  1. I take full responsibility for the registered information, and it represents me.\n 1. Commitment to public morals, acquisitions and properties preservation of the entity that will be visited (Bus- Hotel- Airplane…etc)\n 1. Commitment to the specific time of the trip.\n 1. Commitment to the schedule’s trip prepared by the entity.\n 1. In case of non-compliance with the time and the prepared schedule, the responsible entity shall take an action against me in case of violation.\n 1. In case of non-attendance, the entity shall be noticed no later than THREE days of the trip.\n 1. In case of non-attendance, and not noticing the entity no later than Three days, your name will be added to the black list and you won’t be able to apply for any further trips."
        }
      }
    )
  end
end
