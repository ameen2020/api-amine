class AddOtherColumnsToNotificationsSetting < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications_settings, :mbadrat, :boolean, default:false, null: false
    add_column :notifications_settings, :employee_voice, :boolean, default:false, null: false


    add_index :notifications_settings, :mbadrat
    add_index :notifications_settings, :employee_voice
  end
end
