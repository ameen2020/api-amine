class AddInstallmentToNotificationSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications_settings, :installment, :boolean, default:false, null: false 

    add_index :notifications_settings, :installment
  end
end
