class AddLocationsToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :locations, :json, null: false, default: []

    reversible do |dir|
      dir.up do
        Event.all.map {|e| e.update(locations: [{latitude: e.latitude, longitude: e.longitude}])}
      end
    end

    add_column :offers, :locations, :json, null: false, default: []
  end
end
