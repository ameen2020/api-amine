class AddUsernameToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :username, :string
    add_index  :employees, :username, unique: true, where: 'username is not null'

    Employee.all.each do |emp|
      username = emp.email.split('@')[0]
      emp.update(username: username)
    end

    change_column_null :employees, :username, false
  end
end
