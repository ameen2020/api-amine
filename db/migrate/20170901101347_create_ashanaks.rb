class CreateAshanaks < ActiveRecord::Migration[5.1]
  def change
    create_table :ashanaks do |t|
      t.belongs_to :region, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :occasion, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :work_location, foreign_key: { on_delete: :cascade }, null: false
      t.date :date, null: false
      t.integer :status, default: 0, null: false
      t.timestamps
      t.index :status
    end
  end
end
