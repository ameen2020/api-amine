class CreateOffers < ActiveRecord::Migration[5.1]
  def change
    create_table :offers do |t|
      t.belongs_to :sub_category, foreign_key: { on_delete: :cascade }, null: false
      t.jsonb      :title_translations, null: false
      t.jsonb      :description_translations, null: false
      t.datetime   :expire_on, null: false
      t.decimal    :discount, precision: 10, scale: 2

      t.timestamps
    end
  end
end
