class ChaletsRegions < ActiveRecord::Migration[5.1]
  def change
    create_table :chalets_regions, id: false do |t|
      t.belongs_to :chalet, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :region, foreign_key: { on_delete: :cascade }, null: false

      t.index [:region_id, :chalet_id], unique: true
    end
  end
end
