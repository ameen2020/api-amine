class CreateRegions < ActiveRecord::Migration[5.1]
  def change
    create_table :regions do |t|
      t.jsonb    :title_translations, null: false
      t.integer  :position

      t.timestamps
    end
  end
end
