class OffersRegions < ActiveRecord::Migration[5.1]
  # Create HABTM table
  def change
    create_table :offers_regions, id: false do |t|
      t.belongs_to :offer, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :region, foreign_key: { on_delete: :cascade }, null: false

      t.index [:region_id, :offer_id], unique: true

    end
  end
end
