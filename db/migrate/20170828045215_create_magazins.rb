class CreateMagazins < ActiveRecord::Migration[5.1]
  def change
    create_table :magazins do |t|
      t.jsonb      :title_translations, null: false
      t.jsonb      :description_translations, null: false
      t.attachment :attachment
      
      t.timestamps
    end
  end
end
