class CreateMbadratFaqs < ActiveRecord::Migration[5.1]
  def change
    create_table :mbadrat_faqs do |t|
      t.belongs_to :mbadrat, foreign_key: { on_delete: :cascade }, null: false
      t.jsonb      :title_translations, null: false
      t.jsonb      :description_translations, null: false
      t.timestamps
    end
  end
end
