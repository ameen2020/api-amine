class CreateFeedbackCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :feedback_categories do |t|
      t.jsonb    :title_translations, null: false
      t.integer  :position
      t.timestamps
    end

    add_reference :offer_feedbacks, :category, foreign_key: { to_table: :feedback_categories, on_delete: :cascade }, null: true
  end
end
