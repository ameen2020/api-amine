class AddColumnNoViewsInMultiTable < ActiveRecord::Migration[5.1]
  def change
    add_column :trips,        :views_counter, :integer, default: 0, null: false
    add_column :events,       :views_counter, :integer, default: 0, null: false
    add_column :offers,       :views_counter, :integer, default: 0, null: false
    add_column :memos,        :views_counter, :integer, default: 0, null: false
    add_column :chalets,      :views_counter, :integer, default: 0, null: false
    add_column :magazins,     :views_counter, :integer, default: 0, null: false
    add_column :news,         :views_counter, :integer, default: 0, null: false
    add_column :assignments,  :views_counter, :integer, default: 0, null: false
  end
end
