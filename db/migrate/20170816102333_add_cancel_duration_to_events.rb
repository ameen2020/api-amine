class AddCancelDurationToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :last_cancel, :datetime
  end
end
