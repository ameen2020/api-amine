class CreateNotificationsSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications_settings do |t|
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.boolean :event, default: false, null: false
      t.boolean :offer, default: false, null: false
      t.boolean :trip, default: false, null: false
      t.boolean :news, default: false, null: false
      t.boolean :memo, default: false, null: false
      t.boolean :job, default: false, null: false
      t.boolean :magazine, default: false, null: false
      t.integer :region, array: true, default: []
      t.integer :sub_category, array: true, default: []

      t.timestamps
      t.index :event
      t.index :offer
      t.index :trip
      t.index :news
      t.index :memo
      t.index :job
      t.index :magazine
      t.index :region
      t.index :sub_category
    end
  end
end
