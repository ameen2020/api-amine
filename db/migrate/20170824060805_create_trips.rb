class CreateTrips < ActiveRecord::Migration[5.1]
  def change
    create_table :trips do |t|
      t.jsonb    :title_translations, null: false
      t.jsonb    :description_translations, null: false
      t.jsonb    :location_translations, null: false
      t.datetime :start_date, null: false
      t.datetime :end_date
      t.datetime :last_register_date, null: false
      t.datetime :last_cancel_date
      t.integer  :capacity

      t.timestamps
      t.index :start_date
    end
  end
end
