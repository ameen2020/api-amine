class CreateRelationships < ActiveRecord::Migration[5.1]
  def change
    create_table :relationships do |t|
      t.jsonb   :title_translations, null: false
      t.boolean :status, default: true, null: false
      t.timestamps
    end
  end
end
