class AddRandomAttributeForEventRegistration < ActiveRecord::Migration[5.1]
  def change
    add_column :event_registrations, :register_code, :string
    add_column :trip_registrations, :register_code, :string

    EventRegistration.find_each do |register|
      register.register_code = SecureRandom.hex(3)
      register.save
    end

    TripRegistration.find_each do |register|
      register.register_code = SecureRandom.hex(3)
      register.save
    end

    change_column_null :event_registrations, :register_code, false
    add_index :event_registrations, :register_code, unique: true

    change_column_null :trip_registrations, :register_code, false
    add_index :trip_registrations, :register_code, unique: true
  end
end
