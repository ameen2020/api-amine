class CreateInstallments < ActiveRecord::Migration[5.1]
  def change
    create_table :installments do |t|
      t.belongs_to :nationality,foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.string     :company_name, null: false
      t.string     :locale, null: false, default: 'en'
      t.integer    :super_admin_approve, default: 0, null: false
      t.integer    :sub_admin_approve, default: 0, null: false
      t.text       :super_admin_comment
      t.text       :sub_admin_comment
      t.timestamps
    end
    Category.create({title_translations: { ar: "الأقساط", en: "Installments" }, description_translations: { ar: "Installments details and description", en: "Installments details description" }, category_constant: "INSTALLMENT", position: 11, category_color: Faker::Color.hex_color})
  end
end
