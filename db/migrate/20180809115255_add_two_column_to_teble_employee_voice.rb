class AddTwoColumnToTebleEmployeeVoice < ActiveRecord::Migration[5.1]
  def change
    add_reference :employee_voices, :admin, foreign_key: { to_table: :employees, on_delete: :cascade }, null: true
    add_column :employee_voices, :reply_message, :text
    add_column :employee_voices, :replied_at,    :timestamp
    add_column :employee_voices, :locale,        :string,    null: false, default: 'en'
    add_attachment :employee_voices, :attachment_admin
  end
end
