class CreateAshanakAttachements < ActiveRecord::Migration[5.1]
  def change
    create_table :ashanak_attachements do |t|
      t.belongs_to :ashanak, foreign_key: { on_delete: :cascade }, null: false
      t.attachment :attachment

      t.timestamps
    end
  end
end
