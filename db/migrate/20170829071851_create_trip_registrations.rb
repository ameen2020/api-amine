class CreateTripRegistrations < ActiveRecord::Migration[5.1]
  def change
    create_table :trip_registrations do |t|
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :trip, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :relationship, foreign_key: { on_delete: :cascade }
      t.string   :full_name
      t.integer  :id_number
      t.boolean  :super_admin_approve, default: false, null: false
      t.boolean  :sub_admin_approve, default: false, null: false
      t.boolean  :companion, default: false, null: false
      t.attachment :attachment

      t.timestamps
      t.index [:employee_id, :trip_id], unique: true
    end
  end
end
