class CreateNews < ActiveRecord::Migration[5.1]
  def change
    create_table :news do |t|
      t.jsonb   :title_translations, null: false
      t.jsonb   :description_translations, null: false
      t.date    :date
      t.boolean :important, null: false, default: false

      t.timestamps
      t.index :date
    end
  end
end
