class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.belongs_to  :sender,    foreign_key: { to_table: 'employees', on_delete: :cascade }, null: false, index: true
      t.string      :title,     null: false
      t.string      :message,   null: false

      t.timestamps
    end
  end
end
