class CreateMemos < ActiveRecord::Migration[5.1]
  def change
    create_table :memos do |t|
        t.jsonb   :title_translations, null: false
        t.jsonb   :description_translations, null: false
        t.date    :date

        t.timestamps
    end
  end
end
