class CreateChalets < ActiveRecord::Migration[5.1]
  def change
    create_table :chalets do |t|
      t.jsonb    :title_translations, null: false
      t.jsonb    :description_translations, null: false
      t.jsonb    :address_translations, null: false
      t.jsonb    :city_translations, null: false
      t.jsonb    :location_translations, null: false
      t.datetime :start_date, null: false
      t.datetime :end_date
      t.integer  :capacity
      t.attachment :attachment
      t.decimal  :latitude, precision: 10, scale: 8, default: "0.0", null: false
      t.decimal  :longitude, precision: 11, scale: 8, default: "0.0", null: false
      t.timestamps
      t.datetime :last_cancel
    end
    Category.create({title_translations: { ar: "الشاليهات", en: "Chalets" }, description_translations: { ar: "Chalets details and description", en: "Chalets details description" }, category_constant: "CHALET", position: 8, category_color: Faker::Color.hex_color})
  end
end
