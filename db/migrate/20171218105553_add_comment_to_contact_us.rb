class AddCommentToContactUs < ActiveRecord::Migration[5.1]
  def change
    add_reference :contact_us, :admin, foreign_key: { to_table: :employees, on_delete: :cascade }, null: true
    add_column :contact_us, :reply_message, :text
    add_column :contact_us, :replied_at,    :timestamp
    add_column :contact_us, :locale,        :string,    null: false, default: 'en'
  end
end
