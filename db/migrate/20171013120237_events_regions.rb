class EventsRegions < ActiveRecord::Migration[5.1]
  def change
    create_table :events_regions, id: false do |t|
      t.belongs_to :event,  foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :region, foreign_key: { on_delete: :cascade }, null: false

      t.index [:region_id, :event_id], unique: true
    end
  end
end
