class AddPhoneNumberAndAddressToAshanaks < ActiveRecord::Migration[5.1]
  def change
    add_column :ashanaks, :phone_number, :string, null: false, default: ''
    add_column :ashanaks, :address,      :text,   null: false, default: ''
  end
end
