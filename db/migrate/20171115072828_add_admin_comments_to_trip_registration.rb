class AddAdminCommentsToTripRegistration < ActiveRecord::Migration[5.1]
  def change
    add_column :trip_registrations, :super_admin_comment, :text
    add_column :trip_registrations, :sub_admin_comment,   :text
  end
end
