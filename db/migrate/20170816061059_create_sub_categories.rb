class CreateSubCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :sub_categories do |t|
      t.jsonb   :title_translations, null: false
      t.integer :status, null: false, default: 1
      t.integer :position

      t.timestamps
    end
  end
end
