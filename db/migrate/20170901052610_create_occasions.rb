class CreateOccasions < ActiveRecord::Migration[5.1]
  def change
    create_table :occasions do |t|
      t.jsonb      :title_translations, null: false
      t.jsonb      :description_translations, null: false
      t.timestamps
    end
  end
end
