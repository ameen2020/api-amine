class RemoveColumnsFromCategories < ActiveRecord::Migration[5.1]
  def up
    remove_column :categories, :logo
    remove_column :categories, :category_type
    change_column_null :categories, :category_constant, false
    add_attachment :categories, :logo
  end

  def down
    remove_attachment :categories, :logo
    change_column_null :categories, :category_constant, true
    add_column :categories, :category_type, :integer
    add_column :categories, :logo, :integer
  end
end
