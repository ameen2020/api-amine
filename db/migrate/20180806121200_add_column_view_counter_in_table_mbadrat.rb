class AddColumnViewCounterInTableMbadrat < ActiveRecord::Migration[5.1]
  def change
    add_column :mbadrats,  :views_counter, :integer, default: 0, null: false
  end
end
