class CreateOfferFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :offer_feedbacks do |t|
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :offer, foreign_key: { on_delete: :cascade }, null: false
      t.integer :rating, null: false
      t.text    :comment

      t.timestamps

      t.index [:employee_id, :offer_id], unique: true
    end
  end
end
