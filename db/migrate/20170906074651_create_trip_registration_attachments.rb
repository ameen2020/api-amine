class CreateTripRegistrationAttachments < ActiveRecord::Migration[5.1]
  def change
    create_table :trip_registration_attachments do |t|
      t.belongs_to :trip_registration, foreign_key: { on_delete: :cascade }, null: false
      t.attachment :attachment
      t.timestamps
    end
    remove_attachment :trip_registrations, :attachment
  end
end
