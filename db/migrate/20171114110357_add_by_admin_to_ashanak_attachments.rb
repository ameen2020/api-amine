class AddByAdminToAshanakAttachments < ActiveRecord::Migration[5.1]
  def change
    add_column :ashanak_attachements, :by_admin, :boolean, null: false, default: false
  end
end
