class CreateOfferLocations < ActiveRecord::Migration[5.1]
  def up
    create_table :offer_locations do |t|
      t.belongs_to :offer, foreign_key: { on_delete: :cascade }, null: false
      t.decimal  :latitude,  precision: 10, scale: 8, default: '0.0', null: false
      t.decimal  :longitude, precision: 11, scale: 8, default: '0.0', null: false

      t.timestamps
    end

    # Separate the coordinates of each view and link each view to all its coordinates
    offers = Offer.all
    offers.each do |offer|
      offer['locations'].each do |l|
        offer.offer_locations.create!(latitude: l['latitude'], longitude: l['longitude'])
      end
    end

    # Delete Column in Table Offer
    remove_column :offers, :locations
  end

  def down
    # DROP Table offer locations
    drop_table :offer_locations
    # ADD Column in Table Offer
    add_column :offers, :locations, :json, null: false, default: []
  end
end
