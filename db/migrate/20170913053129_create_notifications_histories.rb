class CreateNotificationsHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications_histories do |t|
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.integer :notifiable_id,  null: false
      t.string :notifiable_type, null: false
      t.boolean :read, default: false
      t.timestamps
    end
  end
end
