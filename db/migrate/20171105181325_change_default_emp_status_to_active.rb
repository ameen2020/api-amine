class ChangeDefaultEmpStatusToActive < ActiveRecord::Migration[5.1]
  def up
    change_column :employees, :status, :integer, default: 1
  end

  def down
    change_column :employees, :status, :integer, default: 0
  end
end
