class AddColumnsInNotificationSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications_settings, :chalet, :boolean, default:false, null: false


    add_index :notifications_settings, :chalet
  end
end
