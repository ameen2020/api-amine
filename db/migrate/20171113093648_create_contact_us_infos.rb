class CreateContactUsInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_us_infos do |t|
      t.string :option
      t.string :value
      t.timestamps
    end
  end
end
