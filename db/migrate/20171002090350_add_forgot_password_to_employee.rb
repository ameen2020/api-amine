class AddForgotPasswordToEmployee < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :reset_password_token,   :string
    add_column :employees, :reset_password_sent_at, :timestamp
    add_index  :employees, :reset_password_token, unique: true, where: 'reset_password_token is not null'
  end
end
