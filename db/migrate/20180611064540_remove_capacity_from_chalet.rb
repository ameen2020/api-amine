class RemoveCapacityFromChalet < ActiveRecord::Migration[5.1]
  def change
    remove_column :chalets, :capacity
  end
end
