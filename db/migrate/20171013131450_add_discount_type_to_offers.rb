class AddDiscountTypeToOffers < ActiveRecord::Migration[5.1]
  def change
    add_column :offers, :discount_type, :string
    change_column :offers, :discount, :integer
  end
end
