class CreateAssignments < ActiveRecord::Migration[5.1]
  def change
    create_table :assignments do |t|
      t.jsonb  :title_translations, null: false
      t.jsonb  :description_translations, null: false
      t.string :email
      t.date   :date
      t.timestamps
    end
  end
end
