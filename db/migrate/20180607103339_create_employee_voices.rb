class CreateEmployeeVoices < ActiveRecord::Migration[5.1]
  def change
    create_table :employee_voices do |t|
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.integer    :voice_category, null:false, default: 0
      t.string     :phone_number, null:false
      t.string     :message, null:false
      t.attachment :attachment

      t.timestamps
    end
    Category.create({title_translations: { ar: "صوت الموظف", en: "Employee Voices" }, description_translations: { ar: "Employee voices details and description", en: "Employee voices details description" }, category_constant: "EMPLOYEEVOICE", position: 10, category_color: Faker::Color.hex_color})
  end
end
