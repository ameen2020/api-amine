class AddLocaleToTables < ActiveRecord::Migration[5.1]
  def change
    %i[ashanaks event_registrations trip_registrations].each do |table|
      add_column table.to_sym, :locale, :string, null: false, default: 'en'
    end
  end
end
