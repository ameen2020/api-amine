class CreateInstallmentAttachments < ActiveRecord::Migration[5.1]
  def change
    create_table :installment_attachments do |t|
      t.belongs_to :installment, foreign_key: { on_delete: :cascade }, null: false
      t.attachment :attachment
      t.timestamps
    end
  end
end
