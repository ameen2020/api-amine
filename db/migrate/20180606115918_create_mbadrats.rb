class CreateMbadrats < ActiveRecord::Migration[5.1]
  def change
    create_table :mbadrats do |t|
      t.jsonb      :title_translations, null: false
      t.jsonb      :description_translations, null: false
      t.string     :video
      t.attachment :attachment
      t.timestamps
    end
    Category.create({title_translations: { ar: "مبادرات", en: "Mbadrats" }, description_translations: { ar: "Mbadrats details and description", en: "Mbadrats details description" }, category_constant: "MBADRAT", position: 9, category_color: Faker::Color.hex_color})
  end
end
