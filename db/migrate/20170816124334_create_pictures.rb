class CreatePictures < ActiveRecord::Migration[5.1]
  def change
    create_table :pictures do |t|
      t.integer :imageable_id,  null: false
      t.string :imageable_type, null: false
      t.boolean :cover,         null: false, default: false
      t.attachment :attachment

      t.timestamps
      t.index :imageable_type
      t.index [:imageable_id, :imageable_type]
      t.index [:imageable_id, :imageable_type, :cover], unique: true, where: 'cover = true'
    end
  end
end
