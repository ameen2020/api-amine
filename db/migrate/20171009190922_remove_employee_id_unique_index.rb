class RemoveEmployeeIdUniqueIndex < ActiveRecord::Migration[5.1]
  def change
    change_column_null :employees, :employee_id, true
    remove_index :employees, name: 'index_employees_on_employee_id'
  end
end
