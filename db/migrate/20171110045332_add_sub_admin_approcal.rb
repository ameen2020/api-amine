class AddSubAdminApprocal < ActiveRecord::Migration[5.1]
  def change
    remove_column :ashanaks, :status, :integer, default: 0, null: false
    add_column :ashanaks, :super_admin_approve, :integer, default: 0, null: false
    add_column :ashanaks, :sub_admin_approve, :integer, default: 0, null: false
    add_column :ashanaks, :super_admin_comment, :text
    add_column :ashanaks, :sub_admin_comment, :text
    add_column :offers, :featured, :boolean, default: false, null: false
    add_attachment :sub_categories, :logo
  end
end
