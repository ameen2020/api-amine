class CreateAdminNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :admin_notifications do |t|
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.integer    :noticeable_id
      t.string     :noticeable_type
      t.integer    :status, null: false, default: 0
      t.timestamps

      t.index %i[noticeable_type noticeable_id]
      t.index :status
    end
  end
end
