class AddColumnToChaletsRegistration < ActiveRecord::Migration[5.1]
  def change
    add_column :chalet_registrations, :super_admin_approve, :integer, default: 0, null: false
    add_column :chalet_registrations, :sub_admin_approve, :integer, default:0, null: false
    add_column :chalet_registrations, :super_admin_comment, :text
    add_column :chalet_registrations, :sub_admin_comment,   :text
  end
end
