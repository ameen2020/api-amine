class CreateContactUs < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_us do |t|
      t.belongs_to :employee,     foreign_key: { on_delete: :cascade }, null: false
      t.string     :phone_number, null: false
      t.text       :message,      null: false
      t.timestamps

      t.index :phone_number
    end
  end
end
