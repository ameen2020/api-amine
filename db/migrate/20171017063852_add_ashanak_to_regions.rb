class AddAshanakToRegions < ActiveRecord::Migration[5.1]
  def change
    add_column :regions, :ashanak, :boolean, default: true, null: false
  end
end
