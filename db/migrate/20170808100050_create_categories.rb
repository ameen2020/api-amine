class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.jsonb   :title_translations, null: false
      t.jsonb   :description_translations
      t.string  :logo
      t.integer :status, null: false, default: 1
      t.integer :category_type, null: false, default: 0
      t.string  :category_constant
      t.integer :position
      t.string  :category_color
      t.timestamps

      t.index :status
      t.index :position
      t.index :category_constant, unique: true, where: 'category_constant IS NOT NULL'
    end
  end
end
