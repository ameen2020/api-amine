class CreateTripBlackLists < ActiveRecord::Migration[5.1]
  def change
    create_table :trip_black_lists do |t|
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :blocker,  foreign_key: { to_table: :employees, on_delete: :cascade }, null: false
      t.timestamps

      t.index :employee_id, unique: true, name: 'uniq_index_trip_black_lists_on_employee_id'
    end
  end
end
