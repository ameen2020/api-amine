class CreateWorkLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :work_locations do |t|
      t.belongs_to :region, foreign_key: { on_delete: :cascade }, null: false
      t.jsonb      :title_translations, null: false

      t.timestamps
    end
  end
end
