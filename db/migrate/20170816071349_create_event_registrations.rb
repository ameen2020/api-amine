class CreateEventRegistrations < ActiveRecord::Migration[5.1]
  def change
    create_table :event_registrations do |t|
      t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
      t.belongs_to :event,    foreign_key: { on_delete: :cascade }, null: false

      t.timestamps

      t.index [:employee_id, :event_id], unique: true
    end
  end
end
