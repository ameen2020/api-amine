class CreateChaletRegistrations < ActiveRecord::Migration[5.1]
  def change
    create_table :chalet_registrations do |t|
        t.belongs_to :employee, foreign_key: { on_delete: :cascade }, null: false
        t.belongs_to :chalet,    foreign_key: { on_delete: :cascade }, null: false
        t.string     :register_code
        t.attachment :attachment
        t.string     :locale , default: 'en' ,null: false
        t.timestamps

        t.index [:employee_id, :chalet_id], unique: true
    end
  end
end
