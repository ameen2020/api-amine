class CreateNationalities < ActiveRecord::Migration[5.1]
  def change
    create_table :nationalities do |t|
      t.jsonb   :name_translations, null: false
      t.integer :status, null:false, default:1
      t.timestamps
    end
  end
end
