class ChangeAttributesDataType < ActiveRecord::Migration[5.1]
  def change
    remove_column :trip_registrations, :super_admin_approve, :boolean, default: false, null: false
    remove_column :trip_registrations, :sub_admin_approve, :boolean, default: false, null: false
    add_column  :trip_registrations, :super_admin_approve, :integer, default: 0, null: false
    add_column  :trip_registrations, :sub_admin_approve, :integer, default: 0, null: false
  end
end
