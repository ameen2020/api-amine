class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.jsonb    :title_translations, null: false
      t.jsonb    :description_translations, null: false
      t.jsonb    :address_translations, null: false
      t.jsonb    :city_translations, null: false
      t.jsonb    :location_translations, null: false
      t.datetime :start_date, null: false
      t.datetime :end_date
      t.integer  :capacity
      t.decimal  :latitude, precision: 10, scale: 8, default: "0.0", null: false
      t.decimal  :longitude, precision: 11, scale: 8, default: "0.0", null: false

      t.timestamps
      t.index :start_date
    end
  end
end
