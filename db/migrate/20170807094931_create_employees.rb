class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string    :employee_id,    null: false
      t.string    :first_name,     null: false
      t.string    :last_name,      null: false, default: '-'
      t.string    :email
      t.string    :password_digest
      t.integer   :role,           null: false, default: 1 # default user
      t.integer   :status,         null: false, default: 0 # default inactive
      # Otp fields
      t.integer   :otp
      t.timestamp :otp_expire_on
      # Register status fields
      t.string    :register_token
      t.timestamp :register_token_expire_on
      t.string    :new_phone_number
      t.string    :phone_number
      t.boolean   :otp_verified,   null: false, default: false

      t.timestamps

      t.index :employee_id,  unique: true
      t.index :status
      t.index :role
      t.index :otp,          unique: true, where: 'otp IS NOT NULL'
      t.index :email,        unique: true, where: 'email IS NOT NULL'
      t.index :phone_number, unique: true, where: 'phone_number IS NOT NULL'
      t.index [:employee_id, :register_token], unique: true, where: 'register_token IS NOT NULL'
    end
  end
end
