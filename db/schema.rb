# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180809115255) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_notifications", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.integer "noticeable_id"
    t.string "noticeable_type"
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_admin_notifications_on_employee_id"
    t.index ["noticeable_type", "noticeable_id"], name: "index_admin_notifications_on_noticeable_type_and_noticeable_id"
    t.index ["status"], name: "index_admin_notifications_on_status"
  end

  create_table "ashanak_attachements", force: :cascade do |t|
    t.bigint "ashanak_id", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "by_admin", default: false, null: false
    t.index ["ashanak_id"], name: "index_ashanak_attachements_on_ashanak_id"
  end

  create_table "ashanaks", force: :cascade do |t|
    t.bigint "region_id", null: false
    t.bigint "occasion_id", null: false
    t.bigint "employee_id", null: false
    t.bigint "work_location_id", null: false
    t.date "date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "phone_number", default: "", null: false
    t.text "address", default: "", null: false
    t.integer "super_admin_approve", default: 0, null: false
    t.integer "sub_admin_approve", default: 0, null: false
    t.text "super_admin_comment"
    t.text "sub_admin_comment"
    t.string "locale", default: "en", null: false
    t.index ["employee_id"], name: "index_ashanaks_on_employee_id"
    t.index ["occasion_id"], name: "index_ashanaks_on_occasion_id"
    t.index ["region_id"], name: "index_ashanaks_on_region_id"
    t.index ["work_location_id"], name: "index_ashanaks_on_work_location_id"
  end

  create_table "assignments", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.string "email"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "views_counter", default: 0, null: false
  end

  create_table "categories", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations"
    t.integer "status", default: 1, null: false
    t.string "category_constant", null: false
    t.integer "position"
    t.string "category_color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.index ["category_constant"], name: "index_categories_on_category_constant", unique: true, where: "(category_constant IS NOT NULL)"
    t.index ["position"], name: "index_categories_on_position"
    t.index ["status"], name: "index_categories_on_status"
  end

  create_table "chalet_registrations", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.bigint "chalet_id", null: false
    t.string "register_code"
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string "locale", default: "en", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "super_admin_approve", default: 0, null: false
    t.integer "sub_admin_approve", default: 0, null: false
    t.text "super_admin_comment"
    t.text "sub_admin_comment"
    t.index ["chalet_id"], name: "index_chalet_registrations_on_chalet_id"
    t.index ["employee_id", "chalet_id"], name: "index_chalet_registrations_on_employee_id_and_chalet_id", unique: true
    t.index ["employee_id"], name: "index_chalet_registrations_on_employee_id"
  end

  create_table "chalets", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.jsonb "address_translations", null: false
    t.jsonb "city_translations", null: false
    t.jsonb "location_translations", null: false
    t.datetime "start_date", null: false
    t.datetime "end_date"
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.decimal "latitude", precision: 10, scale: 8, default: "0.0", null: false
    t.decimal "longitude", precision: 11, scale: 8, default: "0.0", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_cancel"
    t.integer "views_counter", default: 0, null: false
  end

  create_table "chalets_regions", id: false, force: :cascade do |t|
    t.bigint "chalet_id", null: false
    t.bigint "region_id", null: false
    t.index ["chalet_id"], name: "index_chalets_regions_on_chalet_id"
    t.index ["region_id", "chalet_id"], name: "index_chalets_regions_on_region_id_and_chalet_id", unique: true
    t.index ["region_id"], name: "index_chalets_regions_on_region_id"
  end

  create_table "contact_us", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.string "phone_number", null: false
    t.text "message", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "admin_id"
    t.text "reply_message"
    t.datetime "replied_at"
    t.string "locale", default: "en", null: false
    t.index ["admin_id"], name: "index_contact_us_on_admin_id"
    t.index ["employee_id"], name: "index_contact_us_on_employee_id"
    t.index ["phone_number"], name: "index_contact_us_on_phone_number"
  end

  create_table "contact_us_infos", force: :cascade do |t|
    t.string "option"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employee_voices", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.integer "voice_category", default: 0, null: false
    t.string "phone_number", null: false
    t.string "message", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "admin_id"
    t.text "reply_message"
    t.datetime "replied_at"
    t.string "locale", default: "en", null: false
    t.string "attachment_admin_file_name"
    t.string "attachment_admin_content_type"
    t.integer "attachment_admin_file_size"
    t.datetime "attachment_admin_updated_at"
    t.index ["admin_id"], name: "index_employee_voices_on_admin_id"
    t.index ["employee_id"], name: "index_employee_voices_on_employee_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "employee_id"
    t.string "first_name", null: false
    t.string "last_name", default: "-", null: false
    t.string "email"
    t.string "password_digest"
    t.integer "role", default: 1, null: false
    t.integer "status", default: 1, null: false
    t.integer "otp"
    t.datetime "otp_expire_on"
    t.string "register_token"
    t.datetime "register_token_expire_on"
    t.string "new_phone_number"
    t.string "phone_number"
    t.boolean "otp_verified", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.string "username", null: false
    t.index ["email"], name: "index_employees_on_email", unique: true, where: "(email IS NOT NULL)"
    t.index ["employee_id", "register_token"], name: "index_employees_on_employee_id_and_register_token", unique: true, where: "(register_token IS NOT NULL)"
    t.index ["otp"], name: "index_employees_on_otp", unique: true, where: "(otp IS NOT NULL)"
    t.index ["phone_number"], name: "index_employees_on_phone_number", unique: true, where: "(phone_number IS NOT NULL)"
    t.index ["reset_password_token"], name: "index_employees_on_reset_password_token", unique: true, where: "(reset_password_token IS NOT NULL)"
    t.index ["role"], name: "index_employees_on_role"
    t.index ["status"], name: "index_employees_on_status"
    t.index ["username"], name: "index_employees_on_username", unique: true, where: "(username IS NOT NULL)"
  end

  create_table "event_registrations", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.bigint "event_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "register_code", null: false
    t.string "locale", default: "en", null: false
    t.index ["employee_id", "event_id"], name: "index_event_registrations_on_employee_id_and_event_id", unique: true
    t.index ["employee_id"], name: "index_event_registrations_on_employee_id"
    t.index ["event_id"], name: "index_event_registrations_on_event_id"
    t.index ["register_code"], name: "index_event_registrations_on_register_code", unique: true
  end

  create_table "events", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.jsonb "address_translations", null: false
    t.jsonb "city_translations", null: false
    t.jsonb "location_translations", null: false
    t.datetime "start_date", null: false
    t.datetime "end_date"
    t.integer "capacity"
    t.decimal "latitude", precision: 10, scale: 8, default: "0.0", null: false
    t.decimal "longitude", precision: 11, scale: 8, default: "0.0", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_cancel"
    t.json "locations", default: [], null: false
    t.integer "views_counter", default: 0, null: false
    t.index ["start_date"], name: "index_events_on_start_date"
  end

  create_table "events_regions", id: false, force: :cascade do |t|
    t.bigint "event_id", null: false
    t.bigint "region_id", null: false
    t.index ["event_id"], name: "index_events_regions_on_event_id"
    t.index ["region_id", "event_id"], name: "index_events_regions_on_region_id_and_event_id", unique: true
    t.index ["region_id"], name: "index_events_regions_on_region_id"
  end

  create_table "feedback_categories", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "installment_attachments", force: :cascade do |t|
    t.bigint "installment_id", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["installment_id"], name: "index_installment_attachments_on_installment_id"
  end

  create_table "installments", force: :cascade do |t|
    t.bigint "nationality_id", null: false
    t.bigint "employee_id", null: false
    t.string "company_name", null: false
    t.string "locale", default: "en", null: false
    t.integer "super_admin_approve", default: 0, null: false
    t.integer "sub_admin_approve", default: 0, null: false
    t.text "super_admin_comment"
    t.text "sub_admin_comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_installments_on_employee_id"
    t.index ["nationality_id"], name: "index_installments_on_nationality_id"
  end

  create_table "magazins", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "views_counter", default: 0, null: false
  end

  create_table "mbadrat_faqs", force: :cascade do |t|
    t.bigint "mbadrat_id", null: false
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mbadrat_id"], name: "index_mbadrat_faqs_on_mbadrat_id"
  end

  create_table "mbadrats", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.string "video"
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "views_counter", default: 0, null: false
  end

  create_table "memos", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "views_counter", default: 0, null: false
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "sender_id", null: false
    t.string "title", null: false
    t.string "message", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sender_id"], name: "index_messages_on_sender_id"
  end

  create_table "nationalities", force: :cascade do |t|
    t.jsonb "name_translations", null: false
    t.integer "status", default: 1, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "news", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.date "date"
    t.boolean "important", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "views_counter", default: 0, null: false
    t.index ["date"], name: "index_news_on_date"
  end

  create_table "notifications_histories", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.integer "notifiable_id", null: false
    t.string "notifiable_type", null: false
    t.boolean "read", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_notifications_histories_on_employee_id"
  end

  create_table "notifications_settings", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.boolean "event", default: false, null: false
    t.boolean "offer", default: false, null: false
    t.boolean "trip", default: false, null: false
    t.boolean "news", default: false, null: false
    t.boolean "memo", default: false, null: false
    t.boolean "job", default: false, null: false
    t.boolean "magazine", default: false, null: false
    t.integer "region", default: [], array: true
    t.integer "sub_category", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "chalet", default: false, null: false
    t.boolean "mbadrat", default: false, null: false
    t.boolean "employee_voice", default: false, null: false
    t.boolean "installment", default: false, null: false
    t.index ["chalet"], name: "index_notifications_settings_on_chalet"
    t.index ["employee_id"], name: "index_notifications_settings_on_employee_id"
    t.index ["employee_voice"], name: "index_notifications_settings_on_employee_voice"
    t.index ["event"], name: "index_notifications_settings_on_event"
    t.index ["installment"], name: "index_notifications_settings_on_installment"
    t.index ["job"], name: "index_notifications_settings_on_job"
    t.index ["magazine"], name: "index_notifications_settings_on_magazine"
    t.index ["mbadrat"], name: "index_notifications_settings_on_mbadrat"
    t.index ["memo"], name: "index_notifications_settings_on_memo"
    t.index ["news"], name: "index_notifications_settings_on_news"
    t.index ["offer"], name: "index_notifications_settings_on_offer"
    t.index ["region"], name: "index_notifications_settings_on_region"
    t.index ["sub_category"], name: "index_notifications_settings_on_sub_category"
    t.index ["trip"], name: "index_notifications_settings_on_trip"
  end

  create_table "occasions", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offer_attachments", force: :cascade do |t|
    t.bigint "offer_id", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["offer_id"], name: "index_offer_attachments_on_offer_id"
  end

  create_table "offer_feedbacks", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.bigint "offer_id", null: false
    t.integer "rating", null: false
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.index ["category_id"], name: "index_offer_feedbacks_on_category_id"
    t.index ["employee_id", "offer_id"], name: "index_offer_feedbacks_on_employee_id_and_offer_id", unique: true
    t.index ["employee_id"], name: "index_offer_feedbacks_on_employee_id"
    t.index ["offer_id"], name: "index_offer_feedbacks_on_offer_id"
  end

  create_table "offer_locations", force: :cascade do |t|
    t.bigint "offer_id", null: false
    t.decimal "latitude", precision: 10, scale: 8, default: "0.0", null: false
    t.decimal "longitude", precision: 11, scale: 8, default: "0.0", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["offer_id"], name: "index_offer_locations_on_offer_id"
  end

  create_table "offers", force: :cascade do |t|
    t.bigint "sub_category_id", null: false
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.datetime "expire_on", null: false
    t.integer "discount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "discount_type"
    t.boolean "featured", default: false, null: false
    t.string "email"
    t.string "phone_number"
    t.integer "views_counter", default: 0, null: false
    t.index ["sub_category_id"], name: "index_offers_on_sub_category_id"
  end

  create_table "offers_regions", id: false, force: :cascade do |t|
    t.bigint "offer_id", null: false
    t.bigint "region_id", null: false
    t.index ["offer_id"], name: "index_offers_regions_on_offer_id"
    t.index ["region_id", "offer_id"], name: "index_offers_regions_on_region_id_and_offer_id", unique: true
    t.index ["region_id"], name: "index_offers_regions_on_region_id"
  end

  create_table "pages", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "content_translations", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pictures", force: :cascade do |t|
    t.integer "imageable_id", null: false
    t.string "imageable_type", null: false
    t.boolean "cover", default: false, null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["imageable_id", "imageable_type", "cover"], name: "index_pictures_on_imageable_id_and_imageable_type_and_cover", unique: true, where: "(cover = true)"
    t.index ["imageable_id", "imageable_type"], name: "index_pictures_on_imageable_id_and_imageable_type"
    t.index ["imageable_type"], name: "index_pictures_on_imageable_type"
  end

  create_table "regions", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "ashanak", default: true, null: false
  end

  create_table "relationships", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.boolean "status", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sub_categories", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.integer "status", default: 1, null: false
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "trip_black_lists", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.bigint "blocker_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blocker_id"], name: "index_trip_black_lists_on_blocker_id"
    t.index ["employee_id"], name: "index_trip_black_lists_on_employee_id"
    t.index ["employee_id"], name: "uniq_index_trip_black_lists_on_employee_id", unique: true
  end

  create_table "trip_registration_attachments", force: :cascade do |t|
    t.bigint "trip_registration_id", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["trip_registration_id"], name: "index_trip_registration_attachments_on_trip_registration_id"
  end

  create_table "trip_registrations", force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.bigint "trip_id", null: false
    t.bigint "relationship_id"
    t.string "full_name"
    t.integer "id_number"
    t.boolean "companion", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "register_code", null: false
    t.integer "super_admin_approve", default: 0, null: false
    t.integer "sub_admin_approve", default: 0, null: false
    t.text "super_admin_comment"
    t.text "sub_admin_comment"
    t.string "locale", default: "en", null: false
    t.index ["employee_id", "trip_id"], name: "index_trip_registrations_on_employee_id_and_trip_id", unique: true
    t.index ["employee_id"], name: "index_trip_registrations_on_employee_id"
    t.index ["register_code"], name: "index_trip_registrations_on_register_code", unique: true
    t.index ["relationship_id"], name: "index_trip_registrations_on_relationship_id"
    t.index ["trip_id"], name: "index_trip_registrations_on_trip_id"
  end

  create_table "trips", force: :cascade do |t|
    t.jsonb "title_translations", null: false
    t.jsonb "description_translations", null: false
    t.jsonb "location_translations", null: false
    t.datetime "start_date", null: false
    t.datetime "end_date"
    t.datetime "last_register_date", null: false
    t.datetime "last_cancel_date"
    t.integer "capacity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "views_counter", default: 0, null: false
    t.index ["start_date"], name: "index_trips_on_start_date"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.integer "whodunnit"
    t.json "object"
    t.datetime "created_at"
    t.json "object_changes"
    t.index ["item_type", "item_id", "whodunnit"], name: "index_versions_on_item_type_and_item_id_and_whodunnit"
  end

  create_table "work_locations", force: :cascade do |t|
    t.bigint "region_id", null: false
    t.jsonb "title_translations", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_work_locations_on_region_id"
  end

  add_foreign_key "admin_notifications", "employees", on_delete: :cascade
  add_foreign_key "ashanak_attachements", "ashanaks", on_delete: :cascade
  add_foreign_key "ashanaks", "employees", on_delete: :cascade
  add_foreign_key "ashanaks", "occasions", on_delete: :cascade
  add_foreign_key "ashanaks", "regions", on_delete: :cascade
  add_foreign_key "ashanaks", "work_locations", on_delete: :cascade
  add_foreign_key "chalet_registrations", "chalets", on_delete: :cascade
  add_foreign_key "chalet_registrations", "employees", on_delete: :cascade
  add_foreign_key "chalets_regions", "chalets", on_delete: :cascade
  add_foreign_key "chalets_regions", "regions", on_delete: :cascade
  add_foreign_key "contact_us", "employees", column: "admin_id", on_delete: :cascade
  add_foreign_key "contact_us", "employees", on_delete: :cascade
  add_foreign_key "employee_voices", "employees", column: "admin_id", on_delete: :cascade
  add_foreign_key "employee_voices", "employees", on_delete: :cascade
  add_foreign_key "event_registrations", "employees", on_delete: :cascade
  add_foreign_key "event_registrations", "events", on_delete: :cascade
  add_foreign_key "events_regions", "events", on_delete: :cascade
  add_foreign_key "events_regions", "regions", on_delete: :cascade
  add_foreign_key "installment_attachments", "installments", on_delete: :cascade
  add_foreign_key "installments", "employees", on_delete: :cascade
  add_foreign_key "installments", "nationalities", on_delete: :cascade
  add_foreign_key "mbadrat_faqs", "mbadrats", on_delete: :cascade
  add_foreign_key "messages", "employees", column: "sender_id", on_delete: :cascade
  add_foreign_key "notifications_histories", "employees", on_delete: :cascade
  add_foreign_key "notifications_settings", "employees", on_delete: :cascade
  add_foreign_key "offer_attachments", "offers", on_delete: :cascade
  add_foreign_key "offer_feedbacks", "employees", on_delete: :cascade
  add_foreign_key "offer_feedbacks", "feedback_categories", column: "category_id", on_delete: :cascade
  add_foreign_key "offer_feedbacks", "offers", on_delete: :cascade
  add_foreign_key "offer_locations", "offers", on_delete: :cascade
  add_foreign_key "offers", "sub_categories", on_delete: :cascade
  add_foreign_key "offers_regions", "offers", on_delete: :cascade
  add_foreign_key "offers_regions", "regions", on_delete: :cascade
  add_foreign_key "trip_black_lists", "employees", column: "blocker_id", on_delete: :cascade
  add_foreign_key "trip_black_lists", "employees", on_delete: :cascade
  add_foreign_key "trip_registration_attachments", "trip_registrations", on_delete: :cascade
  add_foreign_key "trip_registrations", "employees", on_delete: :cascade
  add_foreign_key "trip_registrations", "relationships", on_delete: :cascade
  add_foreign_key "trip_registrations", "trips", on_delete: :cascade
  add_foreign_key "work_locations", "regions", on_delete: :cascade
end
