# Add Admin
Employee.create(employee_id: -1, first_name: 'Super', last_name: 'Admin', email: 'admin@moh.gov.sa', password: 'password', role: 0, status: 1)
Employee.create(employee_id: -2, first_name: 'Sub', last_name: 'Admin', email: 'sub@moh.gov.sa', password: 'password', role: 0, status: 1)

Category.create({title_translations: { ar: "المناسبات", en: "Events" }, description_translations: { ar: "Events details and description", en: "Events details description" }, category_constant: "EVENTS", position: 1, category_color: Faker::Color.hex_color})
Category.create({title_translations: { ar: "عروض", en: "Offers" }, description_translations: { ar: "عروض خاصه وتخفيضات", en: "special Offers with discount " }, category_constant: "OFFERS", position: 0, category_color: Faker::Color.hex_color})
Category.create({title_translations: { ar: "رحلات", en: "Trips" }, description_translations: { ar: "trips description goes here..", en: "trips description goes here.." }, category_constant: "TRIPS", position: 2, category_color: Faker::Color.hex_color})
Category.create({title_translations: { ar: "أخبار", en: "News" }, description_translations: { ar: "News description here..", en: "News description here" }, category_constant: "NEWS", position: 3, category_color: Faker::Color.hex_color})
Category.create({title_translations: { ar: "مذكرات", en: "Memos" }, description_translations: { ar: "Memos description here..", en: "Memos description here.." }, category_constant: "MEMOS", position: 4, category_color: Faker::Color.hex_color})
Category.create({title_translations: { ar: "مهمات وأعمال", en: "Assignment Job" }, description_translations: { ar: "Assignment Job description goes here...", en: "Assignment Job description " }, category_constant: "JOBS", position: 5, category_color: Faker::Color.hex_color})
Category.create({title_translations: { ar: "المجلة الشهرية", en: "Monthly Magazine" }, description_translations: { ar: "Magazine description ..", en: "Magazine description" }, category_constant: "MAGAZINE", position: 6, category_color: Faker::Color.hex_color})
Category.create({title_translations: { ar: "عشانك", en: "Ashanak" }, description_translations: { ar: "وصف عشانك", en: "Ashanak Description" }, category_constant: "ASHANAK", position: 7, category_color: Faker::Color.hex_color})
