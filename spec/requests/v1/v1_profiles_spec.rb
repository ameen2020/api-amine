require 'rails_helper'

RSpec.describe 'V1::Profiles', type: :request do
  describe 'GET /v1/profiles' do
    before(:each) do
      @emp   = FactoryGirl.create(:employee, status: 1)
      @token = JsonWebToken.encode(@emp.login_payload)
    end

    it 'should reject unlogged in users' do
      get v1_profiles_path
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1001
    end

    it 'should show current active user profile' do
      get v1_profiles_path, headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(200)
      expect(data).to include('employee')
      expect(data['employee']['id']).to eq @emp.id
    end

    it 'should reject inactive users and give error code 1305 for inactive user' do
      @emp.update!(status: 2)
      get v1_profiles_path, headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(403)
      expect(json['error_code']).to eq 1305
    end

    it 'should reject pending users' do
      @emp.update!(status: 0)
      get v1_profiles_path, headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(403)
      expect(json['error_code']).to eq 1102
    end
  end

  describe 'POST /v1/profiles/change_phone_number' do
    before(:each) do
      @mobile   = '+9665123456789'
      @mobile_2 = '+9665987654321'
      FactoryGirl.create(:employee, status: 1, phone_number: @mobile)

      @emp    = FactoryGirl.create(:employee, status: 1, phone_number: @mobile_2)
      @token  = JsonWebToken.encode(@emp.login_payload)
    end

    it 'should require new_phone_number' do
      post change_phone_number_v1_profiles_path, headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1002
      expect(message).to include('new_phone_number')
    end

    it 'should validates and reject if the new number already used by another user' do
      post change_phone_number_v1_profiles_path(new_phone_number: @mobile), headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(422)
      expect(json['error_code']).to eq 1010
    end

    it 'should validates the format of the new number' do
      post change_phone_number_v1_profiles_path(new_phone_number: 'invalid-number'), headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(422)
      expect(json['error_code']).to eq 1010
    end

    it 'should accept a valid unique phone number and change the user new number' do
      new_phone_number = '+966852741963'
      post change_phone_number_v1_profiles_path(new_phone_number: new_phone_number), headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(200)
      expect(message).to eq I18n.t('user.otp.generated')
      emp = Employee.find(@emp.id)
      expect(emp.phone_number).to eq @mobile_2
      expect(emp.new_phone_number).to eq new_phone_number
    end

    it 'should send otp to new number'
  end

  describe 'POST /v1/profiles/confirm_otp' do
    before(:each) do
      @mobile = '+9665123456789'
      @emp    = FactoryGirl.create(:employee, status: 1, phone_number: @mobile)
      @token  = JsonWebToken.encode(@emp.login_payload)

      @new_phone_number = '+966852741963'
      post change_phone_number_v1_profiles_path(new_phone_number: @new_phone_number), headers: { 'Authorization' => "Bearer #{@token}" }
      @otp = data['otp']
    end

    it 'should require otp' do
      post confirm_otp_v1_profiles_path, headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1002
      expect(message).to include('otp')
    end

    it 'should reject invalid OTP' do
      post confirm_otp_v1_profiles_path(otp: 'xxxx'), headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1307
    end

    it 'should reject expired OTP' do
      time_later = Time.zone.now + 5.minutes
      allow(Time.zone).to receive(:now).and_return(time_later)
      post confirm_otp_v1_profiles_path(otp: @otp), headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1307
    end

    it 'should change mobile number on success request' do
      post confirm_otp_v1_profiles_path(otp: @otp), headers: { 'Authorization' => "Bearer #{@token}" }
      expect(response).to have_http_status(200)
      emp = Employee.find(@emp.id)
      expect(emp.phone_number).to eq @new_phone_number
      expect(emp.new_phone_number).to eq nil
    end
  end
end
