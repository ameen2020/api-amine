require 'rails_helper'

RSpec.describe "V1::Categories", type: :request do
  describe "Test Access Roles" do
    before(:all) do
      FactoryGirl.create(:category)
      @admin         = FactoryGirl.create(:employee, status: 1, role: 0)
      @emp           = FactoryGirl.create(:employee, status: 1)
      @admin_headers = { 'Authorization' => "Bearer #{JsonWebToken.encode(@admin.login_payload)}" }
      @emp_headers   = { 'Authorization' => "Bearer #{JsonWebToken.encode(@emp.login_payload)}" }
      @params        = {title_translations: {en:"x", ar: "b"}, category_constant: SecureRandom.hex(2).upcase}
    end

    describe "GET /v1/categories" do
      it 'should allow users' do
        get v1_categories_path, headers: @emp_headers
        expect(response).to have_http_status(200)
      end
      it 'should allow admins' do
        get v1_categories_path, headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end

    describe "POST /v1/categories" do
      it 'should REJECT users' do
        post v1_categories_path(@params), headers: @emp_headers
        expect(response).to have_http_status(403)
      end
      it 'should allow admins' do
        post v1_categories_path(@params), headers: @admin_headers
        expect(response).to have_http_status(201)
      end
    end

    describe "GET /v1/categories/:id" do
      it 'should allow users' do
        get v1_category_path(1), headers: @emp_headers
        expect(response).to have_http_status(200)
      end
      it 'should allow admins' do
        get v1_category_path(1), headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end

    describe "PUT /v1/categories/:id" do
      it 'should REJECT users' do
        put v1_category_path(1, @params), headers: @emp_headers
        # expect(response).to have_http_status(403)
        expect(response).to have_http_status(404)
      end
      it 'should allow admins' do
        put v1_category_path(1, @params), headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end

    describe "DELETE /v1/categories/:id" do
      it 'should REJECT users' do
        delete v1_category_path(1), headers: @emp_headers
        # expect(response).to have_http_status(403)
        expect(response).to have_http_status(404)
      end
      it 'should allow admins' do
        delete v1_category_path(1), headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /v1/categories" do
  end
end
