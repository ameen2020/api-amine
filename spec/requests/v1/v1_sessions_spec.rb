require 'rails_helper'
require 'net/http'

RSpec.describe "V1::Sessions", type: :request do
  describe 'POST /v1/sessions' do
    before(:each) do
      @emp = FactoryGirl.create(:employee, role: 0, password: 'password', status: 1)
    end

    it 'should require email, password' do
      post v1_sessions_path
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1002
      expect(message).to include('email')
      expect(message).to include('password')
    end

    it 'should allow only active admin to login' do
      post v1_sessions_path(email: @emp.email, password: 'password')
      expect(response).to have_http_status(200)
      expect(data).to include('employee')
      expect(data['employee']['id']).to eq @emp.id
      expect(data).to include('token')
    end

    it 'should reject non-admin from login' do
      @emp.employee!
      post v1_sessions_path(email: @emp.email, password: 'password')
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1308
    end

    it 'should reject inactive admins from login' do
      @emp.admin!
      @emp.inactive!
      post v1_sessions_path(email: @emp.email, password: 'password')
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1308
    end
  end

  describe "POST /v1/sessions/register" do
    before(:each) do
      @endpint = URI(Rails.application.secrets.external_emp_api)
      @success_response = "{\"success\":true,\"message\":\"Login successfully\",\"data\":{\"user\":{\"id\":123,\"username\":\"username\",\"first_name\":\"FName\",\"last_name\":\"LName\",\"email\":\"example@live.com\",\"mobile_number\":null}}}"
      @params = {username: 'username', password: 'password'}
    end

    it "should return success with employee_id + register_token in data if success login" do
      stub_request(:post, @endpint).to_return(body: @success_response, status: 200)

      post register_v1_sessions_path(@params)

      expect(response).to have_http_status(200)
      expect(data).to include('employee')
      expect(data['employee']).to include('employee_id')
      expect(data).to include('register_token')
    end

    it "should return bad request with error 1301 for Invalid Credentials" do
      stub_request(:post, @endpint).to_return(body: "", status: 400)

      post register_v1_sessions_path(@params)
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1301
    end

    it "should allow pending users to hit register again" do
      stub_request(:post, @endpint).to_return(body: @success_response, status: 200)

      post register_v1_sessions_path(@params) # first success request
      post register_v1_sessions_path(@params) # second success request
      expect(response).to have_http_status(200)
      expect(data).to include('employee')
      expect(data['employee']).to include('employee_id')
      expect(data).to include('register_token')
    end

    it "should return bad request with error 1309 for Existing Active Users" do
      stub_request(:post, @endpint).to_return(body: @success_response, status: 200)

      post register_v1_sessions_path(@params) # first success request
      Employee.find(data['employee']['id']).active! # activate the user

      post register_v1_sessions_path(@params) # second success request
      expect(response).to have_http_status(401)
      expect(message).to eq I18n.t('errors.1309')
      expect(json['error_code']).to eq 1309
    end

    it 'should geenerate new request token in second try' do
      stub_request(:post, @endpint).to_return(body: @success_response, status: 200)

      post register_v1_sessions_path(@params) # first success request
      expect(response).to have_http_status(200)
      @first_token = data['register_token']

      post register_v1_sessions_path(@params) # second success request
      expect(response).to have_http_status(200)
      expect(data['register_token']).not_to eq @first_token
    end
  end

  describe 'POST /v1/sessions/set_phone_number' do
    before(:each) do
      @emp = FactoryGirl.create(:employee)
    end

    it 'should require employee_id, phone_number, token' do
      post set_phone_number_v1_sessions_path
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1002
      expect(message).to include('employee_id')
      expect(message).to include('phone_number')
      expect(message).to include('token')
    end

    it 'should validate and send OTP if the request is valid' do
      post set_phone_number_v1_sessions_path({employee_id: @emp.employee_id, token: @emp.register_token, phone_number: '+12345678912'})
      expect(response).to have_http_status(200)
      expect(message).to include('successfully')
      expect(Employee.find_by(employee_id: @emp.employee_id).otp.present?).to be true
    end

    it 'should reject and render error if register token is over or employee not found' do
      time_later = Time.zone.now + 5.minutes
      allow(Time.zone).to receive(:now).and_return(time_later)
      post set_phone_number_v1_sessions_path({employee_id: @emp.employee_id, token: @emp.register_token, phone_number: '+12345678912'})
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1310
    end

    it 'should reject and render error if mobile number is invalid format' do
      post set_phone_number_v1_sessions_path({employee_id: @emp.employee_id, token: @emp.register_token, phone_number: 'invalid-number'})
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1010
      expect(message).to include(I18n.t('activerecord.errors.messages.invalid_mobile_number'))
    end
  end

  describe 'POST /v1/sessions/otp' do
    before(:each) do
      @emp = FactoryGirl.create(:employee)
    end

    it "should return success and generate otp if phone_number exist active account not disabled" do
      post otp_v1_sessions_path({phone_number: @emp.phone_number})

      expect(response).to have_http_status(200)
      expect(Employee.find(@emp.id).otp.present?).to be true
    end

    it "should reject and return error if account is disabled" do
      @emp.inactive!
      post otp_v1_sessions_path({phone_number: @emp.phone_number})
      expect(response).to have_http_status(403)
      expect(json['error_code']).to eq 1305
    end

    it "should reject and return error code 1306 if phone_number not found" do
      post otp_v1_sessions_path({phone_number: "+9999999999"})
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1306
    end
  end

  describe 'POST /v1/sessions/confirm_otp' do
    before(:each) do
      @emp = FactoryGirl.create(:employee)
      @emp.generate_and_send_otp!
    end

    it 'should requires phone_number and otp params' do
      post confirm_otp_v1_sessions_path
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1002
      expect(message).to include('phone_number')
      expect(message).to include('otp')
    end

    it 'should return success with token if request is valid' do
      post confirm_otp_v1_sessions_path(phone_number: @emp.phone_number, otp: @emp.otp)
      expect(response).to have_http_status(200)
      expect(data).to include('employee')
      expect(data['employee']['id']).to eq @emp.id
      expect(data).to include('token')
    end

    it 'should reject and render error code 1307 if otp didnt match' do
      post confirm_otp_v1_sessions_path(phone_number: @emp.phone_number, otp: 'xxxx')
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1307
    end

    it 'should reject and render error code 1307 if otp expired' do
      time_later = Time.zone.now + 5.minutes
      allow(Time.zone).to receive(:now).and_return(time_later)
      post confirm_otp_v1_sessions_path(phone_number: @emp.phone_number, otp: @emp.otp)
      expect(response).to have_http_status(400)
      expect(json['error_code']).to eq 1307
    end

    it 'should reject and render error code 1306 if employee is dissabled' do
      @emp.inactive!
      post confirm_otp_v1_sessions_path(phone_number: @emp.phone_number, otp: @emp.otp)
      expect(response).to have_http_status(403)
      expect(json['error_code']).to eq 1305
    end
  end
end
