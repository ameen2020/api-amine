require 'rails_helper'

RSpec.describe 'V1::EventRegistrations', type: :request do
  describe 'Test Access Roles' do
    before(:all) do
      @event         = FactoryGirl.create(:event, start_date: Date.tomorrow, capacity: nil)
      @admin         = FactoryGirl.create(:employee, status: 1, role: 0)
      @emp           = FactoryGirl.create(:employee, status: 1)
      @admin_headers = { 'Authorization' => "Bearer #{JsonWebToken.encode(@admin.login_payload)}" }
      @emp_headers   = { 'Authorization' => "Bearer #{JsonWebToken.encode(@emp.login_payload)}" }
      # @params        = {}
    end

    describe 'GET /v1/events/:event_id/registration' do
      it 'should REJECT users' do
        get "/v1/events/#{@event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(403)
      end
      it 'should allow admins' do
        get "/v1/events/#{@event.id}/registration", headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end

    describe 'POST /v1/events/:event_id/registration' do
      it 'should allow users' do
        post "/v1/events/#{@event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(201)
      end
      it 'should REJECT admins' do
        post "/v1/events/#{@event.id}/registration", headers: @admin_headers
        expect(response).to have_http_status(403)
      end
    end

    describe 'DELETE /v1/events/:event_id/registration' do
      it 'should allow users' do
        EventRegistration.create(event_id: @event.id, employee_id: @emp.id)
        delete "/v1/events/#{@event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(200)
      end
      it 'should REJECT admins without employee_id' do
        EventRegistration.create(event_id: @event.id, employee_id: @emp.id)
        delete "/v1/events/#{@event.id}/registration", headers: @admin_headers
        expect(response).to have_http_status(400)
      end
      it 'should allow admins with employee_id' do
        EventRegistration.create(event_id: @event.id, employee_id: @emp.id)
        delete "/v1/events/#{@event.id}/registration", params: { employee_id: @emp.id }, headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'API Tests' do
    before(:each) do
      @emp           = FactoryGirl.create(:employee, status: 1)
      @emp_headers   = { 'Authorization' => "Bearer #{JsonWebToken.encode(@emp.login_payload)}" }
    end

    describe "POST /v1/events/:event_id/registration (As Employee)" do
      it 'should ALLOW register if event start_date > today and capacity is available and not registered yet' do
        event = FactoryGirl.create(:event, start_date: Date.tomorrow, capacity: 2)
        post "/v1/events/#{event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(201)
        expect(message).to eq I18n.t('event_registrations.register_success')
        expect(event.employees.pluck(:id)).to include @emp.id
      end

      it 'should ALLOW register if event start_date > today and capacity is unlimited and not registered yet' do
        event = FactoryGirl.create(:event, start_date: Date.tomorrow, capacity: nil)
        post "/v1/events/#{event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(201)
        expect(message).to eq I18n.t('event_registrations.register_success')
      end

      it 'should REJECT register if event start_date > today and capacity is full and not registered' do
        capacity = 2
        event = FactoryGirl.create(:event, capacity: capacity)
        capacity.times do FactoryGirl.build(:event_registration, event_id: event.id, employee_id: FactoryGirl.create(:employee).id).save(validate: false) end

        post "/v1/events/#{event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(422)
        expect(message).to include('cant register')
      end

      it 'should REJECT register if event start_date < today and not registered' do
        event = FactoryGirl.build(:event, capacity: nil, start_date: Date.yesterday, last_cancel: nil)
        event.save!(validate: false)
        post "/v1/events/#{event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(422)
        expect(message).to include('cant register')
      end

      it 'should REJECT registration if already registered' do
        event = FactoryGirl.create(:event, start_date: Date.tomorrow, capacity: nil)
        post "/v1/events/#{event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(201)

        post "/v1/events/#{event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(422)
        expect(message).to include "Already registered"
      end
    end

    describe "DELETE /v1/events/:event_id/registration (As Employee)" do
      it 'should ALLOW  cancel if registered and today < start_date with X available time' do
        event = FactoryGirl.create(:event, capacity: nil, start_date: Date.today + 1.days, last_cancel: Date.today)
        er = EventRegistration.new(event_id: event.id, employee_id: @emp.id)
        er.save(validate: false)

        delete "/v1/events/#{event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(200)
        expect(EventRegistration.exists?(er.id)).to be false
      end

      it 'should REJECT cancel if registered and today < start_date' do
        event = FactoryGirl.build(:event, capacity: nil, start_date: Date.yesterday, last_cancel: nil)
        event.save!(validate: false)
        er = EventRegistration.new(event_id: event.id, employee_id: @emp.id)
        er.save!(validate: false)

        delete "/v1/events/#{event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(400)
        expect(message).to include "You cant cancel"
        expect(EventRegistration.exists?(er.id)).to be true
      end

      it 'should REJECT cancel if registered and today > start_date after X available time' do
        event = FactoryGirl.build(:event, capacity: nil, start_date: Date.today + 1.days, last_cancel: Date.yesterday)
        event.save!(validate: false)
        er = EventRegistration.new(event_id: event.id, employee_id: @emp.id)
        er.save(validate: false)

        delete "/v1/events/#{event.id}/registration", headers: @emp_headers
        expect(response).to have_http_status(400)
        expect(message).to include "You cant cancel"
        expect(EventRegistration.exists?(er.id)).to be true
      end
    end

    # describe "DELETE /v1/events/:event_id/registration (As Admin)" do
    # end
  end
end
