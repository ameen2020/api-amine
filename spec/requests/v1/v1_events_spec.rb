require 'rails_helper'

RSpec.describe 'V1::Events', type: :request do
  describe 'Test Access Roles' do
    before(:all) do
      FactoryGirl.create(:event)
      @admin         = FactoryGirl.create(:employee, status: 1, role: 0)
      @emp           = FactoryGirl.create(:employee, status: 1, role: 1)
      @admin_headers = { 'Authorization' => "Bearer #{JsonWebToken.encode(@admin.login_payload)}" }
      @emp_headers   = { 'Authorization' => "Bearer #{JsonWebToken.encode(@emp.login_payload)}" }
      @params        = FactoryGirl.build(:event).as_json['event'].except('id', 'created_at', 'updated_at')
      @params_update = {capacity: 10}
    end

    before(:each) do
      @event = FactoryGirl.create(:event)
    end

    describe 'GET /v1/events' do
      it 'should allow users' do
        get v1_events_path, headers: @emp_headers
        expect(response).to have_http_status(200)
      end
      it 'should allow admins' do
        get v1_events_path, headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end

    describe 'POST /v1/events' do
      it 'should REJECT users' do
        post v1_events_path(@params), headers: @emp_headers
        expect(response).to have_http_status(403)
      end
      it 'should allow admins' do
        post v1_events_path(@params), headers: @admin_headers
        expect(response).to have_http_status(201)
      end
    end

    describe 'GET /v1/events/:id' do
      it 'should allow users' do
        get v1_event_path(@event.id), headers: @emp_headers
        expect(response).to have_http_status(200)
      end
      it 'should allow admins' do
        get v1_event_path(@event.id), headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end

    describe 'PUT /v1/events/:id' do
      it 'should REJECT users' do
        put v1_event_path(@event.id, @params_update), headers: @emp_headers
        # expect(response).to have_http_status(403)
        expect(response).to have_http_status(404)
      end
      it 'should allow admins' do
        put v1_event_path(@event.id, @params_update), headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end

    describe 'DELETE /v1/events/:id' do
      it 'should REJECT users' do
        delete v1_event_path(@event.id), headers: @emp_headers
        # expect(response).to have_http_status(403)
        expect(response).to have_http_status(404)
      end
      it 'should allow admins' do
        delete v1_event_path(@event.id), headers: @admin_headers
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'API Data' do

    describe 'GET /v1/events' do
    end

    describe 'GET /v1/events/:id' do
      describe "registration tag" do
        before(:each) do
          @emp         = FactoryGirl.create(:employee, status: 1, role: 1)
          @emp_headers = { 'Authorization' => "Bearer #{JsonWebToken.encode(@emp.login_payload)}" }
        end

        it 'should show can register if start_date > today and capacity is available and not registered' do
          @event = FactoryGirl.create(:event, capacity: 2)
          get v1_event_path(@event.id), headers: @emp_headers
          expect(response).to have_http_status(200)
          expect(data['event']['registration_tag']['can_register']).to   be true
          expect(data['event']['registration_tag']['registered']).to     be false
        end

        it 'should show can register if start_date > today and capacity is unlimited and not registered' do
          @event = FactoryGirl.create(:event, capacity: nil)
          get v1_event_path(@event.id), headers: @emp_headers

          expect(response).to have_http_status(200)
          expect(data['event']['registration_tag']['can_register']).to   be true
          expect(data['event']['registration_tag']['registered']).to     be false
        end

        it 'should show CANT register if start_date > today and capacity is full and not registered' do
          capacity = 2
          @event = FactoryGirl.create(:event, capacity: capacity)
          capacity.times do FactoryGirl.build(:event_registration, event_id: @event.id, employee_id: FactoryGirl.create(:employee).id).save(validate: false) end
          expect(@event.remaining).to eq 0

          get v1_event_path(@event.id), headers: @emp_headers

          expect(response).to have_http_status(200)
          expect(data['event']['registration_tag']['can_register']).to   be false
          expect(data['event']['registration_tag']['registered']).to     be false
        end

        it 'should show CANT register if start_date < today and not registered' do
          @event = FactoryGirl.build(:event, capacity: nil, start_date: Date.yesterday)
          @event.save!(validate: false)
          get v1_event_path(@event.id), headers: @emp_headers

          expect(response).to have_http_status(200)
          expect(data['event']['registration_tag']['can_register']).to   be false
          expect(data['event']['registration_tag']['registered']).to     be false
        end

        it 'should show registered if already registered' do
          @event = FactoryGirl.build(:event, capacity: nil, start_date: Date.yesterday)
          @event.save!(validate: false)
          EventRegistration.new(event_id: @event.id, employee_id: @emp.id).save!(validate: false)

          get v1_event_path(@event.id), headers: @emp_headers
          expect(response).to have_http_status(200)
          expect(data['event']['registration_tag']['can_register']).to   be false
          expect(data['event']['registration_tag']['registered']).to     be true
        end

        it 'should show can_unregister if registered and date < start_date with X available time' do
          @event = FactoryGirl.create(:event, capacity: nil, start_date: Date.today + 1.days, last_cancel: Date.today)
          EventRegistration.new(event_id: @event.id, employee_id: @emp.id).save(validate: false)

          get v1_event_path(@event.id), headers: @emp_headers
          expect(response).to have_http_status(200)
          expect(data['event']['registration_tag']['registered']).to     be true
          expect(data['event']['registration_tag']['can_unregister']).to be true
        end

        it 'should show CANT unregister if registered and date < start_date' do
          @event = FactoryGirl.build(:event, capacity: nil, start_date: Date.yesterday, last_cancel: nil)
          @event.save!(validate: false)
          EventRegistration.new(event_id: @event.id, employee_id: @emp.id).save!(validate: false)

          get v1_event_path(@event.id), headers: @emp_headers
          expect(response).to have_http_status(200)
          expect(data['event']['registration_tag']['registered']).to     be true
          expect(data['event']['registration_tag']['can_unregister']).to be false
        end

        it 'should show CANT unregister if registered and date > start_date after X available time' do
          @event = FactoryGirl.build(:event, capacity: nil, start_date: Date.today + 1.days, last_cancel: Date.yesterday)
          @event.save!(validate: false)
          EventRegistration.new(event_id: @event.id, employee_id: @emp.id).save(validate: false)

          get v1_event_path(@event.id), headers: @emp_headers
          expect(response).to have_http_status(200)
          expect(data['event']['registration_tag']['registered']).to     be true
          expect(data['event']['registration_tag']['can_unregister']).to be false
        end
      end
    end
  end
end
