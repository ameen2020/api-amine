module Requests
  module JsonHelpers
    def json
      JSON.parse(response.body)
    end

    def data
      json['data']
    end

    def message
      json['message']
    end
  end
end
