# Preview all emails at http://localhost:3000/rails/mailers/employee
class EmployeePreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/employee/forgot_password
  def forgot_password
    @employee = Employee.where('email is not null').order('RANDOM()').first || FactoryGirl.create(:employee)
    @employee.forgot_password(false)
    EmployeeMailer.forgot_password(@employee.id, @employee.reset_password_token)
  end
end
