# Preview all emails at http://localhost:3000/rails/mailers/admin_notifiaction
class AdminNotifiactionPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/admin_notifiaction/new_post
  def new_post
    AdminNotifiactionMailer.new_post(Employee.where(role: [0, 3]]).first, NotificationAdmin.order('random()').first)
  end
end
