# Preview all emails at http://localhost:3000/rails/mailers/ashanak
class AshanakPreview < ActionMailer::Preview

  # Preview all emails at http://localhost:3000/rails/mailers/ashanak/ashanak_email_to_admin
  def ashanak_email_to_admin
    @ashanak =  Ashanak.order("RANDOM()").first
    emp_id    = Employee.order("RANDOM()").first.id
    AshanakMailer.ashanak_email_to_admin(@ashanak.id, emp_id)
  end

  # Preview all emails at http://localhost:3000/rails/mailers/ashanak/send_approval_email
  def send_approval_email
    ashanak = Ashanak.where("super_admin_approve != ?", 0).first
    AshanakMailer.send_approval_email(ashanak.id)
  end

  # Preview all emails at http://localhost:3000/rails/mailers/ashanak/infrom_super_admins
  def infrom_super_admins
    ashanak =  Ashanak.order("RANDOM()").first
    AshanakMailer.inform_super_admin(ashanak.id, Employee.admin.first.id, Employee.admin.first.id)
  end
end
