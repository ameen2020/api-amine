# Preview all emails at http://localhost:3000/rails/mailers/offer_feedback
class OfferFeedbackPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/offer_feedback/infrom_admins
  def infrom_admins
    @feedback = OfferFeedback.order("RANDOM()").first
    OfferFeedbackMailer.infrom_admins(@feedback.id, Faker::Internet.email)
  end
end
