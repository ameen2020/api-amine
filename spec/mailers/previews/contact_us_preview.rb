# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/contact_us
class ContactUsPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/contact_us/contact
  def contact
    ContactUsMailer.contact(ContactUs.order('RANDOM()').first.id)
  end

  # Preview this email at http://localhost:3000/rails/mailers/contact_us/reply
  def reply
    ContactUsMailer.reply(ContactUs.order('RANDOM()').where.not(admin_id: nil).first.id || ContactUs.order('RANDOM()').first.id)
  end
end
