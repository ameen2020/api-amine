# Preview all emails at http://localhost:3000/rails/mailers/trip_registration
class TripRegistrationPreview < ActionMailer::Preview

  # Preview This email at http://localhost:3000/rails/mailers/trip_registration/send_approval_email
  def send_approval_email
    @registration =  TripRegistration.order("RANDOM()").first.id
    TripRegistrationMailer.send_approval_email(@registration)
  end

  # Preview all emails at http://localhost:3000/rails/mailers/trip_registration/reminder
  def reminder
    @registration =  TripRegistration.order("RANDOM()").first
    TripRegistrationMailer.reminder(@registration.id, [1,3].sample)
  end

  # Preview all emails at http://localhost:3000/rails/mailers/trip_registration/new_registration
  def new_registration
    @registration =  TripRegistration.order("RANDOM()").first
    @admin        =  Employee.order("RANDOM()").first
    TripRegistrationMailer.new_registration(@registration.id, @admin.id)
  end

  # Preview all emails at http://localhost:3000/rails/mailers/trip_registration/inform_super_admin
  def inform_super_admin
    @registration =  TripRegistration.order("RANDOM()").first
    @admin        =  Employee.order("RANDOM()").first
    TripRegistrationMailer.inform_super_admin(@registration.id, @admin.id)
  end

  # Preview all emails at http://localhost:3000/rails/mailers/trip_registration/cancel_by_user
  def cancel_by_user
    @registration =  TripRegistration.order("RANDOM()").first
    @admin        =  Employee.order("RANDOM()").first
    TripRegistrationMailer.cancel_by_user(@admin.id, @registration.employee_id, @registration.trip_id, JSON.parse(@registration.to_json)['trip_registration'])
  end
end
