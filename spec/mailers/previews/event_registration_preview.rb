# Preview all emails at http://localhost:3000/rails/mailers/event_registration
class EventRegistrationPreview < ActionMailer::Preview

  # Preview all emails at http://localhost:3000/rails/mailers/event_registration/send_registration_code
  def send_registration_code
    @registration =  EventRegistration.order("RANDOM()").first
    EventRegistrationMailer.send_registration_code(@registration.id)
  end

  # Preview all emails at http://localhost:3000/rails/mailers/event_registration/reminder
  def reminder
    @registration =  EventRegistration.order("RANDOM()").first
    EventRegistrationMailer.reminder(@registration.id, [1,3].sample)
  end

  # Preview all emails at http://localhost:3000/rails/mailers/event_registration/new_registration
  def new_registration
    @registration =  EventRegistration.order("RANDOM()").first
    @admin        =  Employee.order("RANDOM()").first
    EventRegistrationMailer.new_registration(@registration.id, @admin.id)
  end

  # Preview all emails at http://localhost:3000/rails/mailers/event_registration/cancel_by_user
  def cancel_by_user
    @registration =  EventRegistration.order("RANDOM()").first
    @admin        =  Employee.order("RANDOM()").first
    EventRegistrationMailer.cancel_by_user(@admin.id, @registration.employee_id, @registration.event_id, JSON.parse(@registration.to_json)['event_registration'])
  end
end
