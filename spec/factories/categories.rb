# == Schema Information
#
# Table name: categories
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb
#  status                   :integer          default("active"), not null
#  category_constant        :string           not null
#  position                 :integer
#  category_color           :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  logo_file_name           :string
#  logo_content_type        :string
#  logo_file_size           :integer
#  logo_updated_at          :datetime
#

FactoryGirl.define do
  factory :category do

    title_translations       { { ar: Faker::Name.title , en:  Faker::Name.title } }
    description_translations { { ar: Faker::Name.title , en:  Faker::Name.title } }
    category_constant        { SecureRandom.hex(2).upcase }
    position                 { Faker::Number.number(1)}
    category_color           { Faker::Color.hex_color}
  end
end
