# == Schema Information
#
# Table name: event_registrations
#
#  id            :integer          not null, primary key
#  employee_id   :integer          not null
#  event_id      :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  register_code :string           not null
#  locale        :string           default("en"), not null
#

FactoryGirl.define do
  factory :event_registration do
    employee { Employee.order('RANDOM()').first }
    event    { Event.order('RANDOM()').first }
    # register_code {Faker::Code.asin}
  end
end
