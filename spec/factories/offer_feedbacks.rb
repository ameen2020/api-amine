# == Schema Information
#
# Table name: offer_feedbacks
#
#  id          :integer          not null, primary key
#  employee_id :integer          not null
#  offer_id    :integer          not null
#  rating      :integer          not null
#  comment     :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer
#

FactoryGirl.define do
  factory :offer_feedback do
    employee_id { Employee.order('RANDOM()').first.id }
    offer_id    { Offer.order('RANDOM()').first.id }
    comment     { Faker::Lorem.sentence}
    rating      { rand(1..5) }
  end
end
