# == Schema Information
#
# Table name: news
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  date                     :date
#  important                :boolean          default(FALSE), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  views_counter            :integer          default(0), not null
#

FactoryGirl.define do
  factory :news, class: 'New' do
    title_translations       { { ar: Faker::Name.title, en: Faker::Name.title } }
    description_translations { { ar: Faker::Name.title, en: Faker::Name.title } }
    date                     { Faker::Date.forward(23) }
  end
end
