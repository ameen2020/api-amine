# == Schema Information
#
# Table name: employees
#
#  id                       :integer          not null, primary key
#  employee_id              :string
#  first_name               :string           not null
#  last_name                :string           default("-"), not null
#  email                    :string
#  password_digest          :string
#  role                     :integer          default("employee"), not null
#  status                   :integer          default("active"), not null
#  otp                      :integer
#  otp_expire_on            :datetime
#  register_token           :string
#  register_token_expire_on :datetime
#  new_phone_number         :string
#  phone_number             :string
#  otp_verified             :boolean          default(FALSE), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  reset_password_token     :string
#  reset_password_sent_at   :datetime
#  username                 :string           not null
#

FactoryGirl.define do
  factory :employee do
    employee_id      { Faker::Number.number(5) }
    first_name       { Faker::GameOfThrones.character }
    last_name        { Faker::Name.last_name }
    email            { Faker::Internet.email(first_name) }
    phone_number     { "+" + Faker::Number.number(10) }
  end
end
