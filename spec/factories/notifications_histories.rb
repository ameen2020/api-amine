# == Schema Information
#
# Table name: notifications_histories
#
#  id              :integer          not null, primary key
#  employee_id     :integer          not null
#  notifiable_id   :integer          not null
#  notifiable_type :string           not null
#  read            :boolean          default("unseen")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :notifications_history do
    
  end
end
