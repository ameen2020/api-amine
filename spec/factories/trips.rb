# == Schema Information
#
# Table name: trips
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  location_translations    :jsonb            not null
#  start_date               :datetime         not null
#  end_date                 :datetime
#  last_register_date       :datetime         not null
#  last_cancel_date         :datetime
#  capacity                 :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  views_counter            :integer          default(0), not null
#

FactoryGirl.define do
  factory :trip do
    title_translations       { { ar: Faker::Name.title, en: Faker::Name.title } }
    description_translations { { ar: Faker::Name.title, en: Faker::Name.title } }
    location_translations    { { ar: Faker::Address.city, en: Faker::Address.city } }
    start_date               { Faker::Date.forward(23) }
    end_date                 { [nil, Faker::Date.between(23.days.from_now, 50.days.from_now)].sample }
    last_register_date       { start_date - rand(1..2).days }
    last_cancel_date         { [nil, last_register_date - rand(1..2).days].sample }
    capacity                 { [nil, rand(20..200)].sample }
  end
end
