# == Schema Information
#
# Table name: relationships
#
#  id                 :integer          not null, primary key
#  title_translations :jsonb            not null
#  status             :boolean          default(TRUE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

FactoryGirl.define do
  factory :relationship do
    relationship_en = %i[ Brother Sister Father Mother Son Daughter]
    relationship_ar = %i[ أخ أخت أب أم أبن بنت]
    title_translations       { { ar: relationship_ar.sample, en:relationship_en.sample  } }

  end

end
