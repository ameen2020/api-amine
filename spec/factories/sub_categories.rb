# == Schema Information
#
# Table name: sub_categories
#
#  id                 :integer          not null, primary key
#  title_translations :jsonb            not null
#  status             :integer          default(1), not null
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  logo_file_name     :string
#  logo_content_type  :string
#  logo_file_size     :integer
#  logo_updated_at    :datetime
#

FactoryGirl.define do
  factory :sub_category do
    title_translations       { { ar: Faker::Name.title , en:  Faker::Name.title } }
    position                 { Faker::Number.number(1)}

  end
end
