# == Schema Information
#
# Table name: contact_us_infos
#
#  id         :integer          not null, primary key
#  option     :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :contact_us_info do
    option { %w(facebook twitter snapchat email phone_number instagram whatsapp).sample }
    value  { Faker::Internet.slug }
  end
end
