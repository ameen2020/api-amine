# == Schema Information
#
# Table name: offers
#
#  id                       :integer          not null, primary key
#  sub_category_id          :integer          not null
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  expire_on                :datetime         not null
#  discount                 :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  discount_type            :string
#  featured                 :boolean          default(FALSE), not null
#  email                    :string
#  phone_number             :string
#  views_counter            :integer          default(0), not null
#

FactoryGirl.define do
  factory :offer do
  # 1..30 passed on the colums those i alredy have on sub_category table
  sub_category_id          { SubCategory.order('RANDOM()').first.id }
  title_translations       { { ar: Faker::Name.title , en:  Faker::Name.title } }
  description_translations { { ar: Faker::Name.title , en:  Faker::Name.title } }
  expire_on                { Faker::Date.forward(23) }
  discount                  {Faker::Number.decimal(2, 2)}


  end
end
