# == Schema Information
#
# Table name: installments
#
#  id                  :integer          not null, primary key
#  nationality_id      :integer          not null
#  employee_id         :integer          not null
#  company_name        :string           not null
#  locale              :string           default("en"), not null
#  super_admin_approve :integer          default("pending"), not null
#  sub_admin_approve   :integer          default("pending"), not null
#  super_admin_comment :text
#  sub_admin_comment   :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

FactoryGirl.define do
  factory :installment do
    
  end
end
