# == Schema Information
#
# Table name: chalets
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  address_translations     :jsonb            not null
#  city_translations        :jsonb            not null
#  location_translations    :jsonb            not null
#  start_date               :datetime         not null
#  end_date                 :datetime
#  attachment_file_name     :string
#  attachment_content_type  :string
#  attachment_file_size     :integer
#  attachment_updated_at    :datetime
#  latitude                 :decimal(10, 8)   default(0.0), not null
#  longitude                :decimal(11, 8)   default(0.0), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  last_cancel              :datetime
#  views_counter            :integer          default(0), not null
#

FactoryGirl.define do
  factory :chalet do
    title_translations       { { ar: Faker::Name.title, en: Faker::Name.title } }
    description_translations { { ar: Faker::Name.title, en: Faker::Name.title } }
    address_translations     { { ar: Faker::Address.street_name, en: Faker::Address.street_name } }
    city_translations        { { ar: Faker::Address.city, en: Faker::Address.city } }
    location_translations    { { ar: Faker::Address.street_address, en: Faker::Address.street_address } }
    start_date               { Faker::Date.forward(23) }
    end_date                 { [nil, Faker::Date.between(23.days.from_now, 50.days.from_now)].sample }
    latitude                 { Faker::Address.latitude }
    longitude                { Faker::Address.longitude }
    last_cancel              { [nil, start_date - rand(1..2).days].sample }
  end
end
