# == Schema Information
#
# Table name: offer_locations
#
#  id         :integer          not null, primary key
#  offer_id   :integer          not null
#  latitude   :decimal(10, 8)   default(0.0), not null
#  longitude  :decimal(11, 8)   default(0.0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :offer_location do
    
  end
end
