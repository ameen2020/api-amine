# == Schema Information
#
# Table name: contact_us
#
#  id            :integer          not null, primary key
#  employee_id   :integer          not null
#  phone_number  :string           not null
#  message       :text             not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  admin_id      :integer
#  reply_message :text
#  replied_at    :datetime
#  locale        :string           default("en"), not null
#

FactoryGirl.define do
  factory :contact_us do
    employee_id  { Employee.order('RANDOM()').first.id }
    phone_number { "+" + Faker::Number.number(10) }
    message      { Faker::Lorem.paragraph }
  end
end
