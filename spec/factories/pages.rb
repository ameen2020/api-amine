# == Schema Information
#
# Table name: pages
#
#  id                   :integer          not null, primary key
#  title_translations   :jsonb            not null
#  content_translations :jsonb            not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

FactoryGirl.define do
  factory :page do
    title_translations       { { ar: Faker::Name.title, en: Faker::Name.title } }
    content_translations     { { ar: Faker::Name.title, en: Faker::Name.title } }
  end
end
