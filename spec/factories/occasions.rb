# == Schema Information
#
# Table name: occasions
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

FactoryGirl.define do
  factory :occasion do
    title_translations       { { ar: Faker::ElderScrolls.race, en: Faker::ElderScrolls.race } }
    description_translations { { ar: Faker::Lorem.sentence(3), en: Faker::Lorem.sentence(3) } }
  end
end
