# == Schema Information
#
# Table name: work_locations
#
#  id                 :integer          not null, primary key
#  region_id          :integer          not null
#  title_translations :jsonb            not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

FactoryGirl.define do
  factory :work_location do
    region_id { Region.order('RANDOM()').first.id }
    title_translations { { ar: Faker::Company.buzzword, en: Faker::Company.buzzword } }


  end
end
