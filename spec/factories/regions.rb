# == Schema Information
#
# Table name: regions
#
#  id                 :integer          not null, primary key
#  title_translations :jsonb            not null
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  ashanak            :boolean          default(TRUE), not null
#

FactoryGirl.define do
  factory :region do
    title_translations { { ar: Faker::GameOfThrones.city , en:  Faker::GameOfThrones.city } }
  end
end
