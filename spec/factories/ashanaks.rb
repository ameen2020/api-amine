# == Schema Information
#
# Table name: ashanaks
#
#  id                  :integer          not null, primary key
#  region_id           :integer          not null
#  occasion_id         :integer          not null
#  employee_id         :integer          not null
#  work_location_id    :integer          not null
#  date                :date             not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  phone_number        :string           default(""), not null
#  address             :text             default(""), not null
#  super_admin_approve :integer          default("pending"), not null
#  sub_admin_approve   :integer          default("pending"), not null
#  super_admin_comment :text
#  sub_admin_comment   :text
#  locale              :string           default("en"), not null
#

FactoryGirl.define do
  factory :ashanak do
    region_id        { Region.order('RANDOM()').first.id }
    employee_id      { Employee.order('RANDOM()').first.id }
    occasion_id      { Occasion.order('RANDOM()').first.id }
    work_location_id { WorkLocation.order('RANDOM()').first.id }
    date             { Faker::Date.forward(23) }
    phone_number     { "+" + Faker::Number.number(10) }
    address          { Faker::Address.full_address }
  end
end
