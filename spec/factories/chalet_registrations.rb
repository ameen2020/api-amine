# == Schema Information
#
# Table name: chalet_registrations
#
#  id                      :integer          not null, primary key
#  employee_id             :integer          not null
#  chalet_id               :integer          not null
#  register_code           :string
#  attachment_file_name    :string
#  attachment_content_type :string
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#  locale                  :string           default("en"), not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  super_admin_approve     :integer          default("pending"), not null
#  sub_admin_approve       :integer          default("pending"), not null
#  super_admin_comment     :text
#  sub_admin_comment       :text
#

FactoryGirl.define do
  factory :chalet_registration do
    
  end
end
