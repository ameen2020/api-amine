# == Schema Information
#
# Table name: trip_black_lists
#
#  id          :integer          not null, primary key
#  employee_id :integer          not null
#  blocker_id  :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :trip_black_list do
    name "MyString"
  end
end
