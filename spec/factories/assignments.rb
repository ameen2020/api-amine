# == Schema Information
#
# Table name: assignments
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  email                    :string
#  date                     :date
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  views_counter            :integer          default(0), not null
#

FactoryGirl.define do
  factory :assignment do
    title_translations       { { ar: Faker::Name.title, en: Faker::Name.title } }
    description_translations { { ar: Faker::Name.title, en: Faker::Name.title } }
    date                     { Faker::Date.forward(23) }
    email                    { Faker::Internet.email }
  end
end
