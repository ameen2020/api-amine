# == Schema Information
#
# Table name: pictures
#
#  id                      :integer          not null, primary key
#  imageable_id            :integer          not null
#  imageable_type          :string           not null
#  cover                   :boolean          default(FALSE), not null
#  attachment_file_name    :string
#  attachment_content_type :string
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

FactoryGirl.define do
  factory :picture do
    
  end
end
