# == Schema Information
#
# Table name: feedback_categories
#
#  id                 :integer          not null, primary key
#  title_translations :jsonb            not null
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

FactoryGirl.define do
  factory :feedback_category do
    title_translations       { { ar: Faker::Name.title , en:  Faker::Name.title } }
  end
end
