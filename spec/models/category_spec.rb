# == Schema Information
#
# Table name: categories
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb
#  status                   :integer          default("active"), not null
#  category_constant        :string           not null
#  position                 :integer
#  category_color           :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  logo_file_name           :string
#  logo_content_type        :string
#  logo_file_size           :integer
#  logo_updated_at          :datetime
#

require 'rails_helper'

RSpec.describe Category, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
end
