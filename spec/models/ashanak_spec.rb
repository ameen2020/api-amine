# == Schema Information
#
# Table name: ashanaks
#
#  id                  :integer          not null, primary key
#  region_id           :integer          not null
#  occasion_id         :integer          not null
#  employee_id         :integer          not null
#  work_location_id    :integer          not null
#  date                :date             not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  phone_number        :string           default(""), not null
#  address             :text             default(""), not null
#  super_admin_approve :integer          default("pending"), not null
#  sub_admin_approve   :integer          default("pending"), not null
#  super_admin_comment :text
#  sub_admin_comment   :text
#  locale              :string           default("en"), not null
#

require 'rails_helper'

RSpec.describe Ashanak, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
