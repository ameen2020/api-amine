# == Schema Information
#
# Table name: employees
#
#  id                       :integer          not null, primary key
#  employee_id              :string
#  first_name               :string           not null
#  last_name                :string           default("-"), not null
#  email                    :string
#  password_digest          :string
#  role                     :integer          default("employee"), not null
#  status                   :integer          default("active"), not null
#  otp                      :integer
#  otp_expire_on            :datetime
#  register_token           :string
#  register_token_expire_on :datetime
#  new_phone_number         :string
#  phone_number             :string
#  otp_verified             :boolean          default(FALSE), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  reset_password_token     :string
#  reset_password_sent_at   :datetime
#  username                 :string           not null
#

require 'rails_helper'

RSpec.describe Employee, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  describe "validations" do
    subject { FactoryGirl.build(:employee) }
    it { should have_secure_password }
    it { should validate_presence_of(:employee_id) }
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_uniqueness_of(:email) }
    it { should validate_uniqueness_of(:employee_id) }
    # it { should validate_uniqueness_of(:phone_number) }
    it { should allow_value("user@example.com").for(:email) }
    it { should_not allow_value("not-an-email").for(:email) }
  end

  describe 'Generate register token' do
    before(:all) do
      @emp = FactoryGirl.create(:employee)
    end

    it 'generate register token after create' do
      expect(@emp.register_token.present?).to be true
    end

    it 'valid_register_token? within 5 minutes' do
      time_later = Time.zone.now + 5.minutes
      expect(@emp.valid_register_token?).to be true
      allow(Time.zone).to receive(:now).and_return(time_later)
      expect(@emp.valid_register_token?).to be false
    end

    it 'clear_register_token!' do
      expect(@emp.clear_register_token!).to be true
      expect(@emp.register_token).to be nil
      expect(@emp.register_token_expire_on).to be nil
    end

    it 'dont crash in valid_register_token? if no token' do
      expect(@emp.register_token_expire_on).to be nil
      expect(@emp.valid_register_token?).to be false
    end
  end

  describe ".find_registered_user(employee_id, token)" do
    before(:all) do
      @emp = FactoryGirl.create(:employee)
    end

    it 'should return true if user found and token is valid' do
      expect(Employee.find_registered_user(@emp.employee_id, @emp.register_token)[:id]).to eq @emp.id
    end

    it 'should return false if user not found' do
      expect(Employee.find_registered_user('123456', @emp.register_token)).to be false
    end

    it 'should return false if user found but token is not valid' do
      expect(Employee.find_registered_user(@emp.employee_id, '123456')).to be false
    end

    it 'should return false if user found but token is expired' do
      time_later = Time.zone.now + 5.minutes
      allow(Time.zone).to receive(:now).and_return(time_later)
      expect(Employee.find_registered_user(@emp.employee_id, @emp.register_token)).to be false
    end

    it 'should return false if user is status is not pending' do
      @emp.active!
      expect(Employee.find_registered_user(@emp.employee_id, @emp.register_token)).to be false
    end
  end
end
