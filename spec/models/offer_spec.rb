# == Schema Information
#
# Table name: offers
#
#  id                       :integer          not null, primary key
#  sub_category_id          :integer          not null
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  expire_on                :datetime         not null
#  discount                 :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  discount_type            :string
#  featured                 :boolean          default(FALSE), not null
#  email                    :string
#  phone_number             :string
#  views_counter            :integer          default(0), not null
#

require 'rails_helper'

RSpec.describe Offer, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
end
