# == Schema Information
#
# Table name: employee_voices
#
#  id                            :integer          not null, primary key
#  employee_id                   :integer          not null
#  voice_category                :integer          default("suggestion"), not null
#  phone_number                  :string           not null
#  message                       :string           not null
#  attachment_file_name          :string
#  attachment_content_type       :string
#  attachment_file_size          :integer
#  attachment_updated_at         :datetime
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  admin_id                      :integer
#  reply_message                 :text
#  replied_at                    :datetime
#  locale                        :string           default("en"), not null
#  attachment_admin_file_name    :string
#  attachment_admin_content_type :string
#  attachment_admin_file_size    :integer
#  attachment_admin_updated_at   :datetime
#

require 'rails_helper'

RSpec.describe EmployeeVoice, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
