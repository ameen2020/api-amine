# == Schema Information
#
# Table name: events
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  address_translations     :jsonb            not null
#  city_translations        :jsonb            not null
#  location_translations    :jsonb            not null
#  start_date               :datetime         not null
#  end_date                 :datetime
#  capacity                 :integer
#  latitude                 :decimal(10, 8)   default(0.0), not null
#  longitude                :decimal(11, 8)   default(0.0), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  last_cancel              :datetime
#  locations                :json             not null
#  views_counter            :integer          default(0), not null
#

require 'rails_helper'

RSpec.describe Event, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
end
