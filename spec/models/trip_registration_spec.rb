# == Schema Information
#
# Table name: trip_registrations
#
#  id                  :integer          not null, primary key
#  employee_id         :integer          not null
#  trip_id             :integer          not null
#  relationship_id     :integer
#  full_name           :string
#  id_number           :integer
#  companion           :boolean          default(FALSE), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  register_code       :string           not null
#  super_admin_approve :integer          default("pending"), not null
#  sub_admin_approve   :integer          default("pending"), not null
#  super_admin_comment :text
#  sub_admin_comment   :text
#  locale              :string           default("en"), not null
#

require 'rails_helper'

RSpec.describe TripRegistration, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
