# == Schema Information
#
# Table name: chalets
#
#  id                       :integer          not null, primary key
#  title_translations       :jsonb            not null
#  description_translations :jsonb            not null
#  address_translations     :jsonb            not null
#  city_translations        :jsonb            not null
#  location_translations    :jsonb            not null
#  start_date               :datetime         not null
#  end_date                 :datetime
#  attachment_file_name     :string
#  attachment_content_type  :string
#  attachment_file_size     :integer
#  attachment_updated_at    :datetime
#  latitude                 :decimal(10, 8)   default(0.0), not null
#  longitude                :decimal(11, 8)   default(0.0), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  last_cancel              :datetime
#  views_counter            :integer          default(0), not null
#

require 'rails_helper'

RSpec.describe Chalet, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
