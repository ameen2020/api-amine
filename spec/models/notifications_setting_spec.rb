# == Schema Information
#
# Table name: notifications_settings
#
#  id             :integer          not null, primary key
#  employee_id    :integer          not null
#  event          :boolean          default(FALSE), not null
#  offer          :boolean          default(FALSE), not null
#  trip           :boolean          default(FALSE), not null
#  news           :boolean          default(FALSE), not null
#  memo           :boolean          default(FALSE), not null
#  job            :boolean          default(FALSE), not null
#  magazine       :boolean          default(FALSE), not null
#  region         :integer          default([]), is an Array
#  sub_category   :integer          default([]), is an Array
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  chalet         :boolean          default(FALSE), not null
#  mbadrat        :boolean          default(FALSE), not null
#  employee_voice :boolean          default(FALSE), not null
#  installment    :boolean          default(FALSE), not null
#

require 'rails_helper'

RSpec.describe NotificationsSetting, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
