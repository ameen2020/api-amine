require 'rails_helper'

RSpec.describe Responders, type: :helper do
  before(:each) do
    @package = { user: { id: 1 } }
  end

  describe 'render_data' do
    it 'should render the data package with default success message and status :ok' do
      expect(helper).to receive(:render).with(json: { success: true, message: I18n.t(:data_found), data: @package }, status: :ok)
      helper.render_data(@package)
    end

    it 'should render the data package with custom message' do
      message = 'this is custom message'
      expect(helper).to receive(:render).with(json: { success: true, message: message, data: @package }, status: :ok)
      helper.render_data(@package, message: message)
    end

    it 'should merge the meta object to data package' do
      package = { users: [{ id: 1 }] }
      meta    = { pagination: { total: 10 } }

      merged  = { users: [{ id: 1 }], pagination: { total: 10 } }
      expect(helper).to receive(:render).with(json: { success: true, message: I18n.t(:data_found), data: merged }, status: :ok)
      helper.render_data(package, meta: meta)
    end
  end

  describe 'render_success' do
    before(:each) do
      @message = 'this is custom message'
    end
    it 'should render a success custom message with default empty data object' do
      expect(helper).to receive(:render).with(json: { success: true, message: @message, data: {} }, status: :ok)
      helper.render_success(@message)
    end

    it 'should render the success custom message and the data package' do
      expect(helper).to receive(:render).with(json: { success: true, message: @message, data: @package }, status: :ok)
      helper.render_success(@message, data: @package)
    end

    it 'should merge the meta object to data package and render created' do
      meta   = {access_token: 'xxx', role_id: 1}
      merged = { user: { id: 1 }, access_token: 'xxx', role_id: 1 }
      expect(helper).to receive(:render).with(json: { success: true, message: @message, data: merged }, status: :ok)
      helper.render_success(@message, data: @package, meta: meta)
    end
  end

  describe 'render_created' do
    it 'should render a created package with success and status :created' do
      expect(helper).to receive(:render).with(json: { success: true, message: I18n.t(:created_successfully), data: @package }, status: :created)
      helper.render_created(@package)
    end

    it 'should render the created package with custom message' do
      message = 'this is custom message'
      expect(helper).to receive(:render).with(json: { success: true, message: message, data: @package }, status: :created)
      helper.render_created(@package, message: message)
    end

    it 'should merge the meta object to data package and render created' do
      meta   = {access_token: 'xxx', role_id: 1}
      merged = { user: { id: 1 }, access_token: 'xxx', role_id: 1 }
      expect(helper).to receive(:render).with(json: { success: true, message: I18n.t(:created_successfully), data: merged }, status: :created)
      helper.render_created(@package, meta: meta)
    end
  end
end
